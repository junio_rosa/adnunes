<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 255);
            $table->string('endereco', 255);
            $table->string('bairro', 255);
            $table->string('cidade', 255);
            $table->string('estado', 2);
            $table->string('email', 255)->nullable();
            $table->string('tel_fixo', 20)->nullable();
            $table->string('tel_comercio', 20)->nullable();
            $table->string('tel_cel', 20)->nullable();
            $table->string('cpf', 14)->nullable();
            $table->longText('observacoes')->nullable();
            $table->string('nome_empresa', 255)->nullable();
            $table->string('cnpj', 18)->nullable();
            $table->string('endereco_empresa', 255)->nullable();
            $table->string('email_empresa', 255)->nullable();
            $table->string('bairro_empresa', 255)->nullable();
            $table->string('cidade_empresa', 255)->nullable();
            $table->string('estado_empresa', 2)->nullable();
            $table->string('info_comercial_tel_1', 20)->nullable();
            $table->string('info_comercial_tel_2', 20)->nullable();
            $table->longText('info_comercial_obs')->nullable();
            $table->string('formas_pagamento', 255)->nullable();
            $table->string('status', 16);
            $table->string('img_contrato', 255)->nullable();
            $table->boolean('aprovado');
            $table->string('tipo_cadastro', 20);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
