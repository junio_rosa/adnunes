<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('clients');
            $table->date('devolution_date');
            $table->double('total_daily');
            $table->string('status', 999);
            $table->string('address', 999);
            $table->string('hour', 5);
            $table->string('validation', 20);
            $table->double('total_price');
            $table->date('rent_date');
            $table->double('previous_paid');
            $table->longText('authorized_workers')->nullable();
            $table->double('delivery_value');
            $table->double('desconto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rents');
    }
}
