<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Rent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rent = Rent::whereDate('devolution_date', '<=', date('Y-m-d'))->where('validation', 'Aberto')->get();
        foreach($rent as $r){
            $clients = Client::find($r->id_client);
            $clients->status = 'Possui pendência';
            $clients->save();
            $r->validation = 'Venceu';
            $r->save();
        }

        return view('home');
    }

    public function atualizar_dados_perfil(Request $request){
        $msg = '';
        $user = User::find($request->input('id'));
        if($user->email != $request->input('email')){
            if($this->verifica_email_db($request->input('email')))  {
                $msg .= 'E-mail já cadastrado<br>';
            }
        }
        if($msg == ''){
            $user->name = $request->input('nome');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('senha'));
            $user->save();
            return 'Perfil atualizado com sucesso';
        }else{
            return 'ERROS:<br><br>'.$msg;
        }
    }

    private function verifica_email_db($email){
        $user = User::where('email', $email)->get()->count();
        if($user > 0){
            return 1;
        }
        return 0;
    }

    public function atualizar_header(){
        return view('navbar.navbar_auth')->render();
    }
}
