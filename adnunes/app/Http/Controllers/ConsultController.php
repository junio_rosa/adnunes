<?php

namespace App\Http\Controllers;

use App\Models\Cash_Flow;
use App\Models\Equip_In;
use App\Models\Equip_Out;
use App\Models\Product;
use Illuminate\Http\Request;

class ConsultController extends Controller
{
    public function index(){
        $qty_equip = Product::query()->compareProducts('', 'name')->productSection(true);
        $array_sum_qty = [];
        $array_sum_return_qty = [];
        $array_rent = [];
        $aux_rent = '';
        foreach($qty_equip as $q){
            $rent = Product::query()->id_rents($q->id);
            foreach($rent as $r){
                $aux_qty = $r->qty;
                $aux_return_qty = $r->return_qty;
                if($aux_qty > $aux_return_qty){
                    $aux_rent = $aux_rent. $r->id.' - '.$r->nome.' ';
                }
            }
            $sum_qty = Product::query()->sumQty($q->id);
            $return_qty = Product::query()->sumReturnQty($q->id);
            array_push($array_rent, $aux_rent);
            $aux_rent = '';
            array_push($array_sum_qty, $sum_qty);
            array_push($array_sum_return_qty, $return_qty);
        }
        $equip_in = Equip_In::whereDate('date', date('Y-m-d'))->orderBy('rent_id', 'desc')->paginate(10);
        $equip_out = Equip_Out::whereDate('date', date('Y-m-d'))->orderBy('rent_id', 'desc')->paginate(10);
        $cash_flow = Cash_Flow::whereDate('date', date('Y-m-d'))->orderBy('rent_id', 'desc')->paginate(10);
        $sum_paid = Cash_Flow::whereDate('date', date('Y-m-d'))->orderBy('rent_id', 'desc')->sum('paid');
        return view('consults.consults', compact(['equip_in', 'equip_out', 'cash_flow', 'qty_equip', 'array_sum_qty', 'array_sum_return_qty', 'sum_paid', 'array_rent']));
    }

    public function equip_in(Request $request){
        $init_date = $request->get('init_date');
        $ending_date = $request->get('ending_date');
        $equip_in = Equip_In::query()->orderBy('rent_id', 'desc')->dateInterval($init_date, $ending_date)->paginate(10);
        return view('consults.table_equip_in', compact('equip_in'))->render();
    }

    public function equip_out(Request $request){
        $init_date = $request->get('init_date');
        $ending_date = $request->get('ending_date');
        $equip_out = Equip_Out::query()->orderBy('rent_id', 'desc')->dateInterval($init_date, $ending_date)->paginate(10);
        return view('consults.table_equip_out', compact('equip_out'))->render();
    }

    public function cash_flow(Request $request){
        $init_date = $request->get('init_date');
        $ending_date = $request->get('ending_date');
        $cash_flow = Cash_Flow::query()->orderBy('rent_id', 'desc')->dateInterval($init_date, $ending_date)->paginate(10);
        $sum_paid = Cash_Flow::query()->orderBy('rent_id', 'desc')->dateInterval($init_date, $ending_date)->sum('paid');;
        return view('consults.table_cash_flow', compact(['cash_flow', 'sum_paid']))->render();
    }

    public function qty_equip(Request $request){
        $input = $request->get('input');
        $qty_equip = Product::query()->compareProducts($input, 'name')->productSection(true);
        $array_sum_qty = [];
        $array_sum_return_qty = [];
        $array_rent = [];
        $aux_rent = '';
        foreach($qty_equip as $q){
            $rent = Product::query()->id_rents($q->id);
            foreach($rent as $r){
                $aux_qty = $r->qty;
                $aux_return_qty = $r->return_qty;
                if($aux_qty > $aux_return_qty){
                    $aux_rent = $aux_rent. $r->id.' - '.$r->nome.' ';
                }
            }
            $sum_qty = Product::query()->sumQty($q->id);
            $return_qty = Product::query()->sumReturnQty($q->id);
            array_push($array_rent, $aux_rent);
            $aux_rent = '';
            array_push($array_sum_qty, $sum_qty);
            array_push($array_sum_return_qty, $return_qty);
        }

        return view('consults.table_qty_equip', compact(['qty_equip', 'array_sum_qty', 'array_sum_return_qty', 'array_rent']))->render();
    }
	
	public function equip_pdf(Request $request){
		$input = $request->get('input');
        $qty_equip = Product::query()->compareProducts($input, 'name')->get();
        $array_sum_qty = [];
        $array_sum_return_qty = [];
        $array_rent = [];
        $aux_rent = '';
        foreach($qty_equip as $q){
            $rent = Product::query()->id_rents($q->id);
            foreach($rent as $r){
                $aux_qty = $r->qty;
                $aux_return_qty = $r->return_qty;
                if($aux_qty > $aux_return_qty){
                    $aux_rent = $aux_rent. '<strong>Pedido número: </strong>'.$r->id.' - <strong>Cliente:</strong> '.$r->nome.'§';
                }
            }
            $sum_qty = Product::query()->sumQty($q->id);
            $return_qty = Product::query()->sumReturnQty($q->id);
            array_push($array_rent, $aux_rent);
            $aux_rent = '';
            array_push($array_sum_qty, $sum_qty);
            array_push($array_sum_return_qty, $return_qty);
        }
        return view('consults.equip_pdf', compact(['qty_equip', 'array_sum_qty', 'array_sum_return_qty', 'array_rent']));
	}
}
