<?php

namespace App\Http\Controllers;

use App\Models\Cash_Flow;
use App\Models\Client;
use App\Models\Equip_In;
use App\Models\Equip_Out;
use App\Models\Product;
use App\Models\Rent;
use App\Models\Rent_Details;
use Illuminate\Http\Request;

class RentController extends Controller
{
    public function index(){
        $rent = Rent::whereDate('devolution_date', '<=', date('Y-m-d'))->where('validation', 'Aberto')->get();
        foreach($rent as $r){
            $clients = Client::find($r->id_client);
            $clients->status = 'Possui pendência';
            $clients->save();
            $r->validation = 'Venceu';
            $r->save();
        }

        $clients = Client::orderBy('nome', 'asc')->where('aprovado', 1)->paginate(10);
        $products = Product::orderBy('name', 'asc')->paginate(10);
        $rents_preview = Rent::orderBy('id', 'desc')->paginate(10);

        $sum_total_price = Rent::orderBy('id', 'desc')->sum('total_price');
        $sum_delivery_value = Rent::orderBy('id', 'desc')->sum('delivery_value');
        $sum_desconto = Rent::orderBy('id', 'desc')->sum('desconto');
        $sum_previous_paid = Rent::orderBy('id', 'desc')->sum('previous_paid');
        return view('rent.rent', compact(['clients', 'products', 'rents_preview', 'sum_total_price', 'sum_delivery_value', 'sum_previous_paid', 'sum_desconto']));
    }

    public function store(Request $request){
        $last_rent = Rent::orderBy('id', 'desc')->first();
        $rent_id = 0;
        if($last_rent != NULL){
            $rent_id = $last_rent->id + 1;
        }else{
            $rent_id = 1;
        }

        $rent = new Rent();
        $rent->validation = 'Aberto';
        $delivery_value = $request->input('delivery_value');
        $previous_paid = $request->input('previous_paid');
        $total_price = $request->input('total_price') + $delivery_value - $request->input('desconto');
        if(number_format($total_price, 2) == number_format($previous_paid, 2)){
            $rent->status = 'Quitado';
        }else{
            $rent->status = 'A quitar';
        }
        $rent->id = $rent_id;
        $rent->id_client = $request->input('id_client');
        $rent->devolution_date = $request->input('devolution_date');
        $rent->total_daily = float_format($request->input('total_daily'));
        $rent->address = $request->input('address');
        $rent->hour = $request->input('hour');
        $rent->previous_paid = $previous_paid;
        $rent->rent_date = $request->input('rent_date');
        $rent->desconto = $request->input('desconto');
        $rent->authorized_workers = $request->input('authorized_workers');
        $rent->delivery_value = $delivery_value;
        $rent->total_price = $request->input('total_price');
        $rent->save();

        if($previous_paid != 0){
            $cash_flow = new Cash_Flow();
            $cash_flow->rent_id = $rent_id;
            $cash_flow->date = $request->input('rent_date');
            $cash_flow->total_price = $rent->total_price + $rent->delivery_value - $rent->desconto;
            $cash_flow->paid = $previous_paid;
            $cash_flow->total_paid = $previous_paid;
            $cash_flow->save();
        }

        $products = $request->input('products');
        $i = 0;
        while($i < count($products)){
            $product = Product::find($products[$i]['product_id']);
            $return_qty = 0;
            if($product->is_rent == 1){
                $equip_out = new Equip_Out();
                $equip_out->rent_id = $rent_id;
                $equip_out->client_id = $request->input('id_client');
                $equip_out->product_id = $products[$i]['product_id'];
                $equip_out->date = $request->input('rent_date');
                $equip_out->qty = $products[$i]['qty'];
                $equip_out->save();
            }else{
                $return_qty = $products[$i]['qty'];
            }
            $rent_details = new Rent_Details();
            $rent_details->product_id = $products[$i]['product_id'];
            $rent_details->qty = $products[$i]['qty'];
            $rent_details->daily = float_format($products[$i]['daily']);
            $rent_details->rent_id = $rent_id;
            $rent_details->return_qty = $return_qty;
            $rent_details->save();
            
            $product->qty_stock = $product->qty_stock - $products[$i]['qty'];
            $product->save();
            $i++;
        }
        $msg = 'Aluguel cadastrado com sucesso';
        $rent = Rent::find($rent_id);
        $array = array(
            'msg' => $msg,
            'rent' => $rent,
            'rent_client' => $rent->client,
            'rent_details' => $rent->rent_details
        );
        return $array;
    }

    public function autocomplete(Request $request)
    {
        $input = $request->input('input');
        $select = $request->input('select');
		if($select == 'nome'){
			$rent = Client::where('nome', 'like', '%'.$input.'%')->orWhere('nome_empresa', 'like', '%'.$input.'%')->orderBy('nome', 'asc')->limit(10)->get();
		}
		else{
			$rent = Client::where($select, 'like', '%'.$input.'%')->orderBy('nome', 'asc')->limit(10)->get();
		}
        return $rent;
    }

    public function show(Request $request){
        $input = $request->get('input');
        $select = $request->get('select');
        $init_date = $request->get('init_date');
        $ending_date = $request->get('ending_date');
        $report_type = $request->get('report_type');

        $rents_preview = Rent::query()->orderBy('id', 'desc')->compareRentClients($input, $select)->dateInterval($init_date, $ending_date)->reportType($report_type)->rentSection(true);
        $sum_total_price = Rent::query()->orderBy('id', 'desc')->compareRentClients($input, $select)->dateInterval($init_date, $ending_date)->reportType($report_type)->get()->sum('total_price');
        $sum_delivery_value = Rent::query()->orderBy('id', 'desc')->compareRentClients($input, $select)->dateInterval($init_date, $ending_date)->reportType($report_type)->get()->sum('delivery_value');
        $sum_previous_paid = Rent::query()->orderBy('id', 'desc')->compareRentClients($input, $select)->dateInterval($init_date, $ending_date)->reportType($report_type)->get()->sum('previous_paid');
        $sum_desconto = Rent::query()->orderBy('id', 'desc')->compareRentClients($input, $select)->dateInterval($init_date, $ending_date)->reportType($report_type)->get()->sum('desconto');
        return view('rent.table_rent', compact(['rents_preview', 'sum_desconto', 'sum_total_price', 'sum_delivery_value', 'sum_previous_paid']))->render();
    }

    public function rent_pdf(Request $request){
        $rent_pdf = Rent::find($request->input('id'));
        return view('rent.pdf_rent', compact('rent_pdf'));
    }

    public function recibo(Request $request){
        $rent_pdf = Rent::find($request->input('id'));
        return view('rent.recibo', compact('rent_pdf'));
    }

    public function destroy($id)
    {
        $rent = Rent::find($id);
        if(isset($rent)){
            $rent_details = Rent_Details::where('rent_id', $id)->get();
            foreach($rent_details as $p){
                $product = Product::find($p->product_id);
                $product->qty_stock = $product->qty_stock + $p->qty;
                $product->save();
                $p->delete();
            }
            $equip_out = Equip_Out::where('rent_id', $id)->get();
            foreach($equip_out as $e){
                $e->delete();
            }
            $equip_in = Equip_In::where('rent_id', $id)->get();
            foreach($equip_in as $e){
                $e->delete();
            }

            $id_client = $rent->id_client;
            $rent->delete();

            $pending_orders = Rent::where('id_client', $id_client)->where('validation', 'Venceu')->get()->count();
            if($pending_orders == 0){
                $client = Client::find($id_client);
                $client->status = 'Aprovado';
                $client->save();
            }
            return 'Pedido deletado com sucesso';
        }else{
            return 'ERRO:<br>Pedido não encontrado';
        }
    }

    public function update(Request $request, $id){
        $products = $request->input('products');
        $i = 0;
        $size_products = count($products);
        $is_discharge = false;
        while($i < count($products)){
            $rent_details = Rent_Details::where('rent_id', $id)->where('product_id', $products[$i]['product_id'])->get();
            foreach($rent_details as $r){
                if($products[$i]['total_return_qty'] != "Produto para venda"){
                    $r->return_qty = $products[$i]['total_return_qty'];
                    $r->save();
                }
            }

            if($products[$i]['qty'] == $products[$i]['total_return_qty'] || 
                $products[$i]['total_return_qty'] == "Produto para venda"){
                $size_products = $size_products - 1;
            }
            if($products[$i]['return_qty'] > 0 && $products[$i]['total_return_qty'] != "Produto para venda"){
                $equip_in = new Equip_In();
                $equip_in->rent_id = $id;
                $equip_in->client_id = $request->input('id_client');
                $equip_in->product_id = $products[$i]['product_id'];
                $equip_in->date = date('Y-m-d');
                $equip_in->qty = $products[$i]['return_qty'];
                $equip_in->save();

                $product = Product::find($products[$i]['product_id']);
                $product->qty_stock = $product->qty_stock + $products[$i]['return_qty'];
                $product->save();
            }
            $i++;
        }
        if($size_products == 0){
            $is_discharge = true;
        }

        $rent = Rent::find($id);
        $previous_devolution_date = $rent->devolution_date;
        $actual_devolution_date = $request->input('devolution_date');
        if($previous_devolution_date < $actual_devolution_date){
            $rent->status = 'A quitar';
            $rent->validation = 'Aberto';
            $id_client = $rent->id_client;
            $pending_orders = Rent::where('id_client', $id_client)->where('validation', 'Venceu')->get()->count();
            if($pending_orders == 0){
                $client = Client::find($id_client);
                $client->status = 'Aprovado';
                $client->save();
            }
        }

        $delivery_value = $request->input('delivery_value');
        $previous_paid = $request->input('previous_paid');
        $total_price = $request->input('total_price') + $delivery_value - $request->input('desconto');
        if(number_format($total_price, 2) == number_format($previous_paid, 2)){
            if($is_discharge){
                $rent->validation = 'Pago';
                $rent->status = 'Baixa';
            }else{
                $rent->status = 'Quitado';
            }
        }

        $rent->id_client = $request->input('id_client');
        $rent->devolution_date = $request->input('devolution_date');
        $rent->total_daily = float_format($request->input('total_daily'));
        $rent->address = $request->input('address');
        $rent->hour = $request->input('hour');
        $rent->previous_paid = $previous_paid;
        $rent->rent_date = $request->input('rent_date');
        $rent->authorized_workers = $request->input('authorized_workers');
        $rent->delivery_value = $delivery_value;
        $rent->total_price = $request->input('total_price');
        $rent->desconto = $request->input('desconto');
        $rent->save();

        $previous_paid = $previous_paid - $request->input('paid');
        if($previous_paid != 0){
            $cash_flow = new Cash_Flow();
            $cash_flow->rent_id = $id;
            $cash_flow->date = date('Y-m-d');
            $cash_flow->total_price = $total_price + $delivery_value - $request->input('desconto') - $previous_paid;
            $cash_flow->paid = $previous_paid;
            $cash_flow->total_paid = ($previous_paid + $request->input('paid'));
            $cash_flow->save();
        }
        $pending_orders = Rent::where('id_client', $request->input('id_client'))->where('validation', 'Venceu')->get()->count();
        if($pending_orders == 0){
            $client = Client::find($request->input('id_client'));
            $client->status = 'Aprovado';
            $client->save();
        }
        $msg = 'Aluguel atualizado com sucesso';
        $rent = Rent::find($id);
        $array = array(
            'msg' => $msg,
            'rent' => $rent,
            'rent_client' => $rent->client,
            'rent_details' => $rent->rent_details
        );
        return $array;
    }

    public function payAll($id){
        $rent = Rent::find($id);
        $rent_details = Rent_Details::where('rent_id', $id)->get();
        $size_products = count($rent_details);
        $is_discharge = false;
        foreach($rent_details as $r){
            if($r->return_qty == $r->qty){
                $size_products = $size_products - 1;
            }
        }
        if($size_products == 0){
            $is_discharge = true;
        }
        $payAll = $rent->total_price - $rent->previous_paid + $rent->delivery_value - $rent->desconto;
        $rent->previous_paid = $payAll + $rent->previous_paid;
        if($is_discharge){
            $rent->status = 'Baixa';
            $rent->validation = 'Pago';
        }else{
            $rent->status = 'Quitado';
        }
        $rent->save();

        $cash_flow = new Cash_Flow();
        $cash_flow->rent_id = $id;
        $cash_flow->date = date('Y-m-d');
        $cash_flow->total_price = $rent->total_price + $rent->delivery_value - $rent->desconto;
        $cash_flow->paid = $payAll;
        $cash_flow->total_paid = $rent->total_price + $rent->delivery_value - $rent->desconto;
        $cash_flow->save();

        return 'Pedido pago com sucesso';
    }
}
