<?php

namespace App\Http\Controllers;

use App\Mail\TermoAutorizacao;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ClientsController extends Controller
{
    public function index(){
        $clients = Client::orderBy('nome', 'asc')->paginate(10);
        $able_cli = true;
        return view('clients.clients', compact(['clients', 'able_cli']));
    }

    public function store(Request $request)
    {
        $msg = '';
        if($request->input('cpf') != ''){
            $count_registered = Client::where('cpf', $request->input('cpf'))->get()->count();
            if($count_registered > 0){
                $msg .= 'CPF já cadastrado<br>';
            }
        }
        if($request->input('cnpj') != ''){
            $count_registered = Client::where('cnpj', $request->input('cnpj'))->get()->count();
            if($count_registered > 0){
                $msg .= 'CNPJ já cadastrado<br>';
            }
        }
        if($msg == ''){
            $client = new Client();
            $client->nome = $request->input('nome');
            $client->email = $request->input('email');
            $client->endereco = $request->input('endereco');
            $client->bairro = $request->input('bairro');
            $client->tel_fixo = $request->input('tel_fixo');
            $client->tel_comercio = $request->input('tel_comercio');
            $client->tel_cel = $request->input('tel_cel');
            $client->observacoes = $request->input('observacoes');
            $client->tipo_cadastro = $request->input('tipo_cadastro');
            $client->status = $request->input('status');
            $client->aprovado = $request->input('aprovado');
            if($request->tipo_cadastro != 'Cadastro Simplificado'){
                $client->cidade = $request->input('cidade');
                $client->estado = $request->input('estado');
                $client->cpf = $request->input('cpf');
                $client->formas_pagamento = $request->input('formas_pagamento');

                if($request->tipo_cadastro == 'Pessoa Física'){
                    if($request->input('extensao') != ''){
                        $controle_loop = true;
                        while($controle_loop){
                            $chave_nome_arquivo = md5($request->file('thumb')->getClientOriginalName(). time()).'.'.$request->input('extensao');
                            $count_registered = Client::where('img_contrato', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                            if($count_registered == 0){
                                $controle_loop = false;
                                $request->file('thumb')->storeAs('assets/contratos_clientes/', $chave_nome_arquivo,'uploads');
                                $client->img_contrato = 'assets/contratos_clientes/'.$chave_nome_arquivo;
                            }
                        }
                    }
                }else{
                    $client->nome_empresa = $request->nome_empresa;
                    $client->email_empresa = $request->email_empresa;
                    $client->endereco_empresa = $request->endereco_empresa;
                    $client->bairro_empresa = $request->bairro_empresa;
                    $client->cidade_empresa = $request->cidade_empresa;
                    $client->estado_empresa = $request->estado_empresa;
                    $client->cnpj = $request->cnpj;
                    if($request->input('extensao_contrato_social') != ''){
                        $controle_loop = true;
                        while($controle_loop){
                            $chave_nome_arquivo = md5($request->file('thumb_contrato_social')->getClientOriginalName(). time()).'.'.$request->input('extensao_contrato_social');
                            $count_registered = Client::where('contrato_social', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                            if($count_registered == 0){
                                $controle_loop = false;
                                $request->file('thumb_contrato_social')->storeAs('assets/contrato_social/', $chave_nome_arquivo,'uploads');
                                $client->contrato_social = 'assets/contrato_social/'.$chave_nome_arquivo;
                            }
                        }
                    }
                    if($request->input('extensao_junta_comercial') != ''){
                        $controle_loop = true;
                        while($controle_loop){
                            $chave_nome_arquivo = md5($request->file('thumb_junta_comercial')->getClientOriginalName(). time()).'.'.$request->input('extensao_junta_comercial');
                            $count_registered = Client::where('junta_comercial', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                            if($count_registered == 0){
                                $controle_loop = false;
                                $request->file('thumb_junta_comercial')->storeAs('assets/junta_comercial/', $chave_nome_arquivo,'uploads');
                                $client->junta_comercial = 'assets/junta_comercial/'.$chave_nome_arquivo;
                            }
                        }
                    }
                    if($request->input('extensao_termo') != ''){
                        $controle_loop = true;
                        while($controle_loop){
                            $chave_nome_arquivo = md5($request->file('thumb_termo')->getClientOriginalName(). time()).'.'.$request->input('extensao_termo');
                            $count_registered = Client::where('termo', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                            if($count_registered == 0){
                                $controle_loop = false;
                                $request->file('thumb_termo')->storeAs('assets/termo_autorizacao/', $chave_nome_arquivo,'uploads');
                                $client->termo = 'assets/termo_autorizacao/'.$chave_nome_arquivo;
                            }
                        }
                    }
                }
            }
            $client->save();
            $msg = 'Pré-cadastro realizado com sucesso';
            return $msg;
        }else{
            return $msg;
        }
    }

    public function email_termo(Request $request){
        if ($this->sendResetEmail($request->email)) {
            $msg = array();
            array_push($msg, 'Um formulário contendo os termos de autorização foi enviado para o seu endereço de email.');
            $type = 'alert-success';
            return view('clients.termo_comprovante', compact('msg', 'type'));
        } else {
            $msg = array();
            array_push($msg, 'ERRO:');
            array_push($msg, '');
            array_push($msg, '');
            array_push($msg, 'Ocorreu um erro de internet. Por favor tente mais tarde.');
            $type = 'alert-danger';
            return view('clients.termo_comprovante', compact('msg', 'type'));
        }
    }

    private function sendResetEmail($email){
        try {
            Mail::to($email)
                ->send(new TermoAutorizacao($email));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function termo(){
        return view('clients.termo_comprovante');
    }

    public function show(Request $request)
    {
        $input = $request->get('input');
        $select = $request->get('select');
        $rent = $request->get('rent');
        $able_cli = true;
        if(strpos($select, '_del') !== false){
            $able_cli = false;
        }
        $select = str_replace('_del', '', $select);
        if($select == 'aprovado'){
            $clients = Client::where('aprovado', 0)->where('nome', 'like', '%'.$input.'%')->orWhere('nome_empresa', 'like', '%'.$input.'%')->orderBy('nome', 'asc')->paginate(10);
        }else{
            $clients = Client::query()->clientTrash($able_cli)->compareClients($input, $select)->clientSection(true);
        }
        if($rent == 1){
            $clients = Client::query()->clientTrash($able_cli)->compareClients($input, $select)->where('aprovado', 1)->clientSection(true);
        }
        if($rent == 1){
            return view('clients.table_clients', compact(['clients', 'able_cli', 'rent']))->render();
        }else{
            return view('clients.table_clients', compact(['clients', 'able_cli']))->render();
        }
    }


    public function autocomplete(Request $request)
    {
        $input = $request->input('input');
        $select = $request->input('select');
        $able_cli = true;
        if(strpos($select, '_del') !== false){
            $able_cli = false;
        }
        $select = str_replace('_del', '', $select);
        $clients = Client::query()->clientTrash($able_cli)->compareClients($input, $select)->clientSection(false);
        return $clients;
    }


    public function update(Request $request)
    {
        $client = Client::find($request->input('id'));
        if(isset($client)){
            $msg = '';
            if($request->input('cpf') != '' && $request->input('cpf') != $client->cpf){
                $count_registered = Client::where('cpf', $request->input('cpf'))->get()->count();
                if($count_registered > 0){
                    $msg .= 'CPF já cadastrado<br>';
                }
            }
            if($request->input('cnpj') != '' && $client->input('cnpj') != $client->cnpj){
                $count_registered = Client::where('cnpj', $request->input('cnpj'))->get()->count();
                if($count_registered > 0){
                    $msg .= 'CNPJ já cadastrado<br>';
                }
            }
            if($msg == ''){
                $client->nome = $request->input('nome');
                $client->email = $request->input('email');
                $client->endereco = $request->input('endereco');
                $client->bairro = $request->input('bairro');
                $client->tel_fixo = $request->input('tel_fixo');
                $client->tel_comercio = $request->input('tel_comercio');
                $client->tel_cel = $request->input('tel_cel');
                $client->observacoes = $request->input('observacoes');
                if($request->tipo_cadastro != 'Cadastro Simplificado'){
                    $client->cidade = $request->input('cidade');
                    $client->estado = $request->input('estado');
                    $client->cpf = $request->input('cpf');
                    $client->formas_pagamento = $request->input('formas_pagamento');
                    $client->tipo_cadastro = $request->input('tipo_cadastro');

                    if($request->tipo_cadastro == 'Pessoa Física'){
                        if($client->img_contrato != '' && $request->input('extensao') != ''){
                            unlink($client->img_contrato);
                        }
                        if($request->input('extensao') != ''){
                            $controle_loop = true;
                            while($controle_loop){
                                $chave_nome_arquivo = md5($request->file('thumb')->getClientOriginalName(). time()).'.'.$request->input('extensao');
                                $count_registered = Client::where('img_contrato', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                                if($count_registered == 0){
                                    $controle_loop = false;
                                    $request->file('thumb')->storeAs('assets/contratos_clientes/', $chave_nome_arquivo,'uploads');
                                    $client->img_contrato = 'assets/contratos_clientes/'.$chave_nome_arquivo;
                                }
                            }
                        }
                    }else{
                        $client->nome_empresa = $request->nome_empresa;
                        $client->email_empresa = $request->email_empresa;
                        $client->endereco_empresa = $request->endereco_empresa;
                        $client->bairro_empresa = $request->bairro_empresa;
                        $client->cidade_empresa = $request->cidade_empresa;
                        $client->estado_empresa = $request->estado_empresa;
                        $client->cnpj = $request->cnpj;
                        if($request->input('extensao_contrato_social') != ''){
                            $controle_loop = true;
                            while($controle_loop){
                                $chave_nome_arquivo = md5($request->file('thumb_contrato_social')->getClientOriginalName(). time()).'.'.$request->input('extensao_contrato_social');
                                $count_registered = Client::where('contrato_social', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                                if($count_registered == 0){
                                    $controle_loop = false;
                                    $request->file('thumb_contrato_social')->storeAs('assets/contrato_social/', $chave_nome_arquivo,'uploads');
                                    $client->contrato_social = 'assets/contrato_social/'.$chave_nome_arquivo;
                                }
                            }
                        }
                        if($request->input('extensao_junta_comercial') != ''){
                            $controle_loop = true;
                            while($controle_loop){
                                $chave_nome_arquivo = md5($request->file('thumb_junta_comercial')->getClientOriginalName(). time()).'.'.$request->input('extensao_junta_comercial');
                                $count_registered = Client::where('junta_comercial', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                                if($count_registered == 0){
                                    $controle_loop = false;
                                    $request->file('thumb_junta_comercial')->storeAs('assets/junta_comercial/', $chave_nome_arquivo,'uploads');
                                    $client->junta_comercial = 'assets/junta_comercial/'.$chave_nome_arquivo;
                                }
                            }
                        }
                        if($request->input('extensao_termo') != ''){
                            $controle_loop = true;
                            while($controle_loop){
                                $chave_nome_arquivo = md5($request->file('thumb_termo')->getClientOriginalName(). time()).'.'.$request->input('extensao_termo');
                                $count_registered = Client::where('termo', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                                if($count_registered == 0){
                                    $controle_loop = false;
                                    $request->file('thumb_termo')->storeAs('assets/termo_autorizacao/', $chave_nome_arquivo,'uploads');
                                    $client->termo = 'assets/termo_autorizacao/'.$chave_nome_arquivo;
                                }
                            }
                        }
                    }
                }
                $client->save();
                $msg .= 'Cliente atualizado com sucesso';
                return $msg;
            }else{
                return $msg;
            }
        }else{
            return 'ERRO:<br>Cliente não encontrado';
        }
    }


    public function destroy($id)
    {
        $client = Client::find($id);
        if(isset($client)){
            $client->delete();
            return 'Cliente desabilitado com sucesso';
        }else{
            return 'ERRO:<br>Cliente não encontrado';
        }
    }

    public function create($id)
    {
        $client = Client::onlyTrashed()->find($id);
        if(isset($client)){
            $client->restore();
            return 'Cliente habilitado com sucesso';
        }else{
            return 'ERRO:<br>Cliente não encontrado';
        }
    }

    public function aprovar_cliente($id)
    {
        $client = Client::find($id);
        if(isset($client)){
            $client->aprovado = 1;
            $client->save();
            return 'Cliente aprovado com sucesso';
        }else{
            return 'ERRO:<br>Cliente não encontrado';
        }
    }
}
