<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        $products = Product::orderBy('name', 'asc')->paginate(10);
        $able_prod = true;
        return view('products.products', compact(['products', 'able_prod']));
    }

    public function store(Request $request)
    {
        $msg = '';
        $count_registered = Product::where('name', $request->input('name'))->get()->count();
        if($count_registered > 0){
            $msg .= 'Produto já cadastrado<br>';
        }
        if($msg != ''){
            return 'ERROS<br>'.$msg;
        }else{
            $product = new Product();
            $product->name = $request->input('name');
            $product->units = $request->input('units');
            $product->qty_stock = $request->input('qty_stock');
            $product->price_cost = float_format($request->input('price_cost'));
            $product->daily_value = float_format($request->input('daily_value'));
            $product->is_rent = $request->input('is_rent');
            if($request->input('thumb') != NULL){
                $product->thumb = 'assets/sem_img.png';
            }else{
                $controle_loop = true;
                while($controle_loop){
                    $chave_nome_arquivo = md5($request->file('thumb')->getClientOriginalName(). time()).'.'.$request->input('extensao');
                    $count_registered = Product::where('thumb', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                    if($count_registered == 0){
                        $controle_loop = false;
                        $request->file('thumb')->storeAs('assets/products', $chave_nome_arquivo,'uploads');
                        $product->thumb = 'assets/products/'.$chave_nome_arquivo;
                    }
                }
            }
            $product->save();
            $msg = 'Produto cadastrado com sucesso';
            return $msg;
        }
    }

    public function show(Request $request)
    {
        $input = $request->get('input');
        $select = $request->get('select');
        $rent = $request->get('rent');
        $able_prod = true;
        if(strpos($select, '_del') !== false){
            $able_prod = false;
        }
        $select = str_replace('_del', '', $select);
        $products = Product::query()->productTrash($able_prod)->compareProducts($input, $select)->productSection(true);
        if($rent == 1){
            return view('products.table_products', compact(['products', 'able_prod', 'rent']))->render();
        }else{
            return view('products.table_products', compact(['products', 'able_prod']))->render();
        }
    }

    public function autocomplete(Request $request)
    {
        $input = $request->get('input');
        $select = $request->get('select');
        $able_prod = true;
        if(strpos($select, '_del') !== false){
            $able_prod = false;
        }
        $select = str_replace('_del', '', $select);
        $products = Product::query()->productTrash($able_prod)->compareProducts($input, $select)->productSection(false);
        return $products;
    }

    public function update(Request $request)
    {
        $product = Product::find($request->input('id'));
        $msg = '';
        if(isset($product)){
            if(strtoupper($request->input('name')) != strtoupper($product->name)){
                $count_registered = Product::where('name', $request->input('name'))->get()->count();
                if($count_registered > 0){
                    $msg .= 'Produto já cadastrado<br>';
                }
            }
            if($msg == ''){
                $product->name = $request->input('name');
                $product->units = $request->input('units');
                $product->qty_stock = $request->input('qty_stock');
                $product->price_cost = float_format($request->input('price_cost'));
                $product->daily_value = float_format($request->input('daily_value'));
                $product->is_rent = $request->input('is_rent');
                if($request->input('thumb') == NULL){
                    if($product->thumb != 'assets/sem_img.png'){
                        unlink($product->thumb);
                    }
                    $controle_loop = true;
                    while($controle_loop){
                        $chave_nome_arquivo = md5($request->file('thumb')->getClientOriginalName(). time()).'.'.$request->input('extensao');
                        $count_registered = Product::where('thumb', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                        if($count_registered == 0){
                            $controle_loop = false;
                            $request->file('thumb')->storeAs('assets/products', $chave_nome_arquivo,'uploads');
                            $product->thumb = 'assets/products/'.$chave_nome_arquivo;
                        }
                    }
                }
                $product->save();
                $msg = 'Produto atualizado com sucesso';
                return $msg;
            }else{
                return "ERROS:<br>".$msg;
            }
        }else{
            return 'ERRO:<br>Produto não encontrado';
        }
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if(isset($product)){
            $product->delete();
            return 'Produto desabilitado com sucesso';
        }else{
            return 'ERRO:<br>Produto não encontrado';
        }
    }

    public function create($id)
    {
        $product = Product::onlyTrashed()->find($id);
        if(isset($product)){
            $product->restore();
            return 'Produto habilitado com sucesso';
        }else{
            return 'ERRO:<br>Produto não encontrado';
        }
    }
}
