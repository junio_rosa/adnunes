<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $token;

    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }


    public function build()
    {
        return $this->from('suporteadnuneslocacao@gmail.com')
                    ->to($this->user->email)
                    ->subject('Redefinir senha no site ADNUNES')
                    ->markdown('reset-email')
                    ->with([
                        'user' => $this->user,
                        'token' => $this->token
                    ]);
    }
}
