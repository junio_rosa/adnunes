<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rent_Details extends Model
{
    use HasFactory;

    protected $table = 'rent_details';
}
