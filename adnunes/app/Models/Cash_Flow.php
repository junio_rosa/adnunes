<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cash_Flow extends Model
{
    use HasFactory;

    protected $table = 'cash_flow';

    protected $casts = [
        'date' => 'date:Y-m-d'
    ];

    function client_name(){
        return $this->hasOne(Rent::class, 'id', 'rent_id')
                    ->join('clients', 'rents.id_client', '=', 'clients.id')
                    ->select('clients.nome as client_name');
    }

    function scopeDateInterval($query, $init_date, $ending_date){
        if($init_date != '' && $ending_date != ''){
            return $query->whereBetween('date', [$init_date, $ending_date]);
        }else{
            if($init_date != ''){
                return $query->whereDate('date', $init_date);
            }
            if($ending_date != ''){
                return $query->whereDate('date', $ending_date);
            }
        }
    }

    function rent(){
        return $this->hasOne(Rent::class, 'id', 'rent_id');
    }
}
