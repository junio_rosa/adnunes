<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    use HasFactory;

    protected $casts = [
        'devolution_date' => 'date:Y-m-d',
        'rent_date' => 'date:Y-m-d'
    ];

    function scopeCompareRentClients($query, $input, $select){
        if($select == 'address'){
            $select = 'rents.address';
        }
		if($select == 'nome'){
			return $query->join('clients', 'clients.id', '=', 'rents.id_client')
						->where('clients.nome', 'like', '%'.$input.'%')
						->orwhere('clients.nome_empresa', 'like', '%'.$input.'%')
						->orderBy('nome', 'asc')
						->select('clients.nome', 'clients.nome_empresa', 'clients.tipo_cadastro', 'clients.cpf', 'clients.cnpj', 'rents.*');
		}else{
			if($select == 'cpf'){
				return $query->join('clients', 'clients.id', '=', 'rents.id_client')
							->where('clients.cpf', 'like', '%'.$input.'%')
							->orderBy('nome', 'asc')
							->select('clients.nome', 'clients.cpf', 'clients.cnpj', 'rents.*');
			}else{
				return $query->join('clients', 'clients.id', '=', 'rents.id_client')
							->where('clients.cnpj', 'like', '%'.$input.'%')
							->orderBy('nome', 'asc')
							->select('clients.nome', 'clients.cpf', 'clients.cnpj', 'rents.*');
			}
		}
    }

    function scopeDateInterval($query, $init_date, $ending_date){
        if($init_date != '' && $ending_date != ''){
            return $query->whereBetween('devolution_date', [$init_date, $ending_date]);
        }else{
            if($init_date != ''){
                return $query->whereDate('devolution_date', $init_date);
            }
            if($ending_date != ''){
                return $query->whereDate('devolution_date', $ending_date);
            }
        }
    }

    function scopeReportType($query, $report_type){
        $type = 'rents.status';
        if($report_type != 'vazio'){
            if($report_type == 'quitar'){
                $report_type = 'A quitar';
            }
            if($report_type == 'quitados'){
                $report_type = 'Quitado';
            }
            if($report_type == 'baixa'){
                $report_type = 'Baixa';
            }
            if($report_type == 'vencidos'){
                $type = 'validation';
                $report_type = 'Venceu';
            }
            return $query->where($type, $report_type);
        }
    }

    function scopeRentSection($query, $section){
        if(!$section){
            return $query->limit(10)->get();
        }else{
            return $query->paginate(10);
        }
    }

    function client(){
        return $this->belongsTo(Client::class, 'id_client')
                    ->join('rents', 'rents.id_client', '=', 'clients.id')
                    ->select('clients.tipo_cadastro', 'clients.nome_empresa', 'clients.nome as client_name', 'clients.endereco as client_address', 'clients.bairro as client_neighborhood',
                    'clients.tel_fixo as client_phone1', 'clients.tel_comercio as client_phone2', 'clients.tel_cel as client_phone3',
                    'clients.cidade as client_city', 'clients.estado as client_state', 'clients.email as client_email');
    }

    function rent_details(){
        return $this->hasMany(Rent_Details::class, 'rent_id')
                    ->join('rents', 'rents.id', '=', 'rent_details.rent_id')
                    ->join('products', 'products.id', '=', 'rent_details.product_id')
                    ->select('rent_details.*', 'products.thumb', 'products.qty_stock', 'products.name', 'products.units', 'products.price_cost', 'products.is_rent');
    }
}
