<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equip_Out extends Model
{
    use HasFactory;

    protected $table = 'equip_out';

    protected $casts = [
        'date' => 'date:Y-m-d'
    ];

    function client(){
        return $this->hasOne(Client::class, 'id', 'client_id')->withTrashed();
    }

    function product(){
        return $this->hasOne(Product::class, 'id', 'product_id')->withTrashed();
    }

    function rent(){
        return $this->hasOne(Rent::class, 'id', 'rent_id');
    }

    function scopeDateInterval($query, $init_date, $ending_date){
        if($init_date != '' && $ending_date != ''){
            return $query->whereBetween('date', [$init_date, $ending_date]);
        }else{
            if($init_date != ''){
                return $query->whereDate('date', $init_date);
            }
            if($ending_date != ''){
                return $query->whereDate('date', $ending_date);
            }
        }
    }
}
