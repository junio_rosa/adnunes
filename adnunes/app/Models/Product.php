<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;

    use SoftDeletes;

    function scopeCompareProducts($query, $input, $select){
        return $query->where($select, 'like', '%'.$input.'%')
                    ->orderBy('name', 'asc');
    }

    function scopeId_rents($query, $id){
        return $query->where('products.id', $id)
                    ->where('rents.status', '<>', 'Baixa')
                    ->join('rent_details', 'rent_details.product_id', '=', 'products.id')
                    ->join('rents', 'rent_details.rent_id', '=', 'rents.id')
                    ->join('clients', 'rents.id_client', '=', 'clients.id')
                    ->select('rents.id', 'rent_details.qty', 'rent_details.return_qty', 'clients.nome')
                    ->get();
    }

    function scopeSumQty($query, $id){
        return $query->where('products.id', $id)
                    ->where('rents.status', '<>', 'Baixa')
                    ->join('rent_details', 'rent_details.product_id', '=', 'products.id')
                    ->join('rents', 'rent_details.rent_id', '=', 'rents.id')
                    ->sum('rent_details.qty');
    }

    function scopeSumReturnQty($query, $id){
        return $query->where('products.id', $id)
                    ->where('rents.status', '<>', 'Baixa')
                    ->join('rent_details', 'rent_details.product_id', '=', 'products.id')
                    ->join('rents', 'rent_details.rent_id', '=', 'rents.id')
                    ->sum('rent_details.return_qty');
    }

    function scopeProductTrash($query, $able_prod){
        if(!$able_prod){
            return $query->onlyTrashed();
        }
    }

    function scopeProductSection($query, $section){
        if(!$section){
            return $query->limit(10)->get();
        }else{
            return $query->paginate(10);
        }
    }
}
