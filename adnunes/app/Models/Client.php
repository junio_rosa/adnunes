<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory;

    use SoftDeletes;

    function scopeCompareClients($query, $input, $select){
        if($select != 'cpf' && $select != 'cnpf'){
            return $query->where($select, 'like', '%'.$input.'%')
                    ->orWhere($select.'_empresa', 'like', '%'.$input.'%')
                    ->orderBy('nome', 'asc');
        }else{
            return $query->where($select, 'like', '%'.$input.'%')
                    ->orderBy('nome', 'asc');
        }
    }

    function scopeClientTrash($query, $able_cli){
        if(!$able_cli){
            return $query->onlyTrashed();
        }
    }

    function scopeClientSection($query, $section){
        if(!$section){
            return $query->limit(10)->get();
        }else{
            return $query->paginate(10);
        }
    }
}
