<?php
if (!function_exists('float_format')) {
    function float_format($number){
        $number = str_replace('.', '', $number);
        $number = str_replace(',','.', $number);
        return floatval($number);
    }
}
?>
