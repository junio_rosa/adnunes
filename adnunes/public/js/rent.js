/* ABRIR MODAL ALUGUEL PARA CADASTRO OU EDIÇÃO */
function open_rent_modal(obj, client, rent_details){
    if(obj == 0){
        var array_input = [
            'rent_modal_cliDevolutionDate', 'rent_modal_cliHour', 'rent_modal_cliRentDate',
            'rent_modal_cliAddress', 'rent_modal_cliTotalPrice'];
        var array_small = [{
            small: 'rent_small_cliDevolutionDate',
            msg: 'Digite uma data de devolução para o pedido'
        }, {
            small: 'rent_small_cliHour',
            msg: 'Digite o horário que o pedido foi realizado'
        },{
            small: 'rent_small_cliRentDate',
            msg: 'Digite a data que o pedido foi realizado'
        },{
            small: 'rent_small_cliAddress',
            msg: 'Digite o endereço de entrega'
        },{
            small: 'rent_small_cliTotalPrice',
            msg: 'Digite o valor total do pedido'
        }];
        clear_modal_fields(array_input, array_small);
        document.getElementById('rent_modal_cliTotalDaily').value = 0;
        document.getElementById('rent_modal_title').innerHTML = 'Pedido';
        document.getElementById('rent_modal_rentDeliveryValue').value = 0;
        document.getElementById('rent_modal_rentDeliveryValue').readOnly = false;
        document.getElementById('desconto').value = 0;
        document.getElementById('desconto').readOnly = false;
        document.getElementById('rent_modal_cliTotalPrice').value = 0;
        document.getElementById('rent_modal_cliTotalPrice').readOnly = false;
        document.getElementById('rent_modal_cliPreviousPaid').value = 0;
        document.getElementById('rent_modal_cliPreviousPaid').readOnly = false;
        document.getElementById('rent_modal_button').innerHTML = '<i class="fas fa-plus-circle text-white"></i> Cadastrar';
        document.getElementById('form_group_rent_paid_value').classList.add('d-none');
        document.getElementById('form_group_rent_total_price').classList.remove('col-lg-4');
        document.getElementById('form_group_rent_total_price').classList.add('col-lg-6');
        document.getElementById('form_group_rent_previous_paid').classList.remove('col-lg-4');
        document.getElementById('form_group_rent_previous_paid').classList.add('col-lg-6');
        document.getElementById('rent_modal_cliHour').readOnly = false;
        document.getElementById('rent_modal_cliRentDate').readOnly = false;
        document.getElementById('rent_modal_cliDevolutionDate').value = '';
        document.getElementById('rent_modal_cliDevolutionDate').readOnly = false;
        document.getElementById('rent_modal_cliAddress').readOnly = false;
        document.getElementById('rent_modal_button').classList.remove('d-none');
    }else{
        $('#rent_table>tbody>tr').remove();
        document.getElementById('rent_modal_rentId').value = obj.id;
        document.getElementById('rent_modal_rentStatus').value = obj.status;
        document.getElementById('rent_modal_cliId').value = obj.id_client;
        document.getElementById('rent_modal_cliName').value = client.client_name;
        document.getElementById('rent_modal_cliAddress').value = obj.address;
        document.getElementById('rent_modal_cliDevolutionDate').value = obj.devolution_date;
        document.getElementById('rent_modal_cliHour').value = obj.hour;
        document.getElementById('rent_modal_cliRentDate').value = obj.rent_date;
        document.getElementById('rent_modal_cliHour').readOnly = true;
        document.getElementById('rent_modal_cliRentDate').readOnly = true;
        document.getElementById('rent_modal_cliTotalPrice').value = number_format(obj.total_price, 2, ',', '.');
        document.getElementById('rent_modal_cliPreviousPaid').value = 0;
        $('#rent_modal_cliAuthorizedWorkers').jqteVal(obj.authorized_workers);
        document.getElementById('rent_modal_cliTotalDaily').value = number_format(obj.total_daily, 2, ',', '.');
        document.getElementById('rent_modal_rentDeliveryValue').value = number_format(obj.delivery_value, 2, ',', '.');
        document.getElementById('desconto').value = number_format(obj.desconto, 2, ',', '.');
        document.getElementById('rent_modal_title').innerHTML = 'Editar Pedido';
        document.getElementById('rent_modal_button').innerHTML = '<i class="fas fa-sync text-white"></i> Atualizar';
        document.getElementById('form_group_rent_paid_value').classList.remove('d-none');
        document.getElementById('rent_modal_cliPaidValue').value = number_format(obj.previous_paid, 2, ',', '.');
        document.getElementById('form_group_rent_total_price').classList.remove('col-lg-6');
        document.getElementById('form_group_rent_total_price').classList.add('col-lg-4');
        document.getElementById('form_group_rent_previous_paid').classList.remove('col-lg-6');
        document.getElementById('form_group_rent_previous_paid').classList.add('col-lg-4');
        rent_form_validate();
        $('#rent_table>thead').remove();
        thead = '<thead><tr class="text-center">'
                +'<th>Código</th>'
                +'<th>Produto</th>'
                +'<th>Nome</th>'
                +'<th>Tipo do produto</th>'
                +'<th>Estoque</th>'
                +'<th>Diária</th>'
                +'<th>Qtde Alugada/Vendida</th>'
                +'<th>Qtde Já Devolvida</th>'
                +'<th>Prod Devolvidos</th>'
                +'</tr></thead>';
        $('#rent_table').append(thead);
        i=0;
        while(i < rent_details.length){
            row = rent_details_row(rent_details[i]);
            insert_table_row('rent_table', row, rent_details[i].name, 2);
            i++;
        }
        rent_discharge();
        $('#rent_modal').modal();
    }
}

/* LINHA DA TABELA DE ALUGUEL */
function rent_details_row(rent_details){
    var type_product;
    if(rent_details.is_rent){
        type_product = 'Aluguel';
    }else{
        type_product = 'Venda';
    }
    max_value = rent_details.qty - rent_details.return_qty;
    var row = "<tr class=\"text-center\">"
                    +"<td class='align-middle'>"+rent_details.product_id+"</td>"
                    +"<td><img class= 'rounded' src='"+rent_details.thumb+"' style='max-height:100px; width:auto'></td>"
                    +"<td class='align-middle'>"+rent_details.name+"</td>"
                    +"<td class='align-middle'>"+type_product+"</td>"
                    +"<td class='align-middle'>"+rent_details.qty_stock+"</td>"
                    +"<td class='align-middle'>"+number_format(rent_details.daily, 2,',','.')+"</td>"
                    +"<td class='align-middle'>"+rent_details.qty+"</td>"
                    +"<td class='align-middle'>"+rent_details.return_qty+"</td>"
    if(rent_details.is_rent == 0){
        row += "<td class='align-middle'>"
            +"Produto para venda"
            +"</td>"
            +"</tr>";
    }else{
        row +=  "<td class='align-middle'>"
                +"<input type='number' class='border-0 text-center' value=0 max='"+max_value+"' onkeypress=\"event.preventDefault()\">"
                +"</td>"
                +"</tr>";
    }
    return row;
}

/* AUTOCOMPLETE CLIENTE*/
$('#rent_cli_input_search').keyup(function(event){
    input = $(this).val();
    select = document.getElementById('rent_cli_select_search').value;
    if (event.keyCode === 13) {
        fetch_data_client('rent_cli_div_autocomplete', 'rent_cli_input_pagination', 'rent_cli_input_search', 'rent_cli_select_search', 'section_rent_clients_table', 1);
        reset_rent_cliId();
    }
    else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('clients/autocomplete', search, function(client){
                document.getElementById('rent_cli_div_autocomplete').innerHTML ='';
                if(client.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#rent_cli_div_autocomplete').append(data);
                    client.forEach(function(client){
                        cpf_cnpj = '';
                        if(client.cpf != null){
                        if(client.cpf.length > 0){
                            cpf_cnpj += '<strong>CPF: </strong>'+client.cpf+' ';
                        }}
                        if(client.cnpj != null){
                        if(client.cnpj.length > 0){
                            cpf_cnpj += '<strong>CNPJ: </strong>'+client.cnpj;
                        }}
                        if(client.tipo_cadastro == 'Pessoa Física' || client.tipo_cadastro == 'Cadastro Simplificado'){
                            data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\"><strong>Nome:</strong> "
                                    +client.nome+" <strong>Endereço: </strong>"
                                    +client.endereco+' '+cpf_cnpj+"</button></li>");
                        }else{
                            data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\"><strong>Nome:</strong> "
                                    +client.nome_empresa+" <strong>Endereço: </strong>"
                                    +client.endereco_empresa+' '+cpf_cnpj+"</button></li>");
                        }
                        data.find('button').click(function() {
                            fill_cli_searchInput(client, 'rent_cli_input_search', 'rent_cli_select_search', 'rent_cli_div_autocomplete');
                        });
                        $('#rent_cli_div_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#rent_cli_div_autocomplete').append(data);
                    $('#rent_cli_div_autocomplete').fadeIn();
                }
            });
        }
    }
});

/* AUTOCOMPLETE PRODUTO*/
$('#rent_prod_input_search').keyup(function(event){
    input = $(this).val();
    select = document.getElementById('rent_prod_select_search').value;
    if (event.keyCode === 13) {
        fetch_data_product('rent_prod_div_autocomplete', 'rent_prod_input_pagination', 'rent_prod_input_search', 'rent_prod_select_search', 'section_rent_products_table', 1);
    }
    else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('products/autocomplete', search, function(product){
                document.getElementById('rent_prod_div_autocomplete').innerHTML ='';
                if(product.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#rent_prod_div_autocomplete').append(data);
                    product.forEach(function(product){
                        data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\">"
                                    +"<img class= 'rounded' src='"+product.thumb+"' style='height:30px; width:auto;'> "
                                    +"<strong>Nome:</strong> "
                                    +product.name+" <strong>Estoque: </strong>"
                                    +product.qty_stock+' <strong>Diária: </strong>'
                                    +number_format(product.daily_value, 2, ',', '.')+"</button></li>");
                        data.find('button').click(function() {
                            fill_prod_searchInput(product, 'rent_prod_input_search', 'rent_prod_select_search', 'rent_prod_div_autocomplete');
                        });
                        $('#rent_prod_div_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#rent_prod_div_autocomplete').append(data);
                    $('#rent_prod_div_autocomplete').fadeIn();
                }
            });
        }
    }
});

/* AVANÇAR PARA PRODUTOS */
function rent_next(){
    var id = document.getElementById('rent_cli_id').value;
    if(id != ''){
        document.getElementById('rent_control_pagination').value = 2;
        $('#section_rent_clients_table').fadeOut();
        $('#section_table_rent_reports').fadeOut();
        $('#section_rent_products_table').fadeIn();
        document.getElementById('section_rent_searchClient').classList.add('d-none');
        document.getElementById('section_rent_reports').classList.add('d-none');
        document.getElementById('section_rent_searchProduct').classList.remove('d-none');
    }else{
        document.getElementById('snackbar5').innerHTML = 'ERRO:<br><br>Selecione um cliente na tabela de clientes<br> para prosseguir com o pedido de aluguel';
        show_snackbar('snackbar5');
    }
}

/* VOLTAR PARA CLIENTES */
function rent_previous(){
    $('#section_rent_clients_table').fadeIn();
    $('#section_rent_products_table').fadeOut();
    $('#section_table_rent_reports').fadeOut();
    document.getElementById('rent_control_pagination').value = 1;
    document.getElementById('section_rent_searchClient').classList.remove('d-none');
    document.getElementById('section_rent_searchProduct').classList.add('d-none');
    document.getElementById('section_rent_reports').classList.add('d-none');
    open_rent_modal(0, '', '');
    $('#rent_table>thead').remove();
        thead = '<thead><tr class="text-center">'
                +'<th>Código</th>'
                +'<th>Produto</th>'
                +'<th>Nome</th>'
                +'<th>Tipo de produto</th>'
                +'<th>Estoque</th>'
                +'<th>Diária</th>'
                +'<th>Qtde Alugada/Vendida</th>'
                +'<th>Ações</th>'
                +'</tr></thead>';
        $('#rent_table').append(thead);
    $('#rent_table>tbody>tr').remove();
}

/* SESSÃO DE RELATÓRIOS  */
function rent_reports(){
    document.getElementById('section_rent_searchClient').classList.add('d-none');
    document.getElementById('section_rent_searchProduct').classList.add('d-none');
    document.getElementById('section_rent_reports').classList.remove('d-none');
    $('#section_rent_clients_table').fadeOut();
    $('#section_table_rent_reports').fadeIn();
    $('#section_rent_products_table').fadeOut();
    document.getElementById('rent_control_pagination').value = 3;
    $('#rent_table>tbody>tr').remove();
}

/* SELECIONA ID DO CLIENTE PARA REALIZAR ALUGUEL */
function select_client(client){
    var previous_tr = document.getElementById('rent_cli_id').value;
    var previous_status = document.getElementById('rent_cli_status').value;
    document.getElementById('tr_cli'+client.id).classList.remove('bg-aproved');
    document.getElementById('tr_cli'+client.id).classList.remove('bg-pending');
    document.getElementById('tr_cli'+client.id).classList.add('bg-dark');
    document.getElementById('tr_cli'+client.id).classList.add('text-white');
    if(previous_tr != '' && previous_tr != client.id){
        document.getElementById('tr_cli'+previous_tr).classList.remove('bg-dark');
        document.getElementById('tr_cli'+previous_tr).classList.remove('text-white');
        if(previous_status == 'Aprovado'){
            document.getElementById('tr_cli'+previous_tr).classList.add('bg-aproved');
        }else{
            document.getElementById('tr_cli'+previous_tr).classList.add('bg-pending');
        }
    }
    document.getElementById('rent_cli_id').value = client.id;
    document.getElementById('rent_cli_status').value = client.status;
    open_rent_modal(0, '', '');
    document.getElementById('rent_modal_rentStatus').value = 'A quitar';
    document.getElementById('rent_modal_cliId').value = client.id;
    if(client.tipo_cadastro == 'Pessoa Física'){
        document.getElementById('rent_modal_cliName').value = client.nome;
    }else{
        document.getElementById('rent_modal_cliName').value = client.nome_empresa;
    }
    document.getElementById('rent_modal_cliAddress').value = '';
    $('#rent_modal_cliAuthorizedWorkers').jqteVal(client.observacoes);
    $('#rent_table>thead').remove();
        thead = '<thead><tr class="text-center">'
                +'<th>Código</th>'
                +'<th>Produto</th>'
                +'<th>Nome</th>'
                +'<th>Tipo de produto</th>'
                +'<th>Estoque</th>'
                +'<th>Diária</th>'
                +'<th>Qtde Alugada/Vendida</th>'
                +'<th>Ações</th>'
                +'</tr></thead>';
        $('#rent_table').append(thead);
    $('#rent_table>tbody>tr').remove();
}

/* ZERAR TR PARA SELEÇÃO DE CLIENTES PARA REALIZAR O ALUGUEL */
function reset_rent_cliId(){
    document.getElementById('rent_cli_id').value = '';
    document.getElementById('rent_cli_status').value = '';
}

function rent_preview(){
    data_pedido = document.getElementById('rent_modal_cliRentDate').value;
    if(data_pedido == ''){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = yyyy + '-' + mm + '-' + dd;

        document.getElementById('rent_modal_cliRentDate').value = today;
        field_date_validation('rent_modal_cliRentDate', 'rent_small_cliRentDate', 'Data Válida', 'Data Inválida');
    }
    $('#rent_modal').modal();
}

function rent_modal_addProducts(product){
    var table = $('#rent_table>tbody>tr');
    var i = 0;
    var qtde = 0;
    while(i < table.length){
        id = table[i].cells[0].textContent;
        if(id == product.id){
            qtde = qtde + parseInt(table[i].cells[6].textContent);
            break;
        }
        i++;
    }
    document.getElementById('rent_input_addProducts').value = qtde;
    document.getElementById('rent_previousQty_addProducts').value = qtde;
    document.getElementById('rent_qtyStock_addProducts').value = product.qty_stock;
    document.getElementById('rent_idProducts_addProducts').value = product.id;
    document.getElementById('rent_thumb_addProducts').value = product.thumb;
    document.getElementById('rent_name_addProducts').value = product.name;
    if(product.is_rent){
        document.getElementById('rent_typeProduct_addProducts').value = 'Aluguel';
    }else{
        document.getElementById('rent_typeProduct_addProducts').value = 'Venda';
    }
    document.getElementById('rent_daily_addProducts').value = product.daily_value;
    if(parseInt(qtde) >= parseInt(product.qty_stock)){
        document.getElementById('rent_msg_addProducts').classList.add('text-danger');
        document.getElementById('rent_msg_addProducts').classList.remove('text-primary');
        document.getElementById('rent_msg_addProducts').innerHTML = 'Não é possível adicionar mais produtos '+product.name+' no pedido';
    }else{
        document.getElementById('rent_msg_addProducts').classList.remove('text-danger');
        document.getElementById('rent_msg_addProducts').classList.add('text-primary');
        document.getElementById('rent_msg_addProducts').innerHTML = 'Digite a quantidade do produto '+product.name+' que você deseja adicionar no pedido';
    }
    $('#rent_add_products').modal();
}

function rent_addProduct(){
    var stock = document.getElementById('rent_qtyStock_addProducts').value;
    var qty = document.getElementById('rent_input_addProducts').value;
    if(parseInt(qty) <= 0 || qty == ''){
        document.getElementById('snackbar7').innerHTML = 'Digite uma quantidade maior que 0<br> para adicionar o produto no pedido';
        show_snackbar('snackbar7');
    }else{
        qty = parseInt(qty);
        stock = parseInt(stock);
        if(qty > stock){
            document.getElementById('snackbar7').innerHTML = 'Não foi possível adicionar o produto no pedido,<br>pois o estoque é somente de '+stock+' unidades';
            show_snackbar('snackbar7');
        }else{
            var i = 0;
            id_product = document.getElementById('rent_idProducts_addProducts').value;
            name_product = document.getElementById('rent_name_addProducts').value;
            total_daily = document.getElementById('rent_modal_cliTotalDaily').value;
            total_daily = convert_float(total_daily);
            daily = document.getElementById('rent_daily_addProducts').value;
            daily = parseFloat(daily);
            previousQty = document.getElementById('rent_previousQty_addProducts').value;
            previousQty = parseInt(previousQty);
            total_daily = total_daily - (daily * previousQty);
            total_daily = total_daily + (daily * qty);
            document.getElementById('rent_modal_cliTotalDaily').value = number_format(total_daily, 2, ',', '.');
            var table = $('#rent_table>tbody>tr');
            while (i < table.length) {
                var id_cell = table[i].cells[0].textContent;
                if(id_cell == id_product){
                    delete_table_row('rent_table', id_product);
                    break;
                }
                i++;
            }
            var row = rent_product_row();
            insert_table_row('rent_table', row, name_product, 2);
            document.getElementById('snackbar7').innerHTML = 'Produto adicionado com sucesso';
            show_snackbar('snackbar7');
        }
    }
}

/* LINHA DA TABELA DE ALUGUEL */
function rent_product_row(){
    var row = "<tr class=\"text-center\">"
                    +"<td class='align-middle'>"+document.getElementById('rent_idProducts_addProducts').value+"</td>"
                    +"<td><img class= 'rounded' src='"+document.getElementById('rent_thumb_addProducts').value+"' style='max-height:100px; width:auto'></td>"
                    +"<td class='align-middle'>"+document.getElementById('rent_name_addProducts').value+"</td>"
                    +"<td class='align-middle'>"+document.getElementById('rent_typeProduct_addProducts').value+"</td>"
                    +"<td class='align-middle'>"+parseInt(document.getElementById('rent_qtyStock_addProducts').value)+"</td>"
                    +"<td class='align-middle'>"+number_format(parseFloat(document.getElementById('rent_daily_addProducts').value),2,',','.')+"</td>"
                    +"<td class='align-middle'>"+parseInt(document.getElementById('rent_input_addProducts').value)+"</td>"
                    +"<td class='align-middle'>"
                        +"<button class=\"btn btn-sm btn-danger mt-1\" "
                                    +"onclick=\"deleted_product('rent_table', "+
                                        document.getElementById('rent_idProducts_addProducts').value
                                    +")\">"
                            +"<i class=\"fas fa-trash\"></i> Remover"
                        "</button>"
                    +"</td>"
                    +"</tr>";
    return row;
}

/* FUNÇÃO PARA DELETAR PRODUTO DO PEDIDO (TEM QUE ALTERAR OS VALORES MENSAIS E DIÁRIOS) */
function deleted_product(table_name, id){
    table = $('#'+table_name+'>tbody>tr');
    i = 0;
    while(i < table.length){
        id_table = table[i].cells[0].textContent;
        if(id_table == id){
            daily = table[i].cells[4].textContent;
            daily = convert_float(daily);
            qty = parseFloat(table[i].cells[5].textContent);
            total_daily = document.getElementById('rent_modal_cliTotalDaily').value;
            total_daily = convert_float(total_daily);
            total_daily = total_daily - (qty * daily);
            document.getElementById('rent_modal_cliTotalDaily').value = number_format(total_daily, 2, ',', '.');
            break;
        }
        i++;
    }
    delete_table_row(table_name, id);
    document.getElementById('snackbar6').innerHTML = 'Produto deletado com sucesso';
    show_snackbar('snackbar6');
}

/* VALIDADOR DE FORMULÁRIO */
function rent_form_validate(){
    field_date_validation('rent_modal_cliDevolutionDate', 'rent_small_cliDevolutionDate', 'Data Válida', 'Data Inválida');
    hour_validation('rent_modal_cliHour', 'rent_small_cliHour', 'Campo Validado', 'Campo Hora Inválido');
    field_date_validation('rent_modal_cliRentDate', 'rent_small_cliRentDate', 'Data Válida', 'Data Inválida');
    field_validation('rent_modal_cliAddress', 'rent_small_cliAddress', 'Campo Validado', 'Campo endereço inválido', 1);
    money_validation('rent_modal_cliTotalPrice', 'rent_small_cliTotalPrice', 'Campo Validado', 'Campo Valor Total Inválido');
}

/* FORMULÁRIO DE ALUGUEL É VÁLIDO */
function rent_form_isValid(){
    control = true;
    msg = '';
    rent_form_validate();
    devolution_date = document.getElementById('rent_small_cliDevolutionDate').innerHTML;
    hour = document.getElementById('rent_small_cliHour').innerHTML;
    rent_date = document.getElementById('rent_small_cliRentDate').innerHTML;
    address = document.getElementById('rent_small_cliAddress').innerHTML;
    total_price = document.getElementById('rent_small_cliTotalPrice').innerHTML;
    table = $('#rent_table>tbody>tr');
    if(devolution_date != 'Data Válida'){
        msg += devolution_date + '<br>';
    }
    if(hour != 'Campo Validado'){
        msg += hour + '<br>';
    }
    if(rent_date != 'Data Válida'){
        msg += rent_date + '<br>';
    }
    if(address != 'Campo Validado'){
        msg += address + '<br>';
    }
    if(total_price != 'Campo Validado'){
        msg += total_price + '<br>';
    }
    if(table.length == 0){
        msg += 'Não existe produto associado a esse pedido';
    }
    if(msg.length > 0){
        document.getElementById('snackbar6').innerHTML = 'ERROS<br><br>'+msg;
        show_snackbar('snackbar6');
        control = false;
    }
    return control;
}

function register_rent(){
    if(rent_form_isValid()){
        if(compare_dates()){
            previous_paid = document.getElementById('rent_modal_cliPreviousPaid').value;
            delivery_value = document.getElementById('rent_modal_rentDeliveryValue').value;
            desconto = document.getElementById('desconto').value;
            if(desconto == ''){
                desconto = 0;
            }else{
                desconto = convert_float(desconto);
            }
            if(delivery_value == ''){
                delivery_value = 0;
            }else{
                delivery_value = convert_float(delivery_value);
            }
            if(previous_paid == ''){
                previous_paid = 0;
            }else{
                previous_paid = convert_float(previous_paid);
            }
            total_price = convert_float(document.getElementById('rent_modal_cliTotalPrice').value) + delivery_value - desconto;
            if(previous_paid > total_price){
                document.getElementById('snackbar6').innerHTML = 'ERROS:<br><br>Não é possível realizar um pedido, em que <br>o valor já acertado é maior que o valor total.';
                show_snackbar('snackbar6');
            }else{
                var table = $('#rent_table>tbody>tr');
                var products = [];
                i = 0;
                while (i < table.length) {
                    var obj = {
                        product_id : table[i].cells[0].textContent,
                        qty : table[i].cells[6].textContent,
                        daily: table[i].cells[5].textContent,
                    }
                    products.push(obj);
                    i++;
                }
                array = {
                    id_client: document.getElementById('rent_modal_cliId').value,
                    devolution_date: document.getElementById('rent_modal_cliDevolutionDate').value,
                    total_daily: document.getElementById('rent_modal_cliTotalDaily').value,
                    address: document.getElementById('rent_modal_cliAddress').value,
                    hour: document.getElementById('rent_modal_cliHour').value,
                    total_price: convert_float(document.getElementById('rent_modal_cliTotalPrice').value),
                    rent_date: document.getElementById('rent_modal_cliRentDate').value,
                    delivery_value: delivery_value,
                    previous_paid: previous_paid,
                    authorized_workers: document.getElementById('rent_modal_cliAuthorizedWorkers').value,
                    products: products,
                    desconto: desconto
                }
                $.post('/rent', array, function(array){
                    rent = array.rent;
                    document.getElementById('snackbar6').innerHTML = array.msg;
                    show_snackbar('snackbar6');
                    if(array.msg == 'Aluguel cadastrado com sucesso'){
                        fetch_data_product('rent_prod_div_autocomplete', 'rent_prod_input_pagination', 'rent_prod_input_search', 'rent_prod_select_search', 'section_rent_products_table', 1);
                        fetch_data_rent();
                        open_rent_modal(array.rent, array.rent_client, array.rent_details);
                        return_rent_id(rent.id);
                    }
                });
            }
        }
    }
}

function update_rent(){
    if(rent_form_isValid()){
        if(compare_dates()){
            delivery_value = document.getElementById('rent_modal_rentDeliveryValue').value;
            desconto = document.getElementById('desconto').value;
            if(desconto == ''){
                desconto = 0;
            }else{
                desconto = convert_float(desconto);
            }
            if(delivery_value == ''){
                delivery_value = 0;
            }else{
                delivery_value = convert_float(delivery_value);
            }
            paid = document.getElementById('rent_modal_cliPaidValue').value;
            if(paid == ''){
                paid = 0;
            }else{
                paid = convert_float(paid);
            }
            previous_paid = document.getElementById('rent_modal_cliPreviousPaid').value;
            if(previous_paid == ''){
                previous_paid = 0;
            }else{
                previous_paid = convert_float(previous_paid);
            }
            previous_paid = previous_paid + paid;
            total_price = convert_float(document.getElementById('rent_modal_cliTotalPrice').value) + delivery_value - desconto;
            if(previous_paid > total_price){
                document.getElementById('snackbar6').innerHTML = 'ERROS:<br><br>Não é possível realizar um pedido, em que <br>o valor já acertado é maior que o valor total.';
                show_snackbar('snackbar6');
            }else{
                var table = $('#rent_table>tbody>tr');
                var products = [];
                i = 0;
                while (i < table.length) {
                    if(table[i].cells[8].textContent == "Produto para venda"){
                        return_qty = "Produto para venda";
                        input_table = "Produto para venda";
                    }else{
                        return_qty = parseInt(table[i].cells[7].textContent) + parseInt(table[i].cells[8].firstChild.value);
                        input_table = table[i].cells[8].firstChild.value;
                    }
                    var obj = {
                        product_id : table[i].cells[0].textContent,
                        total_return_qty: return_qty,
                        return_qty: input_table,
                        qty: table[i].cells[6].textContent,
                    }
                    products.push(obj);
                    i++;
                }
                array = {
                    id: document.getElementById('rent_modal_rentId').value,
                    id_client: document.getElementById('rent_modal_cliId').value,
                    devolution_date: document.getElementById('rent_modal_cliDevolutionDate').value,
                    total_daily: document.getElementById('rent_modal_cliTotalDaily').value,
                    address: document.getElementById('rent_modal_cliAddress').value,
                    hour: document.getElementById('rent_modal_cliHour').value,
                    total_price: convert_float(document.getElementById('rent_modal_cliTotalPrice').value),
                    rent_date: document.getElementById('rent_modal_cliRentDate').value,
                    delivery_value: delivery_value,
                    previous_paid: previous_paid,
                    paid: paid,
                    authorized_workers: document.getElementById('rent_modal_cliAuthorizedWorkers').value,
                    products: products,
                    desconto: desconto
                }
                $.ajax({
                    type: 'PUT',
                    url: '/rent/'+document.getElementById('rent_modal_rentId').value,
                    data: array,
                    beforeSend: function(){
                        document.getElementById('snackbar6').className = "beforeSend";
                        document.getElementById('snackbar6').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
                    },
                    success: function(array){
                        document.getElementById('snackbar6').className.replace("beforeSend", "");
                        document.getElementById('snackbar6').innerHTML = array.msg;
                        show_snackbar('snackbar6');
                        if(array.msg == 'Aluguel atualizado com sucesso'){
                            fetch_data_client('rent_cli_div_autocomplete', 'rent_cli_input_pagination', 'rent_cli_input_search', 'rent_cli_select_search', 'section_rent_clients_table', 1);
                            fetch_data_product('rent_prod_div_autocomplete', 'rent_prod_input_pagination', 'rent_prod_input_search', 'rent_prod_select_search', 'section_rent_products_table', 1);
                            fetch_data_rent();
                            open_rent_modal(array.rent, array.rent_client, array.rent_details);
                        }
                    },
                    error: function(error){
                        console.log(error);
                    }
                });
            }
        }
    }
}

function action_modal_rent(){
    button = document.getElementById('rent_modal_button').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-white"></i> Cadastrar'){
        register_rent();
    }else{
        update_rent();
    }
}

function fetch_data_rent(){
    $('#rent_reports_div_autocomplete').fadeOut();
    page = document.getElementById('rent_reports_pagination').value;
    input = document.getElementById('rent_reports_input_search').value;
    select = document.getElementById('rent_reports_select_search').value;
    init_date = document.getElementById('rent_reports_init_date').value;
    ending_date = document.getElementById('rent_reports_ending_date').value;
    report_type = document.getElementById('rent_select_rentType').value;
    $.ajax({
        url:"/rent/fetch_data?page="+page+"&input="+input+"&select="+select+"&init_date="+init_date+"&ending_date="+ending_date+"&report_type="+report_type,
        beforeSend: function(){
            document.getElementById('section_table_rent_reports').innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById('section_table_rent_reports').innerHTML = data;
        }
    });
}

$('#rent_reports_input_search').keyup(function(){
    input = $(this).val();
    select = document.getElementById('rent_reports_select_search').value;
    if(input != ''){
        search = {
            input: input,
            select: select
        }
        $.post('rent/autocomplete', search, function(rent){
            document.getElementById('rent_reports_div_autocomplete').innerHTML ='';
            console.log(rent);
            if(rent.length > 0){
                var data = $('<ul class="list-group">');
                $('#rent_reports_div_autocomplete').append(data);
                rent.forEach(function(rent){
                    cpf_cnpj = '';
                    if(rent.cpf != null){
					if(rent.cpf.length > 0){
						cpf_cnpj += '<strong>CPF: </strong>'+rent.cpf+' ';
					}}
					if(rent.cnpj != null){
					if(rent.cnpj.length > 0){
						cpf_cnpj += '<strong>CNPJ: </strong>'+rent.cnpj;
					}}
                    if(rent.tipo_cadastro == 'Pessoa Física' || rent.tipo_cadastro == 'Cadastro Simplificado'){
						data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\"><strong>Nome:</strong> "
								+rent.nome+" <strong>Endereço: </strong>"
								+rent.endereco+' '+cpf_cnpj+"</button></li>");
					}else{
						data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\"><strong>Nome:</strong> "
								+rent.nome_empresa+" <strong>Endereço: </strong>"
								+rent.endereco_empresa+' '+cpf_cnpj+"</button></li>");
					}
                    data.find('button').click(function() {
                        fill_cli_searchInput(rent, 'rent_reports_input_search', 'rent_reports_select_search', 'rent_reports_div_autocomplete');
                    });
                    $('#rent_reports_div_autocomplete').append(data);
                });
                data = $('</ul>');
                $('#rent_reports_div_autocomplete').append(data);
                $('#rent_reports_div_autocomplete').fadeIn();
            }
        });
    }
});

/* GERAR PDF */
function return_rent_id(id){
    var form = document.createElement("form");
    document.body.appendChild(form);
    form.method = "POST";
    form.action = "/api/rent/pdf_rent";
    form.target = '_blank';
    $('<input />').attr('type', 'hidden')
        .attr('name', 'id')
        .attr('value', id)
        .appendTo(form);
    form.submit();
}

function gera_recibo(id){
    var form = document.createElement("form");
    document.body.appendChild(form);
    form.method = "POST";
    form.action = "/api/rent/recibo";
    form.target = '_blank';
    $('<input />').attr('type', 'hidden')
        .attr('name', 'id')
        .attr('value', id)
        .appendTo(form);
    form.submit();
}

function open_modal_delete_rent(obj, cli_name){
    previous_paid = obj.previous_paid;
    if(previous_paid > 0){
        document.getElementById('snackbar5').innerHTML = 'Não é possível deletar um pedido que<br> possui algum valor que já foi pago';
        show_snackbar('snackbar5');
    }else{
        document.getElementById('delete_rentModal_msg').innerHTML = 'Você deseja deletar o pedido de Nº '+obj.id+' associado ao cliente '+cli_name.client_name+'?';
        document.getElementById('delete_rentModal_id').value = obj.id;
        document.getElementById('button_delete_rentModal').innerHTML = '<i class="fas fa-trash"></i> Excluir';
        $('#rent_delete_modal').modal();
    }
}

function delete_rent(){
    id_rent = document.getElementById('delete_rentModal_id').value;
    $.ajax({
        type: 'DELETE',
        url: 'rent/'+id_rent,
        beforeSend: function(){
            document.getElementById('snackbar8').className = "beforeSend";
            document.getElementById('snackbar8').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar8').className.replace("beforeSend", "");
            document.getElementById('snackbar8').innerHTML = msg;
            show_snackbar('snackbar8');
            if(msg == 'Pedido deletado com sucesso'){
                fetch_data_client('rent_cli_div_autocomplete', 'rent_cli_input_pagination', 'rent_cli_input_search', 'rent_cli_select_search', 'section_rent_clients_table', 1);
                fetch_data_product('rent_prod_div_autocomplete', 'rent_prod_input_pagination', 'rent_prod_input_search', 'rent_prod_select_search', 'section_rent_products_table', 1);
                fetch_data_rent();
            }
        }
    });
}

function rent_discharge(){
    current_status = document.getElementById('rent_modal_rentStatus').value;
    document.getElementById('rent_modal_button').innerHTML = '<i class="fas fa-sync text-white"></i> Atualizar';
    document.getElementById('form_group_rent_paid_value').classList.remove('d-none');
    document.getElementById('form_group_rent_total_price').classList.remove('col-lg-6');
    document.getElementById('form_group_rent_total_price').classList.add('col-lg-4');
    document.getElementById('form_group_rent_previous_paid').classList.remove('col-lg-6');
    document.getElementById('form_group_rent_previous_paid').classList.add('col-lg-4');
    document.getElementById('rent_modal_cliHour').readOnly = true;
    document.getElementById('rent_modal_cliRentDate').readOnly = true;
    if(current_status == 'Baixa'){
        document.getElementById('rent_modal_cliId').readOnly = true;
        document.getElementById('rent_modal_cliName').readOnly = true;
        document.getElementById('rent_modal_cliAddress').readOnly = true;
        document.getElementById('rent_modal_cliDevolutionDate').readOnly = true;
        document.getElementById('rent_modal_cliTotalPrice').readOnly = true;
        document.getElementById('rent_modal_cliPreviousPaid').readOnly = true;
        document.getElementById('rent_modal_cliAuthorizedWorkers').readOnly = true;
        document.getElementById('rent_modal_rentDeliveryValue').readOnly = true;
        document.getElementById('desconto').readOnly = true;
        document.getElementById('rent_modal_title').innerHTML = 'Visualizar o Pedido';
        document.getElementById('form_group_rent_total_price').readOnly = true;
        document.getElementById('form_group_rent_previous_paid').readOnly = true;
        document.getElementById('rent_modal_button').classList.add('d-none');
    }else{
        document.getElementById('rent_modal_cliId').readOnly = false;
        document.getElementById('rent_modal_cliName').readOnly = false;
        document.getElementById('rent_modal_cliAddress').readOnly = false;
        document.getElementById('rent_modal_cliDevolutionDate').readOnly = false;
        document.getElementById('rent_modal_cliTotalPrice').readOnly = false;
        document.getElementById('rent_modal_cliPreviousPaid').readOnly = false;
        document.getElementById('rent_modal_cliAuthorizedWorkers').readOnly = false;
        document.getElementById('rent_modal_rentDeliveryValue').readOnly = false;
        document.getElementById('desconto').readOnly = false;
        document.getElementById('rent_modal_title').innerHTML = 'Editar o Pedido';
        document.getElementById('form_group_rent_total_price').readOnly = false;
        document.getElementById('form_group_rent_previous_paid').readOnly = false;
        document.getElementById('rent_modal_button').classList.remove('d-none');
    }
}

function open_modal_pay_all(obj, cli_name){
    document.getElementById('delete_rentModal_msg').innerHTML = 'Você deseja pagar o restante do valor do pedido de Nº '+obj.id+' associado ao cliente '+cli_name.client_name+'?';
    document.getElementById('delete_rentModal_id').value = obj.id;
    document.getElementById('button_delete_rentModal').innerHTML = '<i class="fas fa-dollar-sign"></i> Pagar Tudo';
    $('#rent_delete_modal').modal();
}

function action_button_delete_rent(){
    button = document.getElementById('button_delete_rentModal').innerHTML;
    if(button == '<i class="fas fa-trash"></i> Excluir'){
        delete_rent();
    }else{
        pay_all();
    }
}

function pay_all(){
    id_rent = document.getElementById('delete_rentModal_id').value;
    $.ajax({
        type: 'GET',
        url: 'rent/payAll/'+id_rent,
        success: function(msg){
            document.getElementById('snackbar8').innerHTML = msg;
            show_snackbar('snackbar8');
            if(msg == 'Pedido pago com sucesso'){
                fetch_data_client('rent_cli_div_autocomplete', 'rent_cli_input_pagination', 'rent_cli_input_search', 'rent_cli_select_search', 'section_rent_clients_table', 1);
                fetch_data_rent();
            }
        }
    });
}

$('#popover').on('click', function(){
    if($('.popover').hasClass('in')){
        $(this).popover('hide');
    }
    else{
        delivery_value = document.getElementById('rent_modal_rentDeliveryValue').value;
        desconto = document.getElementById('desconto').value;
        total_price = document.getElementById('rent_modal_cliTotalPrice').value;
        if(desconto == ''){
            desconto = 0;
        }else{
            desconto = convert_float(desconto);
        }
        if(delivery_value == ''){
            delivery_value = 0;
        }else{
            delivery_value = convert_float(delivery_value);
        }
        if(total_price == ''){
            total_price = 0;
        }else{
            total_price = convert_float(total_price);
        }
        msg = '';
        button = document.getElementById('rent_modal_button').innerHTML;
        if(button == '<i class="fas fa-plus-circle text-white"></i> Cadastrar'){
            total_price = total_price + delivery_value - desconto;
            msg = total_price;
        }else{
            paid = document.getElementById('rent_modal_cliPaidValue').value;
            if(paid == ''){
                paid = 0;
            }else{
                paid = convert_float(paid);
            }
            total_price = total_price + delivery_value - desconto - paid;
            msg = total_price;
        }
        $(this).attr('data-content','Falta R$: '+number_format(msg, 2, ',', '.')+' para o cliente acertar o pedido.');
        $(this).popover('show');
    }
});

function calc_preco_total(){
    if(compare_dates()){
        data_devolucao = document.getElementById('rent_modal_cliDevolutionDate').value;
        data_pedido = document.getElementById('rent_modal_cliRentDate').value;
        data_devolucao = new Date(data_devolucao);
        data_pedido = new Date(data_pedido);
        timeDiff = Math.abs(data_devolucao.getTime() - data_pedido.getTime());
        diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        diffDays = parseInt(diffDays.toString()) + 1;
        var table = $('#rent_table>tbody>tr');
        var products = [];
        i = 0;
        if(table.length == 0){
            document.getElementById('snackbar6').innerHTML = 'ERRO:<br><br>Não existe produto associado a esse pedido.';
            show_snackbar('snackbar6');
        }else{
            while (i < table.length) {
                var obj = {
                    product_id : table[i].cells[0].textContent,
                    qty : table[i].cells[6].textContent,
                    daily: table[i].cells[5].textContent,
                    type: table[i].cells[3].textContent,
                }
                products.push(obj);
                i++;
            }
            i = 0;
            preco_total = 0;
            delivery_value = document.getElementById('rent_modal_rentDeliveryValue').value;
            desconto = document.getElementById('desconto').value;
            if(desconto == ''){
                desconto = 0;
            }else{
                desconto = convert_float(desconto);
            }
            if(delivery_value == ''){
                delivery_value = 0;
            }else{
                delivery_value = convert_float(delivery_value);
            }
            while (i < products.length){
                var qty = products[i].qty;
                var daily = products[i].daily;
                var type = products[i].type;
                if(type == 'Aluguel'){
                    var preco = diffDays * qty * convert_float(daily);
                }else{
                    var preco = qty * convert_float(daily);
                }
                preco_total = parseFloat(preco_total) + parseFloat(preco);
                i++;
            }
            document.getElementById('snackbar6').innerHTML = 'Quantidade de diárias: '+diffDays+' dias.';
            show_snackbar('snackbar6');
            document.getElementById('rent_modal_cliTotalPrice').value = number_format(preco_total, 2, ',', '.');
        }
    }
}

function compare_dates(){
    msg = '';
    data_devolucao = document.getElementById('rent_modal_cliDevolutionDate').value;
    data_pedido = document.getElementById('rent_modal_cliRentDate').value;
    if(data_devolucao == ''){
        msg += 'Data de devolução vazia!<br>';
    }
    if(data_pedido == ''){
        msg += 'Data do pedido vazia!<br>';
    }
    if(data_pedido > data_devolucao && data_devolucao.length > 0){
        msg += 'Data do pedido não pode ser maior que a data de devolução.<br>';
    }
    if(msg.length > 0){
        document.getElementById('snackbar6').innerHTML = 'ERROS<br><br>'+msg;
        show_snackbar('snackbar6');
        return false;
    }
    return true;
}
