function abre_modal_pre_cadastro(obj){
    document.getElementById('pre_cad_file_input').value = '';
    document.getElementById('pre_cad_img_extension').value = '';
    document.getElementById('pre_cad_img_thumb').src = 'assets/sem_img.png';
    document.getElementById('pre_cad_img_thumb_termo').src = 'assets/sem_img.png';
    document.getElementById('pre_cad_img_thumb_junta_comercial').src = 'assets/arquivo.png';
    document.getElementById('pre_cad_img_thumb_contrato_social').src = 'assets/arquivo.png';
	document.getElementById('pre_cad_file_input_contrato_social').value = '';
    document.getElementById('pre_cad_img_extension_contrato_social').value = '';
	document.getElementById('pre_cad_file_input_junta_comercial').value = '';
    document.getElementById('pre_cad_img_extension_junta_comercial').value = '';
	document.getElementById('pre_cad_file_input_termo').value = '';
    document.getElementById('pre_cad_img_extension_termo').value = '';
    if(obj == 0){
        document.getElementById('tipo_cadastro').value = 'Pessoa Física';
        pre_cadastro_select();
        document.getElementById('div_pre_cad_tipo_cadastro').classList.remove('d-none');
        document.getElementById('pre_cad_title').innerHTML = 'Pré-cadastro';
        document.getElementById('pre_cad_action_button').innerHTML = '<i class="fas fa-plus-circle text-white"></i> Cadastrar';
    }else{
        document.getElementById('tipo_cadastro').value = obj.tipo_cadastro;
        pre_cadastro_select();
        document.getElementById('div_pre_cad_tipo_cadastro').classList.add('d-none');
        document.getElementById('pre_cad_title').innerHTML = 'Editar Cliente';
        document.getElementById('pre_cad_action_button').innerHTML = '<i class="fas fa-sync"></i> Atualizar';
        document.getElementById('tipo_cadastro').value = obj.tipo_cadastro;
        preenche_modal_pre_cadastro(obj);
        validacao_pre_cadastro();

    }
    $('#modal_pre_cadastro').modal({backdrop: 'static', keyboard: false});
}

function pre_cad_ver_mais(){
    document.getElementById("div_termo_autorizacao").classList.remove('d-none');
    document.getElementById("pre_cad_ver_menos").classList.remove("d-none");
    document.getElementById("pre_cad_ver_mais").classList.add("d-none");
}

function pre_cad_ver_menos(){
    document.getElementById("div_termo_autorizacao").classList.add('d-none');
    document.getElementById("pre_cad_ver_mais").classList.remove("d-none");
    document.getElementById("pre_cad_ver_menos").classList.add("d-none");
}

function phones_validation(){
    var empty = 0;
    var error = 0;
    var tel_fixo = document.getElementById('pre_cad_tel_fixo').value;
    var tel_comercio = document.getElementById('pre_cad_tel_comercio').value;
    var tel_cel = document.getElementById('pre_cad_tel_cel').value;
    if(tel_fixo.length < 1){
        empty++;
    }else{
        field_validation('pre_cad_tel_fixo', 'pre_cad_small_tel_fixo', 'Campo Validado', 'Campo telefone fixo inválido', 14);
        tel_fixo = document.getElementById('pre_cad_small_tel_fixo').innerHTML;
        if(tel_fixo != 'Campo Validado'){
            error++;
        }
    }
    if(tel_comercio.length < 1){
        empty++;
    }else{
        field_validation('pre_cad_tel_comercio', 'pre_cad_small_tel_comercio', 'Campo Validado', 'Campo telefone comercial inválido', 14);
        tel_comercio = document.getElementById('pre_cad_small_tel_comercio').innerHTML;
        if(tel_comercio != 'Campo Validado'){
            error++;
        }
    }
    if(tel_cel.length < 1){
        empty++;
    }else{
        field_validation('pre_cad_tel_cel', 'pre_cad_small_tel_cel', 'Campo Validado', 'Campo celular inválido', 14);
        tel_cel = document.getElementById('pre_cad_small_tel_cel').innerHTML;
        if(tel_cel != 'Campo Validado'){
            error++;
        }
    }
    if(empty == 3){
        field_validation('pre_cad_tel_fixo', 'pre_cad_small_tel_fixo', 'Campo Validado', 'Campo telefone fixo vazio', 14);
        field_validation('pre_cad_tel_comercio', 'pre_cad_small_tel_comercio', 'Campo Validado', 'Campo telefone comercial vazio', 14);
        field_validation('pre_cad_tel_cel', 'pre_cad_small_tel_cel', 'Campo Validado', 'Campo celular vazio', 14);
    }else{
        if(error > 0){
            field_validation('pre_cad_tel_fixo', 'pre_cad_small_tel_fixo', 'Campo Validado', 'Campo telefone fixo inválido', 14);
            field_validation('pre_cad_tel_comercio', 'pre_cad_small_tel_comercio', 'Campo Validado', 'Campo telefone comercial inválido', 14);
            field_validation('pre_cad_tel_cel', 'pre_cad_small_tel_cel', 'Campo Validado', 'Campo celular inválido', 14);
        }else{
            var tel_fixo = document.getElementById('pre_cad_tel_fixo').value;
            var tel_comercio = document.getElementById('pre_cad_tel_comercio').value;
            var tel_cel = document.getElementById('pre_cad_tel_cel').value;
            if(tel_fixo.length == 0){
                input_valid('pre_cad_tel_fixo');
                small_success('pre_cad_small_tel_fixo', 'Esse campo pode ficar vazio');
            }
            if(tel_comercio.length == 0){
                input_valid('pre_cad_tel_comercio');
                small_success('pre_cad_small_tel_comercio', 'Esse campo pode ficar vazio');
            }
            if(tel_cel.length == 0){
                input_valid('pre_cad_tel_cel');
                small_success('pre_cad_small_tel_cel', 'Esse campo pode ficar vazio');
            }
        }
    }
}

function limpa_campos_pre_cadastro(){
    var array_input = [
            'pre_cad_nome',
            'pre_cad_endereco',
            'pre_cad_bairro',
            'pre_cad_cidade',
            'pre_cad_email',
            'pre_cad_tel_fixo',
            'pre_cad_tel_comercio',
            'pre_cad_tel_cel',
            'pre_cad_cpf',
            'pre_cad_nome_empresa',
            'pre_cad_end_empresa',
            'pre_cad_bairro_empresa',
            'pre_cad_cidade_empresa',
            'pre_cad_cnpj',
            'pre_cad_email_empresa'
        ];
    var array_small = [{
        small: 'pre_cad_small_nome',
        msg: 'Digite o nome'
    }, {
        small: 'pre_cad_small_endereco',
        msg: 'Digite o endereço'
    },{
        small: 'pre_cad_small_bairro',
        msg: 'Digite o bairro'
    },{
        small: 'pre_cad_small_cidade',
        msg: 'Digite a cidade'
    },{
        small: 'pre_cad_small_email',
        msg: 'Digite o e-mail'
    },{
        small: 'pre_cad_small_tel_fixo',
        msg: 'Digite o telefone fixo'
    },{
        small: 'pre_cad_small_tel_comercio',
        msg: 'Digite o telefone comercial'
    },{
        small: 'pre_cad_small_tel_cel',
        msg: 'Digite o celular'
    },{
        small: 'pre_cad_small_cpf',
        msg: 'Digite o CPF'
    },{
        small: 'pre_cad_small_nome_empresa',
        msg: 'Digite o nome da empresa'
    },{
        small: 'pre_cad_small_end_empresa',
        msg: 'Digite o endereço da empresa'
    },{
        small: 'pre_cad_small_bairro_empresa',
        msg: 'Digite o bairro da empresa'
    },{
        small: 'pre_cad_small_cidade_empresa',
        msg: 'Digite a cidade da empresa'
    },{
        small: 'pre_cad_small_cnpj',
        msg: 'Digite o CNPJ da empresa'
    },{
        small: 'pre_cad_small_email_empresa',
        msg: 'Digite o e-mail da empresa'
    }];
    clear_modal_fields(array_input, array_small);
    document.getElementById('pre_cad_estado').value = 'MG';
    document.getElementById('pre_cad_estado_empresa').value = 'MG';
    document.getElementById('pre_cad_formas_pagamento').value = 'Emissão de Boleto Bancário';
    /* $('#pre_cad_observacoes').jqteVal(""); */
    document.getElementById('pre_cad_observacoes').value = '';
}

function pre_cadastro_select(){
    limpa_campos_pre_cadastro();
    var select = document.getElementById('tipo_cadastro').value;
    $("#pre_cad_checkbox_confirmacao").prop( "checked", false );
    document.getElementById('div_pre_cad_cidade').classList.remove('d-none');
    document.getElementById('div_pre_cad_estado').classList.remove('d-none');
    document.getElementById('div_pre_cad_cpf').classList.remove('d-none');
    document.getElementById('div_pre_cad_forma_pagamento').classList.remove('d-none');
    document.getElementById('div_pre_cad_upload').classList.remove('d-none');
    document.getElementById('pre_cad_ver_mais').classList.remove('d-none');
    document.getElementById('div_termo_autorizacao').classList.add('d-none');
    document.getElementById('pre_cad_ver_menos').classList.add('d-none');
    if(select == 'Pessoa Física'){
        document.getElementById('div_imagem_contrato').classList.remove('d-none');
        document.getElementById('div_contrato_social').classList.add('d-none');
        document.getElementById('div_junta_comercial').classList.add('d-none');
        document.getElementById('div_termo').classList.add('d-none');
        document.getElementById('div_dados_empresa').classList.add('d-none');
        document.getElementById('div_email_empresa').classList.add('d-none');
        document.getElementById('div_nome_empresa').classList.add('d-none');
        document.getElementById('div_endereco_empresa').classList.add('d-none');
        document.getElementById('div_bairro_empresa').classList.add('d-none');
        document.getElementById('div_cidade_empresa').classList.add('d-none');
        document.getElementById('div_estado_empresa').classList.add('d-none');
        document.getElementById('div_cnpj_empresa').classList.add('d-none');
        document.getElementById('div_dados_representante').classList.add('d-none');
        document.getElementById('div_dados_cliente').classList.remove('d-none');
    }else{
        if(select == 'Pessoa Jurídica'){
            document.getElementById('div_imagem_contrato').classList.add('d-none');
            document.getElementById('div_contrato_social').classList.remove('d-none');
            document.getElementById('div_junta_comercial').classList.remove('d-none');
            document.getElementById('div_termo').classList.remove('d-none');
            document.getElementById('div_dados_empresa').classList.remove('d-none');
            document.getElementById('div_email_empresa').classList.remove('d-none');
            document.getElementById('div_nome_empresa').classList.remove('d-none');
            document.getElementById('div_endereco_empresa').classList.remove('d-none');
            document.getElementById('div_bairro_empresa').classList.remove('d-none');
            document.getElementById('div_cidade_empresa').classList.remove('d-none');
            document.getElementById('div_estado_empresa').classList.remove('d-none');
            document.getElementById('div_cnpj_empresa').classList.remove('d-none');
            document.getElementById('div_dados_representante').classList.remove('d-none');
            document.getElementById('div_dados_cliente').classList.add('d-none');
        }else{
            document.getElementById('div_pre_cad_cidade').classList.add('d-none');
            document.getElementById('div_pre_cad_estado').classList.add('d-none');
            document.getElementById('div_pre_cad_cpf').classList.add('d-none');
            document.getElementById('div_pre_cad_forma_pagamento').classList.add('d-none');
            document.getElementById('div_pre_cad_upload').classList.add('d-none');
            document.getElementById('div_imagem_contrato').classList.add('d-none');
            document.getElementById('div_contrato_social').classList.add('d-none');
            document.getElementById('div_junta_comercial').classList.add('d-none');
            document.getElementById('div_termo').classList.add('d-none');
            document.getElementById('div_dados_empresa').classList.add('d-none');
            document.getElementById('div_email_empresa').classList.add('d-none');
            document.getElementById('div_nome_empresa').classList.add('d-none');
            document.getElementById('div_endereco_empresa').classList.add('d-none');
            document.getElementById('div_bairro_empresa').classList.add('d-none');
            document.getElementById('div_cidade_empresa').classList.add('d-none');
            document.getElementById('div_estado_empresa').classList.add('d-none');
            document.getElementById('div_cnpj_empresa').classList.add('d-none');
            document.getElementById('div_dados_representante').classList.add('d-none');
            document.getElementById('div_dados_cliente').classList.add('d-none');
            document.getElementById('div_imagem_contrato').classList.add('d-none');
            document.getElementById('div_contrato_social').classList.add('d-none');
            document.getElementById('div_junta_comercial').classList.add('d-none');
			document.getElementById('div_termo').classList.add('d-none');
        }
    }
}

$('#pre_cad_img_button').click(function(){
    $("#pre_cad_file_input").click();
});

$('#pre_cad_img_button_contrato_social').click(function(){
    $("#pre_cad_file_input_contrato_social").click();
});

$('#pre_cad_img_button_junta_comercial').click(function(){
    $("#pre_cad_file_input_junta_comercial").click();
});

$('#pre_cad_img_button_termo').click(function(){
    $("#pre_cad_file_input_termo").click();
});

function validacao_pre_cadastro(){
    emailValidation('pre_cad_email_empresa', 'pre_cad_small_email_empresa', 'Campo Validado', 'Campo e-mail empresa inválido');
    emailValidation('pre_cad_email', 'pre_cad_small_email', 'Campo Validado', 'Campo e-mail inválido');
    field_validation('pre_cad_nome_empresa', 'pre_cad_small_nome_empresa', 'Campo Validado', 'Campo nome da empresa inválido', 1);
    field_validation('pre_cad_end_empresa', 'pre_cad_small_end_empresa', 'Campo Validado', 'Campo endereço da empresa inválido', 1);
    field_validation('pre_cad_bairro_empresa', 'pre_cad_small_bairro_empresa', 'Campo Validado', 'Campo bairro da empresa inválido', 1);
    field_validation('pre_cad_cidade_empresa', 'pre_cad_small_cidade_empresa', 'Campo Validado', 'Campo cidade inválido', 1);
    cnpj_validation('pre_cad_cnpj', 'pre_cad_small_cnpj', 'Campo Validado', 'Campo CNPJ inválido');
    field_validation('pre_cad_nome', 'pre_cad_small_nome', 'Campo Validado', 'Campo nome inválido', 1);
    field_validation('pre_cad_endereco', 'pre_cad_small_endereco', 'Campo Validado', 'Campo endereço inválido', 1);
    field_validation('pre_cad_bairro', 'pre_cad_small_bairro', 'Campo Validado', 'Campo bairro inválido', 1);
    field_validation('pre_cad_cidade', 'pre_cad_small_cidade', 'Campo Validado', 'Campo cidade inválido', 1);
    phones_validation();
    cpf_validation('pre_cad_cpf', 'pre_cad_small_cpf', 'Campo Validado', 'Campo CPF inválido');
}

function action_pre_cadastro(){
    var button = document.getElementById('pre_cad_action_button').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-white"></i> Cadastrar'){
        form_register_client();
    }else{
        update_client();
    }
}

function update_client(){
    if(pre_cad_valid()){
        var aux;
        var tipo_cadastro = document.getElementById('tipo_cadastro').value;
        cliente = new FormData();
        cliente.append('id', document.getElementById('pre_cad_id').value);
        cliente.append('nome', document.getElementById('pre_cad_nome').value);
        cliente.append('email', document.getElementById('pre_cad_email').value);
        cliente.append('endereco', document.getElementById('pre_cad_endereco').value);
        cliente.append('bairro', document.getElementById('pre_cad_bairro').value);
        cliente.append('tel_fixo', document.getElementById('pre_cad_tel_fixo').value);
        cliente.append('tel_comercio', document.getElementById('pre_cad_tel_comercio').value);
        cliente.append('tel_cel', document.getElementById('pre_cad_tel_cel').value);
        cliente.append('observacoes', document.getElementById('pre_cad_observacoes').value);
        cliente.append('tipo_cadastro', tipo_cadastro);
        if(tipo_cadastro != 'Cadastro Simplificado'){
            cliente.append('cidade', document.getElementById('pre_cad_cidade').value);
            cliente.append('estado', document.getElementById('pre_cad_estado').value);
            cliente.append('cpf', document.getElementById('pre_cad_cpf').value);
            cliente.append('info_comercial_obs', document.getElementById('pre_cad_formas_pagamento').value);
            if(tipo_cadastro == 'Pessoa Física'){
                aux = document.getElementById('pre_cad_file_input').files;
                if(aux.length != 0){
                    cliente.append('thumb', document.getElementById('pre_cad_file_input').files[0]);
                    cliente.append('extensao', document.getElementById('pre_cad_file_input').files[0].type.replace('image/', ''));
                }else{
                    cliente.append('thumb', '');
                    cliente.append('extensao', '');
                }
            }else{
                cliente.append('nome_empresa', document.getElementById('pre_cad_nome_empresa').value);
                cliente.append('email_empresa', document.getElementById('pre_cad_email_empresa').value);
                cliente.append('endereco_empresa', document.getElementById('pre_cad_end_empresa').value);
                cliente.append('bairro_empresa', document.getElementById('pre_cad_bairro_empresa').value);
                cliente.append('cidade_empresa', document.getElementById('pre_cad_cidade_empresa').value);
                cliente.append('estado_empresa', document.getElementById('pre_cad_estado_empresa').value);
                cliente.append('cnpj', document.getElementById('pre_cad_cnpj').value);
                /* UPLOAD DE ARQUIVOS */
                aux = document.getElementById('pre_cad_file_input_junta_comercial').files;
                if(aux.length != 0){
                    cliente.append('thumb_contrato_social', document.getElementById('pre_cad_file_input_contrato_social').files[0]);
                    cliente.append('extensao_contrato_social', document.getElementById('pre_cad_img_extension_contrato_social').value);
                }else{
                    cliente.append('thumb_contrato_social', '');
                    cliente.append('extensao_contrato_social', '');
                }
                aux = document.getElementById('pre_cad_file_input_contrato_social').files;
                if(aux.length != 0){
                    cliente.append('thumb_contrato_social', document.getElementById('pre_cad_file_input_contrato_social').files[0]);
                    cliente.append('extensao_contrato_social', document.getElementById('pre_cad_img_extension_contrato_social').value);
                }else{
                    cliente.append('thumb_contrato_social', '');
                    cliente.append('extensao_contrato_social', '');
                }
                aux = document.getElementById('pre_cad_file_input_junta_comercial').files;
                if(aux.length != 0){
                    cliente.append('thumb_junta_comercial', document.getElementById('pre_cad_file_input_junta_comercial').files[0]);
                    cliente.append('extensao_junta_comercial', document.getElementById('pre_cad_img_extension_junta_comercial').value);
                }else{
                    cliente.append('thumb_junta_comercial', '');
                    cliente.append('extensao_junta_comercial', '');
                }
                aux = document.getElementById('pre_cad_file_input_termo').files;
                if(aux.length != 0){
                    cliente.append('thumb_termo', document.getElementById('pre_cad_file_input_termo').files[0]);
                    cliente.append('extensao_termo', document.getElementById('pre_cad_img_extension_termo').value);
                }else{
                    cliente.append('thumb_termo', '');
                    cliente.append('extensao_termo', '');
                }
            }
        }
        $.ajax({
            type:"POST",
            url:'clients/update',
            data:cliente,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
            },
            success:function(msg){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = msg;
                show_snackbar('snackbar2');
                if(control){
                    if(msg.includes('Cliente atualizado com sucesso')){
                        fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0);
                    }
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function form_register_client(){
    if(document.getElementById("pre_cad_checkbox_confirmacao").checked){
    if(pre_cad_valid()){
        var tipo_cadastro = document.getElementById('tipo_cadastro').value;
        cliente = new FormData();
        cliente.append('nome', document.getElementById('pre_cad_nome').value);
        cliente.append('email', document.getElementById('pre_cad_email').value);
        cliente.append('endereco', document.getElementById('pre_cad_endereco').value);
        cliente.append('bairro', document.getElementById('pre_cad_bairro').value);
        cliente.append('tel_fixo', document.getElementById('pre_cad_tel_fixo').value);
        cliente.append('tel_comercio', document.getElementById('pre_cad_tel_comercio').value);
        cliente.append('tel_cel', document.getElementById('pre_cad_tel_cel').value);
        cliente.append('observacoes', document.getElementById('pre_cad_observacoes').value);
        cliente.append('tipo_cadastro', tipo_cadastro);
        cliente.append('status', 'Aprovado');
        cliente.append('aprovado', 0);
        if(tipo_cadastro != 'Cadastro Simplificado'){
            cliente.append('cidade', document.getElementById('pre_cad_cidade').value);
            cliente.append('estado', document.getElementById('pre_cad_estado').value);
            cliente.append('cpf', document.getElementById('pre_cad_cpf').value);
            cliente.append('info_comercial_obs', document.getElementById('pre_cad_formas_pagamento').value);
            if(tipo_cadastro == 'Pessoa Física'){
                cliente.append('thumb', document.getElementById('pre_cad_file_input').files[0]);
                cliente.append('extensao', document.getElementById('pre_cad_file_input').files[0].type.replace('image/', ''));
            }else{
                cliente.append('nome_empresa', document.getElementById('pre_cad_nome_empresa').value);
                cliente.append('email_empresa', document.getElementById('pre_cad_email_empresa').value);
                cliente.append('endereco_empresa', document.getElementById('pre_cad_end_empresa').value);
                cliente.append('bairro_empresa', document.getElementById('pre_cad_bairro_empresa').value);
                cliente.append('cidade_empresa', document.getElementById('pre_cad_cidade_empresa').value);
                cliente.append('estado_empresa', document.getElementById('pre_cad_estado_empresa').value);
                cliente.append('cnpj', document.getElementById('pre_cad_cnpj').value);
                /* UPLOAD DE ARQUIVOS */
                cliente.append('thumb_contrato_social', document.getElementById('pre_cad_file_input_contrato_social').files[0]);
                cliente.append('extensao_contrato_social', document.getElementById('pre_cad_img_extension_contrato_social').value);
                cliente.append('thumb_junta_comercial', document.getElementById('pre_cad_file_input_junta_comercial').files[0]);
                cliente.append('extensao_junta_comercial', document.getElementById('pre_cad_img_extension_junta_comercial').value);
                cliente.append('thumb_termo', document.getElementById('pre_cad_file_input_termo').files[0]);
                cliente.append('extensao_termo', document.getElementById('pre_cad_img_extension_termo').value);
            }
        }
        $.ajax({
            type:"POST",
            url:'clients/cadastro',
            data:cliente,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
            },
            success:function(msg){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = msg;
                show_snackbar('snackbar2');
                if(control){
                    if(msg == 'Pré-cadastro realizado com sucesso'){
                        fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0);
                    }
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
    }else{
        document.getElementById('snackbar2').className.replace("beforeSend", "");
        document.getElementById('snackbar2').innerHTML = 'Para realizar o cadastro o cliente deve concordar em ceder os dados';
        show_snackbar('snackbar2');
    }
}

function pre_cad_valid(){
    validacao_pre_cadastro();
    var tipo_cadastro = document.getElementById('tipo_cadastro').value;
    var control = true;
    var msg = '';
    var aux = document.getElementById('pre_cad_small_nome').innerHTML;
    if(aux != 'Campo Validado'){
        msg += aux+'<br>';
    }
    aux = document.getElementById('pre_cad_small_email').innerHTML;
    if(aux != 'Campo Validado'){
        msg += aux+'<br>';
    }
    aux = document.getElementById('pre_cad_small_endereco').innerHTML;
    if(aux != 'Campo Validado'){
        msg += aux+'<br>';
    }
    aux = document.getElementById('pre_cad_small_bairro').innerHTML;
    if(aux != 'Campo Validado'){
        msg += aux+'<br>';
    }
    aux = document.getElementById('pre_cad_small_tel_fixo').innerHTML;
    if(aux == 'Campo telefone fixo inválido'){
        msg += aux+'<br>';
    }
    aux = document.getElementById('pre_cad_small_tel_comercio').innerHTML;
    if(aux == 'Campo telefone comercial vazio'){
        msg += aux+'<br>';
    }
    aux = document.getElementById('pre_cad_small_tel_cel').innerHTML;
    if(aux == 'Campo celular inválido'){
        msg += aux+'<br>';
    }
    if(tipo_cadastro != 'Cadastro Simplificado'){
        aux = document.getElementById('pre_cad_small_cidade').innerHTML;
        if(aux != 'Campo Validado'){
            msg += aux+'<br>';
        }
        aux = document.getElementById('pre_cad_small_cpf').innerHTML;
        if(aux != 'Campo Validado'){
            msg += aux+'<br>';
        }
        if(tipo_cadastro == 'Pessoa Física'){
            aux = document.getElementById('pre_cad_file_input').files;
            if(aux.length != 0){
                if(!aux[0].type.includes('image')){
                    msg += 'Formato de imagem inválido para upload do documento de identificação<br>';
                }
            }else{
                aux = document.getElementById('pre_cad_action_button').innerHTML;
                if(aux != '<i class="fas fa-sync"></i> Atualizar'){
                    msg += 'Por favor coloque uma imagem do documento de identificação para realizar o cadastro<br>';
                }
            }
        }else{
            aux = document.getElementById('pre_cad_small_email_empresa').innerHTML;
            if(aux != 'Campo Validado'){
                msg += aux+'<br>';
            }
            aux = document.getElementById('pre_cad_small_nome_empresa').innerHTML;
            if(aux != 'Campo Validado'){
                msg += aux+'<br>';
            }
            aux = document.getElementById('pre_cad_small_end_empresa').innerHTML;
            if(aux != 'Campo Validado'){
                msg += aux+'<br>';
            }
            aux = document.getElementById('pre_cad_small_bairro_empresa').innerHTML;
            if(aux != 'Campo Validado'){
                msg += aux+'<br>';
            }
            aux = document.getElementById('pre_cad_small_cidade_empresa').innerHTML;
            if(aux != 'Campo Validado'){
                msg += aux+'<br>';
            }
            aux = document.getElementById('pre_cad_small_cnpj').innerHTML;
            if(aux != 'Campo Validado'){
                msg += aux+'<br>';
            }
            aux = document.getElementById('pre_cad_file_input_contrato_social').files;
            if(aux.length != 0){
                aux = document.getElementById('pre_cad_img_extension_contrato_social').value;
                if(!(aux == 'pdf' || aux == 'doc' || aux == 'docx')){
                    msg += 'Formato de documento inválido para upload de contrato social<br>';
                }
            }else{
                aux = document.getElementById('pre_cad_action_button').innerHTML;
                if(aux != '<i class="fas fa-sync"></i> Atualizar'){
                    msg += 'Coloque um documento de contrato social para realizar o cadastro<br>';
                }
            }
            aux = document.getElementById('pre_cad_file_input_junta_comercial').files;
            if(aux.length != 0){
                aux = document.getElementById('pre_cad_img_extension_junta_comercial').value;
                if(!(aux == 'pdf' || aux == 'doc' || aux == 'docx')){
                    msg += 'Formato de documento inválido para upload de junta comercial<br>';
                }
            }else{
                if(aux != '<i class="fas fa-sync"></i> Atualizar'){
                    msg += 'Coloque um documento de junta comercial para realizar o cadastro<br>';
                }
            }
            aux = document.getElementById('pre_cad_file_input_termo').files;
            if(aux.length != 0){
                if(!aux[0].type.includes('image')){
                    msg += 'Por favor coloque uma imagem do documento de identificação do sócio para realizar o cadastro<br>';
                }
            }else{
                if(aux != '<i class="fas fa-sync"></i> Atualizar'){
                    msg += 'Coloque um documento de identificação do sócio para realizar o cadastro<br>';
                }
            }
        }
    }
    if(msg.length > 0){
        control = false;
        document.getElementById('snackbar2').innerHTML = 'ERROS:<br><br>'+msg;
        show_snackbar('snackbar2');
    }
    return control;
}

function cli_select_on_change(input_search, select_search){
    document.getElementById(input_search).value = '';
    select = document.getElementById(select_search).value;
    if(select.includes('nome') || select.includes('address')){
        $('#'+input_search).unmask();
    }
    if(select.includes('cpf')){
        $('#'+input_search).mask('000.000.000-00');
    }
    if(select.includes('cnpj')){
        $('#'+input_search).mask('00.000.000/0000-00');
    }
}

function preenche_modal_pre_cadastro(obj){
    limpa_campos_pre_cadastro();
    document.getElementById('pre_cad_id').value = obj.id;
    document.getElementById('pre_cad_nome').value = obj.nome;
    document.getElementById('pre_cad_email').value = obj.email;
    document.getElementById('pre_cad_endereco').value = obj.endereco
    document.getElementById('pre_cad_bairro').value = obj.bairro;
    document.getElementById('pre_cad_cidade').value = obj.cidade;
    document.getElementById('pre_cad_estado').value = obj.estado;
    document.getElementById('pre_cad_tel_fixo').value = obj.tel_fixo;
    document.getElementById('pre_cad_tel_comercio').value = obj.tel_comercio;
    document.getElementById('pre_cad_tel_cel').value = obj.tel_cel;
    document.getElementById('pre_cad_cpf').value = obj.cpf;
    $('#pre_cad_observacoes').jqteVal(obj.observacoes);
    if(obj.img_contrato != null){
        document.getElementById('pre_cad_img_thumb').src = obj.img_contrato;
    }else{
        document.getElementById('pre_cad_img_thumb').src = 'assets/sem_img.png';
    }
    if(document.getElementById('tipo_cadastro').value == 'Pessoa Jurídica'){
        document.getElementById('pre_cad_nome_empresa').value = obj.nome_empresa;
        document.getElementById('pre_cad_email_empresa').value = obj.email_empresa;
        document.getElementById('pre_cad_end_empresa').value = obj.endereco_empresa;
        document.getElementById('pre_cad_bairro_empresa').value = obj.bairro_empresa;
        document.getElementById('pre_cad_cidade_empresa').value = obj.cidade_empresa;
        document.getElementById('pre_cad_estado_empresa').value = obj.estado_empresa;
        document.getElementById('pre_cad_cnpj').value = obj.cnpj;
        if(obj.contrato_social != null){
            document.getElementById('pre_cad_img_thumb_contrato_social').src = "assets/arquivo_valido.png";
        }
        else{
            document.getElementById('pre_cad_img_thumb_contrato_social').src = 'assets/arquivo.png';
        }
        if(obj.junta_comercial != null){
            document.getElementById('pre_cad_img_thumb_junta_comercial').src = "assets/arquivo_valido.png";
        }
        else{
            document.getElementById('pre_cad_img_thumb_junta_comercial').src = 'assets/arquivo.png';
        }
        if(obj.termo != null){
            document.getElementById('pre_cad_img_thumb_termo').src = obj.termo;
        }
        else{
            document.getElementById('pre_cad_img_thumb_termo').src = 'assets/sem_img.png';
        }
    }
}

function fetch_data_client(autocomplete, input_pagination, input_search, select_search, section, rent){
    $('#'+autocomplete).fadeOut();
    page = document.getElementById(input_pagination).value;
    input = document.getElementById(input_search).value;
    select = document.getElementById(select_search).value;
    $.ajax({
        url:"/clients/fetch_data?page="+page+"&input="+input+"&select="+select+"&rent="+rent,
        beforeSend: function(){
            document.getElementById(section).innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById(section).innerHTML = data;
        }
    });
}

$('#cli_input_search').keyup(function(){
    input = $(this).val();
    select = document.getElementById('cli_select_search').value;
    if (event.keyCode === 13) {
        fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0);
    }else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('clients/autocomplete', search, function(client){
                document.getElementById('cli_div_autocomplete').innerHTML ='';
                if(client.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#cli_div_autocomplete').append(data);
                    client.forEach(function(client){
                        cpf_cnpj = '';
                        if(client.cpf != null){
                        if(client.cpf.length > 0){
                            cpf_cnpj += '<strong>CPF: </strong>'+client.cpf+' ';
                        }}
                        if(client.cnpj != null){
                        if(client.cnpj.length > 0){
                            cpf_cnpj += '<strong>CNPJ: </strong>'+client.cnpj;
                        }}
                        if(client.tipo_cadastro == 'Pessoa Física' || client.tipo_cadastro == 'Cadastro Simplificado'){
                            data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\"><strong>Nome:</strong> "
                                    +client.nome+" <strong>Endereço: </strong>"
                                    +client.endereco+' '+cpf_cnpj+"</button></li>");
                        }else{
                            data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\"><strong>Nome:</strong> "
                                    +client.nome_empresa+" <strong>Endereço: </strong>"
                                    +client.endereco_empresa+' '+cpf_cnpj+"</button></li>");
                        }
                        data.find('button').click(function() {
                            fill_cli_searchInput(client, 'cli_input_search', 'cli_select_search', 'cli_div_autocomplete');
                        });
                        $('#cli_div_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#cli_div_autocomplete').append(data);
                    $('#cli_div_autocomplete').fadeIn();
                }
            });
        }
    }
});

function fill_cli_searchInput(client, input_search, select_search, autocomplete){
    select = document.getElementById(select_search).value;
    if(client.tipo_cadastro == 'Pessoa Jurídica'){
        field = client.nome_empresa;
    }else{
        field = client.nome;
    }
    if(select.includes('endereco')){
        if(client.tipo_cadastro == 'Pessoa Jurídica'){
            field = client.endereco_empresa;
        }else{
            field = client.endereco;
        }
    }
    if(select.includes('cpf')){
        field = client.cpf;
    }
    if(select.includes('cnpj')){
        field = client.cnpj;
    }
    document.getElementById(input_search).value = field;
    $('#'+autocomplete).fadeOut();
}

/* ABRIR MODAL HABILITAR/DELETAR */
function open_cli_modalDelCrtl(client, option){
    document.getElementById('cli_input_modalIdDelCrtl').value = client.id;
    if(option == 1){
        if(client.tipo_cadastro == 'Pessoa Física'){
            document.getElementById('cli_div_modalDelCrtl').innerHTML = "Você tem certeza que deseja desabilitar o cliente "+client.nome+" ?";
        }else{
            document.getElementById('cli_div_modalDelCrtl').innerHTML = "Você tem certeza que deseja desabilitar a empresa "+client.nome_empresa+" ?";
        }
        document.getElementById('cli_h4_modalDelCrtl').innerHTML = 'Desabilitar Cliente';
        document.getElementById('cli_button_modalDelCrtl').innerHTML = '<i class="fas fa-minus-circle text-white"></i> Desabilitar';
    }else{
        if(client.tipo_cadastro == 'Pessoa Física'){
            document.getElementById('cli_div_modalDelCrtl').innerHTML = "Você tem certeza que deseja habilitar o cliente "+client.nome+" ?";
        }else{
            document.getElementById('cli_div_modalDelCrtl').innerHTML = "Você tem certeza que deseja habilitar a empresa "+client.nome_empresa+" ?";
        }
        document.getElementById('cli_h4_modalDelCrtl').innerHTML = 'Habilitar Cliente';
        document.getElementById('cli_button_modalDelCrtl').innerHTML = '<i class="fas fa-check-circle"></i> Habilitar';
    }
    $('#cli_modalDelCrtl').modal();
}

/* BUTTON SE VAI CRIAR OU DELETAR */
function action_cli_modalDelCrtl(){
    var button_innerHTML = document.getElementById('cli_button_modalDelCrtl').innerHTML;
    if(button_innerHTML == '<i class="fas fa-minus-circle text-white"></i> Desabilitar'){
        enable_client();
    }else{
        able_client();
    }
}

/* DESABILITAR CLIENTE */
function enable_client(){
    id = document.getElementById('cli_input_modalIdDelCrtl').value;
    $.ajax({
        type: 'DELETE',
        url: 'clients/'+id,
        beforeSend: function(){
            document.getElementById('snackbar10').className = "beforeSend";
            document.getElementById('snackbar10').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar10').innerHTML = msg;
            show_snackbar('snackbar10');
            if(msg == 'Cliente desabilitado com sucesso'){
                fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0);
            }
        }
    });
}

/* HABILITAR CLIENTE */
function able_client(){
    id = document.getElementById('cli_input_modalIdDelCrtl').value;
    $.ajax({
        type: 'GET',
        url: 'clients/create/'+id,
        beforeSend: function(){
            document.getElementById('snackbar10').className = "beforeSend";
            document.getElementById('snackbar10').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar10').innerHTML = msg;
            show_snackbar('snackbar10');
            if(msg == 'Cliente habilitado com sucesso'){
                fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0);
            }
        }
    });
}

function aprovar_cliente(id){
    $.ajax({
        type: 'GET',
        url: 'clients/aprovar_cliente/'+id,
        beforeSend: function(){
            document.getElementById('snackbar11').className = "beforeSend";
            document.getElementById('snackbar11').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar11').innerHTML = msg;
            show_snackbar('snackbar11');
            if(msg == 'Cliente aprovado com sucesso'){
                fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0);
            }
        }
    });
}

function visualizar_documentos(obj){
    if(obj.img_contrato != null){
        document.getElementById('nome_img_contrato').innerHTML = obj.img_contrato;
        document.getElementById('arquivo_identificacao_cliente').classList.remove('d-none');
    }else{
        document.getElementById('arquivo_identificacao_cliente').classList.add('d-none');
    }

    if(obj.contrato_social != null){
        document.getElementById('nome_termo_contrato_social').innerHTML = obj.contrato_social;
        document.getElementById('arquivo_termo_contrato_social').classList.remove('d-none');
    }else{
        document.getElementById('arquivo_termo_contrato_social').classList.add('d-none');
    }

    if(obj.junta_comercial != null){
        document.getElementById('nome_termo_junta_comercial').innerHTML = obj.junta_comercial;
        document.getElementById('arquivo_termo_junta_comercial').classList.remove('d-none');
    }else{
        document.getElementById('arquivo_termo_junta_comercial').classList.add('d-none');
    }

    if(obj.termo != null){
        document.getElementById('nome_termo_autorizacao').innerHTML = obj.termo;
        document.getElementById('arquivo_termo_autorizacao').classList.remove('d-none');
    }else{
        document.getElementById('arquivo_termo_autorizacao').classList.add('d-none');
    }
    $('#modal_visualizar_documentos').modal();
}

function modal_abrir_documento(arquivo){
    window.open(document.getElementById(arquivo).innerHTML, 'fullscreen=yes');
}


