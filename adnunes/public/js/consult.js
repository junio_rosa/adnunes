function consult_fetch_data(url, input_pagination, section){
    page = document.getElementById(input_pagination).value;
    init_date = document.getElementById('consult_init_date').value;
    ending_date = document.getElementById('consult_ending_date').value;
    $.ajax({
        url:url+"?page="+page+"&init_date="+init_date+"&ending_date="+ending_date,
        beforeSend: function(){
            document.getElementById(section).innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById(section).innerHTML = data;
            navigate_section(links, 'consults');
            change_consult();
        }
    });
}

function consult_qty_equip(){
    page = document.getElementById('qty_equip_pagination').value;
    input = document.getElementById('consult_qty_equip_input').value;
    $.ajax({
        url:"consult/qty_equip?page="+page+"&input="+input,
        beforeSend: function(){
            document.getElementById('section_qty_equip').innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById('section_qty_equip').innerHTML = data;
            change_consult();
            $('#consult_qty_equip_autocomplete').fadeOut();
        }
    });
}

/* GERAR PDF */
function gerar_relatorio_equipamento(){
    var form = document.createElement("form");
    document.body.appendChild(form);
    form.method = "POST";
    form.action = "/api/consult/equip_pdf";
    form.target = '_blank';
    $('<input />').attr('type', 'hidden')
        .attr('name', 'id')
        .attr('value', document.getElementById('consult_qty_equip_input').value)
        .appendTo(form);
    form.submit();
}

function action_consult_search(){
    control = document.getElementById('consult_control_pagination').value;
    if(control == 1){
        consult_fetch_data('/consult/equip_in', 'equip_in_pagination', 'section_equip_entry');
    }
    if(control == 2){
        consult_fetch_data('/consult/equip_out', 'equip_out_pagination', 'section_equip_out');
    }
    if(control == 3){
        consult_fetch_data('/consult/cash_flow', 'cash_flow_pagination', 'section_cash_flow');
    }
    if(control == 4){
        consult_qty_equip();
    }
}

function change_consult(){
    document.getElementById('section_consult_by_date').classList.remove('d-none');
    document.getElementById('section_consult_by_name').classList.add('d-none');
    var consult_links = ['section_equip_entry', 'section_equip_out', 'section_cash_flow', 'section_qty_equip'];
    var consult_type = document.getElementById('consult_select_search').value;
    if(consult_type == 'equip_in'){
        document.getElementById('consult_control_pagination').value = 1;
        consult_fades('section_equip_entry', consult_links);
    }
    if(consult_type == 'equip_out'){
        document.getElementById('consult_control_pagination').value = 2;
        consult_fades('section_equip_out', consult_links);
    }
    if(consult_type == 'cash_flow'){
        document.getElementById('consult_control_pagination').value = 3;
        consult_fades('section_cash_flow', consult_links);
    }
    if(consult_type == 'qty_equip'){
        document.getElementById('section_consult_by_date').classList.add('d-none');
        document.getElementById('section_consult_by_name').classList.remove('d-none');
        document.getElementById('consult_control_pagination').value = 4;
        consult_fades('section_qty_equip', consult_links);
    }
}

function consult_fades(section, array_consults){
    i = 0;
    while(i < array_consults.length){
        if(array_consults[i] == section){
            $('#'+array_consults[i]).fadeIn();
        }else{
            $('#'+array_consults[i]).fadeOut();
        }
        i++;
    }
}

$('#consult_qty_equip_input').keyup(function(event){
    input = $(this).val();
    select = 'name';
    if (event.keyCode === 13) {
        action_consult_search();
    }
    else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('products/autocomplete', search, function(product){
                document.getElementById('consult_qty_equip_autocomplete').innerHTML ='';
                if(product.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#consult_qty_equip_autocomplete').append(data);
                    product.forEach(function(product){
                        data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\">"
                                    +"<img class= 'rounded' src='"+product.thumb+"' style='height:30px; width:auto;'> "
                                    +"<strong>Nome:</strong> "
                                    +product.name+" <strong>Estoque: </strong>"
                                    +product.qty_stock+' <strong> Diária: </strong>'
                                    +number_format(product.daily_value, 2, ',', '.')+"</button></li>");
                        data.find('button').click(function() {
                            fill_prod_searchInput(product, 'consult_qty_equip_input', 'name', 'consult_qty_equip_autocomplete');
                        });
                        $('#consult_qty_equip_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#consult_qty_equip_autocomplete').append(data);
                    $('#consult_qty_equip_autocomplete').fadeIn();
                }
            });
        }
    }
});
