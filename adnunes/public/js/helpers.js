/* VALIDAÇÃO CPF */
function cpf_validation(input, small, hit_msg, error_msg) {
    var cpf = document.getElementById(input).value;
    pontuation = /\.|\-/g;
    cpf = cpf.toString().replace(pontuation, "");

    var numbers, digits, sum, i, result, equal_digits;
    equal_digits = 1;
    if (cpf.length < 11) {
        input_invalid(input);
        small_danger(small, error_msg);
    }
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1)) {
            equal_digits = 0;
            break;
        }
    if (!equal_digits) {
        numbers = cpf.substring(0, 9);
        digits = cpf.substring(9);
        sum = 0;
        for (i = 10; i > 1; i--)
            sum += numbers.charAt(10 - i) * i;
        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result != digits.charAt(0)) {
            input_invalid(input);
            small_danger(small, error_msg);
        }
        numbers = cpf.substring(0, 10);
        sum = 0;
        for (i = 11; i > 1; i--)
            sum += numbers.charAt(11 - i) * i;
        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result != digits.charAt(1)) {
            input_invalid(input);
            small_danger(small, error_msg);
        } else {
            input_valid(input);
            small_success(small, hit_msg);
        }
    } else {
        input_invalid(input);
        small_danger(small, error_msg);
    }
}

/* VALIDAÇÃO CNPJ */
function cnpj_validation(input_cnpj, small_cnpj, hit_msg, error_msg) {
    var cnpj = document.getElementById(input_cnpj).value;
    exp = /[^\d]+/g;
    cnpj = cnpj.toString().replace(exp, "");
    if (cnpj == '') {
        input_invalid(input_cnpj);
        small_danger(small_cnpj, error_msg);
    } else {

        if (cnpj.length != 14) {
            input_invalid(input_cnpj);
            small_danger(small_cnpj, error_msg);
        } else {

            // Elimina CNPJs invalidos conhecidos
            if (cnpj == "00000000000000" ||
                cnpj == "11111111111111" ||
                cnpj == "22222222222222" ||
                cnpj == "33333333333333" ||
                cnpj == "44444444444444" ||
                cnpj == "55555555555555" ||
                cnpj == "66666666666666" ||
                cnpj == "77777777777777" ||
                cnpj == "88888888888888" ||
                cnpj == "99999999999999") {
                    input_invalid(input_cnpj);
                    small_danger(small_cnpj, error_msg);
            } else {
                // Valida DVs
                size = cnpj.length - 2;
                numbers = cnpj.substring(0, size);
                digits = cnpj.substring(size);
                sum = 0;
                pos = size - 7;
                for (i = size; i >= 1; i--) {
                    sum += numbers.charAt(size - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = sum % 11 < 2 ? 0 : 11 - sum % 11;
                if (resultado != digits.charAt(0)) {
                    input_invalid(input_cnpj);
                    small_danger(small_cnpj, error_msg);
                } else {
                    size = size + 1;
                    numbers = cnpj.substring(0, size);
                    sum = 0;
                    pos = size - 7;
                    for (i = size; i >= 1; i--) {
                        sum += numbers.charAt(size - i) * pos--;
                        if (pos < 2)
                            pos = 9;
                    }
                    resultado = sum % 11 < 2 ? 0 : 11 - sum % 11;
                    if (resultado != digits.charAt(1)) {
                        input_invalid(input_cnpj);
                        small_danger(small_cnpj, error_msg);
                    } else {
                        input_valid(input_cnpj);
                        small_success(small_cnpj, hit_msg);
                    }
                }
            }
        }
    }
}

/* VALIDAÇÃO EMAIL */
function emailValidation(input, small_email, msg_acerto, msg_erro) {
    email = document.getElementById(input).value;
    if(!email.includes("@")){
        input_invalid(input);
        small_danger(small_email, msg_erro);
    }else{
        email = email.split('@');
        usuario = email[0];
        dominio = email[1];

        if ((usuario.length >= 1) &&
            (dominio.length >= 3) &&
            (usuario.search("@") == -1) &&
            (dominio.search("@") == -1) &&
            (usuario.search(" ") == -1) &&
            (dominio.search(" ") == -1) &&
            (dominio.search(".") != -1) &&
            (dominio.indexOf(".") >= 1) &&
            (dominio.lastIndexOf(".") < dominio.length - 1)) {
                input_valid(input);
                small_success(small_email, msg_acerto);
        } else {
            input_invalid(input);
            small_danger(small_email, msg_erro);
        }
    }
}

/* DEIXAR SMALL DE FORMULÁRIO COM O TEXTO DANGER E MSG PERSONALIZADA */
function small_danger(small, error_msg) {
    document.getElementById(small).classList.remove('text-secondary');
    document.getElementById(small).classList.remove('text-success');
    document.getElementById(small).classList.add('text-danger');
    document.getElementById(small).innerHTML = error_msg;
}

/* DEIXAR SMALL DE FORMULÁRIO COM O TEXTO SECONDARY E MSG PERSONALIZADA */
function small_default(small, default_msg) {
    document.getElementById(small).classList.remove('text-danger');
    document.getElementById(small).classList.remove('text-success');
    document.getElementById(small).classList.add('text-secondary');
    document.getElementById(small).innerHTML = default_msg;
}

/* DEIXAR SMALL DE FORMULÁRIO COM O TEXTO SUCCESS E MSG PERSONALIZADA */
function small_success(small, hit_msg) {
    document.getElementById(small).classList.remove('text-danger');
    document.getElementById(small).classList.remove('text-secondary');
    document.getElementById(small).classList.add('text-success');
    document.getElementById(small).innerHTML = hit_msg;
}

/* DEIXAR CAMPO INPUT VALIDADO */
function input_valid(input) {
    document.getElementById(input).classList.remove('is-invalid');
    document.getElementById(input).classList.add('is-valid');
}

/* DEIXAR CAMPO INPUT INVALIDADO */
function input_invalid(input) {
    document.getElementById(input).classList.remove('is-valid');
    document.getElementById(input).classList.add('is-invalid');
}

/* VOLTAR COM APARÊNCIA PADRÃO DO INPUT */
function input_default(input) {
    document.getElementById(input).classList.remove('is-valid');
    document.getElementById(input).classList.remove('is-invalid');
    document.getElementById(input).value = '';
}

/* FUNÇÃO PARA EXIBIR SNACKBAR */
function show_snackbar(snack_id) {
    var x = document.getElementById(snack_id);
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}

/* LIMPAR MODAL */
function clear_modal_fields(array_input, array_small) {
    var i = 0;
    while (i < array_input.length) {
        input_default(array_input[i]);
        i++;
    }
    i = 0;
    while (i < array_small.length) {
        small_default(array_small[i].small, array_small[i].msg);
        i++;
    }
}

/* VALIDAÇÃO DE CAMPOS DE FORMULÁRIO POR TAMANHO */
function field_validation(input, small, hit_msg, error_msg, size_input){
    var input_field = document.getElementById(input).value;
    if(input_field.length < size_input){
        small_danger(small, error_msg);
        input_invalid(input);
    }else{
        small_success(small, hit_msg);
        input_valid(input);
    }
}

/* VALIDAÇÃO DE DATAS (NÃO PODE EXISTIR DATA COM ANO DE MAIS DE 4 DÍGITOS NO BANCO DE DADOS) */
function field_date_validation(input, small, hit_msg, error_msg){
    var date = document.getElementById(input).value;
    if(date.length > 10){
        small_danger(input, error_msg);
        input_invalid(input);
    }else{
        field_validation(input, small, hit_msg, error_msg, 10);
    }
}

/* FORMATAÇÃO DE NÚMERO */
function number_format(number, decimals, dec_point, thousands_point) {
    /* VERIFICA SE O NÚMERO É INFINITO OU NULO */
    if (number == null || !isFinite(number)) {
        throw new TypeError("Número não é válido");
    }
    /* CONDIÇÃO PARA CASAS DEPOIS DA VÍRGULA */
    if (!decimals) {
        var len = number.toString().split('.').length;
        decimals = len > 1 ? len : 0;
    }
    /* CONDIÇÃO PARA PONTO DE FRAÇÕES */
    if (!dec_point) {
        dec_point = '.';
    }
    /* CONDIÇÃO PARA PONTO DA CASA DOS MILHARES */
    if (!thousands_point) {
        thousands_point = ',';
    }

    number = parseFloat(number).toFixed(decimals);
    number = number.replace(".", dec_point);
    var splitNum = number.split(dec_point);
    splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
    number = splitNum.join(dec_point);
    return number;
}

/* PREVIEW DE IMAGEM */
function image_preview(img_thumb) {
    if (this.files && this.files[0]) {
        var imagem = 1;
        if(!this.files[0].type.includes('image')){
            imagem = 0;
        }
        var obj = new FileReader();
        obj.onload = function (data) {
            if(imagem){
                document.getElementById(img_thumb).src = data.target.result;
            }else{
                document.getElementById(img_thumb).src = 'assets/imagem_nao_suportada.png';
            }
            document.getElementById(img_thumb).style.display = "block";
        }
        obj.readAsDataURL(this.files[0]);
    }
}

/* VALIDAÇÃO DE DINHEIRO */
function money_validation(input, small, hit_msg, error_msg){
    var money = document.getElementById(input).value;
    if(money == ''){
        small_danger(small, error_msg);
        input_invalid(input);
    }else{
        if(money.length > 16){
            small_danger(small, 'O tamanho máximo do campo é de 12 dígitos');
            input_invalid(input);
        }else{
            money = parseFloat(money.replace(',', '.'));
            small_success(small, hit_msg);
            input_valid(input);
        }
    }
}

/* VALIDAÇÃO DE HORA */
function hour_validation(input, small, hit_msg, error_msg){
    var field = document.getElementById(input).value;
    if(field == '' || field.length < 5){
        input_invalid(input);
        small_danger(small, error_msg);
    }else{
        field = field.split(':');
        hours = parseInt(field[0]);
        if(hours > 23){
            input_invalid(input);
            small_danger(small, error_msg);
        }else{
            min = parseInt(field[1]);
            if(min > 59){
                input_invalid(input);
                small_danger(small, error_msg);
            }else{
                input_valid(input);
                small_success(small, hit_msg);
            }
        }
    }
}

/* VALIDAÇÃO DIA DE PAGAMENTO */
function qty_validation(input, small, hit_msg, error_msg){
    var field = document.getElementById(input).value;
    if(field.length < 1){
        small_danger(small, error_msg);
        input_invalid(input);
    }else{
        field = parseInt(field);
        if(field < 1){
            small_danger(small, 'Digite um valor maior que 0');
            input_invalid(input);
        }else{
            small_success(small, hit_msg);
            input_valid(input);
        }
    }
}

/* INSERIR LINHA NA TABELA */
function insert_table_row(table_name, insert_row, parameter, identifier) {
    var table = $('#' + table_name + '>tbody>tr');
    var control = false;
    var i = 0;
    if(parameter != ''){
        while (i < table.length) {
            var cell = table[i].cells[identifier].textContent;
            parameter = remove_tones(parameter).toUpperCase();
            cell = remove_tones(cell).toUpperCase();
            if (parameter < cell) {
                $('#' + table_name + '> tbody > tr').eq(i).before(insert_row);
                control = true;
                break;
            }
            i++;
        }
    }
    if (!control) {
        $('#' + table_name + '>tbody').append(insert_row);
    }
}

/* REMOVER ACENTOS PARA COMPARAÇÃO */
function remove_tones(string_withTones) {
    var string = string_withTones;
    var map_toneHex = {
        a: /[\xE0-\xE6]/g,
        A: /[\xC0-\xC6]/g,
        e: /[\xE8-\xEB]/g,
        E: /[\xC8-\xCB]/g,
        i: /[\xEC-\xEF]/g,
        I: /[\xCC-\xCF]/g,
        o: /[\xF2-\xF6]/g,
        O: /[\xD2-\xD6]/g,
        u: /[\xF9-\xFC]/g,
        U: /[\xD9-\xDC]/g,
        c: /\xE7/g,
        C: /\xC7/g,
        n: /\xF1/g,
        N: /\xD1/g,
    };

    for (var letter in map_toneHex) {
        var regular_expression = map_toneHex[letter];
        string = string.replace(regular_expression, letter);
    }

    return string;
}

/* FUNÇÃO PARA APAGAR UMA LINHA DE UMA TABELA */
function delete_table_row(table_name, id){
    var linhas = $('#'+table_name+'>tbody>tr');
    e = linhas.filter(function (i, element) {
        return element.cells[0].textContent == id;
    });
    if (e){
        e.remove();
    }
}

/* FUNÇÃO PARA ARRUMAR FORMATO MONETÁRIO EM UM NÚMERO FLOAT */
function convert_float(number){
    number = number.split('.').join('');
    number = number.replace(',', '.');
    return parseFloat(number);
}

function send_whatsapp(){
    window.open("https://api.whatsapp.com/send?phone=553799869675&text=Olá, Tudo bem? Desejo saber mais sobre os seus serviços e preços.");
}

function ValorPorExtenso(valor) {
    valor = String(valor).replace('.', ',');
    var c = true;
    var ex = [
        ["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
        ["dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"],
        ["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
        ["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
    ];
    var a, n, v, i, n = valor.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
    for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
        j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
        if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
        for(a = -1, l = v.length; ++a < l; t = ""){
            if(!(i = v[a] * 1)) continue;
            i % 100 < 20 && (t += ex[0][i % 100]) ||
            i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
            s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
            ((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("?o", "?es") : ex[3][t]) : ""));
        }
        a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
        a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
    }
    return r.join(e);
}

function validacao_senhas(input, input2, small1, small2){
    senha = document.getElementById(input).value;
    repita_senha = document.getElementById(input2).value;
    if(senha.length < 8){
        input_invalid(input);
        small_danger(small1, 'A senha deve possuir no mínimo 8 caracteres');
        input_invalid(input2);
        small_danger(small2, 'A senha deve ser válida para verificar uma segunda vez');
    }else{
        if(senha != repita_senha){
            input_invalid(input2);
            small_danger(small2, 'As senhas não são iguais');
        }else{
            input_valid(input2);
            small_success(small2, 'As senhas são iguais');
        }
    }
}

function mostrar_password(input, button) {
    var x = document.getElementById(input);
    if (x.type === "password") {
        x.type = "text";
        document.getElementById(button).innerHTML = '<i class="fas fa-eye-slash"></i>';
    } else {
        x.type = "password";
        document.getElementById(button).innerHTML = '<i class="fas fa-eye"></i>';
    }
}

function validacao_email(input, small_email, msg_acerto, msg_erro) {
    email = document.getElementById(input).value;
    if(!email.includes("@")){
        input_invalid(input);
        small_danger(small_email, msg_erro);
    }else{
        email = email.split('@');
        usuario = email[0];
        dominio = email[1];

        if ((usuario.length >= 1) &&
            (dominio.length >= 3) &&
            (usuario.search("@") == -1) &&
            (dominio.search("@") == -1) &&
            (usuario.search(" ") == -1) &&
            (dominio.search(" ") == -1) &&
            (dominio.search(".") != -1) &&
            (dominio.indexOf(".") >= 1) &&
            (dominio.lastIndexOf(".") < dominio.length - 1)) {
                input_valid(input);
                small_success(small_email, msg_acerto);
        } else {
            input_invalid(input);
            small_danger(small_email, msg_erro);
        }
    }
}

function documento_preview(file_input, documento_thumb) {
    var extensao = file_input.value.split('.')[1];
    if (this.files && this.files[0]) {
        var documento = 1;
        if(!(extensao == 'pdf' || extensao == 'doc' || extensao == 'docx')){
            documento = 0;
        }

        if(documento){
            document.getElementById(documento_thumb).src = 'assets/arquivo_valido.png';
        }else{
            document.getElementById(documento_thumb).src = 'assets/arquivo_invalido.png';
        }
        document.getElementById(documento_thumb).style.display = "block";
    }
}
