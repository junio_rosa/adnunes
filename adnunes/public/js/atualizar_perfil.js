function abre_modal_atualiza_usuario(obj){
    document.getElementById('updateUser_id').value = obj.id;
    document.getElementById('updateUser_nome').value = obj.name;
    document.getElementById('updateUser_email').value = obj.email;
    document.getElementById('updateUser_senha').value = '';
    document.getElementById('updateUser_repita_senha').value = '';
    document.getElementById('updateUser_id').classList.add('d-none');
    validarFormUpdateUser();
    $('#modal_update_user').modal();
}

function validarFormUpdateUser(){
    validacao_email('updateUser_email', 'updateUser_small_email', 'Campo Validado', 'Campo e-mail inválido');
    field_validation('updateUser_nome', 'updateUser_small_nome', 'Campo Validado', 'Campo nome inválido', 1);
    validacao_senhas('updateUser_senha', 'updateUser_repita_senha', 'updateUser_small_senha', 'updateUser_small_repita_senha');
}

function formUpdateUser_valido(){
    controle = true;
    validarFormUpdateUser();
    nome = document.getElementById('updateUser_small_nome').innerHTML;
    email = document.getElementById('updateUser_small_email').innerHTML;
    senha = document.getElementById('updateUser_small_senha').innerHTML;
    repita_senha = document.getElementById('updateUser_small_repita_senha').innerHTML;
    msg_erro = '';
    if(nome != 'Campo Validado'){
        msg_erro += nome+'<br>';
    }
    if(email != 'Campo Validado'){
        msg_erro += email+'<br>';
    }
    if(senha != 'Campo Validado'){
        msg_erro += senha+'<br>';
    }
    if(repita_senha != 'As senhas são iguais'){
        msg_erro += repita_senha+'<br>';
    }
    if(msg_erro != ''){
        document.getElementById('snackbar9').innerHTML = 'ERROS:<br><br>'+msg_erro;
        show_snackbar('snackbar9');
        controle = false;
    }
    return controle;
}

function action_update_user() {
    if(formUpdateUser_valido()){
        user = {
            id: document.getElementById('updateUser_id').value,
            nome: document.getElementById('updateUser_nome').value,
            email: document.getElementById('updateUser_email').value,
            senha: document.getElementById('updateUser_senha').value,
        }
        $.ajax({
            method: 'POST',
            url: 'atualizar_dados_perfil',
            data: user,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function(){
                document.getElementById('snackbar9').className = "beforeSend";
                document.getElementById('snackbar9').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
            },
            success: function(msg){
                document.getElementById('snackbar9').className.replace("beforeSend", "");
                document.getElementById('snackbar9').innerHTML = msg;
                show_snackbar('snackbar9');
                if(msg == 'Perfil atualizado com sucesso'){
                    atualizar_header();
                }
            }
        });
    }
}

function atualizar_header(){
    $.ajax({
        method: 'GET',
        url: 'atualizar_header/',
        success:function(data){
            document.getElementById('header_navbar').innerHTML = data;
        }
    });
}
