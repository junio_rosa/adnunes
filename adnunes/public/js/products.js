/* ABRIR MODAL PRODUTO PARA CADASTRO OU EDIÇÃO */
function open_prod_modal(obj){
    if(obj == 0){
        var array_input = [
            'prod_input_modalName', 'prod_input_modalUnits', 'prod_input_modalQtyStock',
            'prod_input_modalPriceCost', 'prod_input_modalDailyValue'];
        var array_small = [{
            small: 'prod_small_modalName',
            msg: 'Digite o nome do produto'
        }, {
            small: 'prod_small_modalUnits',
            msg: 'Digite o número de unidades do produto'
        },{
            small: 'prod_small_modalQtyStock',
            msg: 'Digite o estoque do produto'
        },{
            small: 'prod_small_modalPriceCost',
            msg: 'Digite o valor do produto'
        },{
            small: 'prod_small_modalDailyValue',
            msg: 'Digite o valor da diária'
        }];
        clear_modal_fields(array_input, array_small);
        var img = document.getElementById('prod_img_modalThumb');
        document.getElementById('prod_input_modalFileExtension').value = '';
        document.getElementById('prod_select_is_rent').value = 1;
        img.src = "assets/sem_img.png";
        document.getElementById('prod_input_modalThumb').value = '';
        document.getElementById('prod_h4_modalTitle').innerHTML = 'Adicionar Produto';
        document.getElementById('prod_button_modalProd').innerHTML = '<i class="fas fa-plus-circle text-whiter"></i> Cadastrar';
    }else{
        document.getElementById('prod_h4_modalTitle').innerHTML = 'Editar Produto';
        document.getElementById('prod_button_modalProd').innerHTML = '<i class="fas fa-sync"></i> Atualizar';
        var img = document.getElementById('prod_img_modalThumb');
        img.src = obj.thumb;
        document.getElementById('prod_select_is_rent').value = obj.is_rent;
        document.getElementById('prod_input_modalFileExtension').value = '';
        document.getElementById('prod_input_modalThumb').value = '';
        document.getElementById('prod_input_modalId').value = obj.id;
        document.getElementById('prod_input_modalName').value = obj.name;
        document.getElementById('prod_input_modalUnits').value = obj.units;
        document.getElementById('prod_input_modalQtyStock').value = obj.qty_stock;
        document.getElementById('prod_input_modalPriceCost').value = number_format(obj.price_cost, 2, ',', '.');
        document.getElementById('prod_input_modalDailyValue').value = number_format(obj.daily_value, 2, ',', '.');
        prod_form_validate();
    }
    $('#prod_modal').modal();
}

/* CLICK DO BUTTON PARA THUMB DE PRODUTO */
$('#prod_button_modalThumb').click(function(){
    $("#prod_input_modalThumb").click();
});

/* VALIDAÇÃO DE FORMULÁRIO */
function prod_form_validate(){
    field_validation('prod_input_modalName', 'prod_small_modalName', 'Campo Validado', 'Campo nome inválido', 1);
    qty_validation('prod_input_modalUnits', 'prod_small_modalUnits', 'Campo Validado', 'Campo unidades inválido');
    stock_validation();
    money_validation('prod_input_modalPriceCost', 'prod_small_modalPriceCost', 'Campo Validado', 'Campo valor do produto inválido');
    money_validation('prod_input_modalDailyValue', 'prod_small_modalDailyValue', 'Campo Validado', 'Campo valor da diária inválido');
}

/* ACTION CADASTRO/ATUALIZAÇÃO */
function action_modal_product(){
    var button = document.getElementById('prod_button_modalProd').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-whiter"></i> Cadastrar'){
        form_register_product();
    }else{
        update_product();
    }
}

/* FUNÇÃO PARA REALIZAR CADASTRO */
function form_register_product(){
    if(form_prod_valid()){
        product = new FormData();
        product.append('name', document.getElementById('prod_input_modalName').value);
        product.append('units', document.getElementById('prod_input_modalUnits').value);
        product.append('qty_stock', document.getElementById('prod_input_modalQtyStock').value);
        product.append('price_cost', document.getElementById('prod_input_modalPriceCost').value);
        product.append('daily_value', document.getElementById('prod_input_modalDailyValue').value);
        product.append('thumb', document.getElementById('prod_input_modalThumb').files[0]);
        product.append('extensao', document.getElementById('prod_input_modalFileExtension').value);
        product.append('is_rent', document.getElementById('prod_select_is_rent').value);
        $.ajax({
            type:"POST",
            url:'products',
            data:product,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar4').className = "beforeSend";
                document.getElementById('snackbar4').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
            },
            success:function(msg){
                document.getElementById('snackbar4').className.replace("beforeSend", "");
                document.getElementById('snackbar4').innerHTML = msg;
                show_snackbar('snackbar4');
                if(msg == 'Produto cadastrado com sucesso'){
                    fetch_data_product('prod_div_autocomplete', 'prod_input_pagination', 'prod_input_search', 'prod_select_search', 'section_products_table', 0);
                }
            }
        });
    }
}

/* FUNÇÃO PARA INFORMAR SE O FORMULÁRIO É VÁLIDO */
function form_prod_valid(){
    var control = true;
    prod_form_validate();
    var name = document.getElementById('prod_small_modalName').innerHTML;
    var units = document.getElementById('prod_small_modalUnits').innerHTML;
    var qty_stock = document.getElementById('prod_small_modalQtyStock').innerHTML;
    var price_cost = document.getElementById('prod_small_modalPriceCost').innerHTML;
    var daily_value = document.getElementById('prod_small_modalDailyValue').innerHTML;
    var imagem_destaque = document.getElementById('prod_input_modalThumb').files;
    var error_msg = '';
    if(name != 'Campo Validado'){
        error_msg += name+'<br>';
    }
    if(units != 'Campo Validado'){
        error_msg += units+'<br>';
    }
    if(qty_stock != 'Campo Validado'){
        error_msg += qty_stock+'<br>';
    }
    if(price_cost != 'Campo Validado'){
        error_msg += price_cost+'<br>';
    }
    if(daily_value != 'Campo Validado'){
        error_msg += daily_value+'<br>';
    }
    if(imagem_destaque.length != 0){
        if(!imagem_destaque[0].type.includes('image')){
            error_msg += 'Formato de imagem inválido para upload<br>';
        }
    }
    if(error_msg != ''){
        document.getElementById('snackbar4').innerHTML = 'ERROS:<br><br>'+error_msg;
        show_snackbar('snackbar4');
        control = false;
    }
    return control;
}

function fetch_data_product(autocomplete, input_pagination, input_search, select_search, section, rent){
    $('#'+autocomplete).fadeOut();
    page = document.getElementById(input_pagination).value;
    input = document.getElementById(input_search).value;
    select = document.getElementById(select_search).value;
    $.ajax({
        url:"products/fetch_data?page="+page+"&input="+input+"&select="+select+"&rent="+rent,
        beforeSend: function(){
            document.getElementById(section).innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById(section).innerHTML = data;
        }
    });
}

/* AUTOCOMPLETE */
$('#prod_input_search').keyup(function(event){
    input = $(this).val();
    select = document.getElementById('prod_select_search').value;
    if (event.keyCode === 13) {
        fetch_data_product('prod_div_autocomplete', 'prod_input_pagination', 'prod_input_search', 'prod_select_search', 'section_products_table', 'products', 0);
    }
    else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('products/autocomplete', search, function(product){
                document.getElementById('prod_div_autocomplete').innerHTML ='';
                if(product.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#prod_div_autocomplete').append(data);
                    product.forEach(function(product){
                        data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\">"
                                    +"<img class= 'rounded' src='"+product.thumb+"' style='height:30px; width:auto;'> "
                                    +"<strong>Nome:</strong> "
                                    +product.name+" <strong>Estoque: </strong>"
                                    +product.qty_stock+' Diária: </strong>'
                                    +number_format(product.daily_value, 2, ',', '.')+"</button></li>");
                        data.find('button').click(function() {
                            fill_prod_searchInput(product, 'prod_input_search', 'prod_select_search', 'prod_div_autocomplete');
                        });
                        $('#prod_div_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#prod_div_autocomplete').append(data);
                    $('#prod_div_autocomplete').fadeIn();
                }
            });
        }
    }
});

/* PREENCHE CAMPO DE BUSCA */
function fill_prod_searchInput(product, input_search, select_search, autocomplete){
    /* select = document.getElementById('prod_select_search').value; */
    field = product.name;
    document.getElementById(input_search).value = field;
    $('#'+autocomplete).fadeOut();
}

/* FUNÇÃO PARA ATULIZAR DADOS DO PRODUTO */
function update_product(){
    if(form_prod_valid()){
        product = new FormData();
        product.append('id', document.getElementById('prod_input_modalId').value);
        product.append('name', document.getElementById('prod_input_modalName').value);
        product.append('units', document.getElementById('prod_input_modalUnits').value);
        product.append('qty_stock', document.getElementById('prod_input_modalQtyStock').value);
        product.append('price_cost', document.getElementById('prod_input_modalPriceCost').value);
        product.append('daily_value', document.getElementById('prod_input_modalDailyValue').value);
        product.append('thumb', document.getElementById('prod_input_modalThumb').files[0]);
        product.append('extensao', document.getElementById('prod_input_modalFileExtension').value);
        product.append('is_rent', document.getElementById('prod_select_is_rent').value);
        $.ajax({
            type: 'POST',
            url: '/products/update',
            data: product,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar4').className = "beforeSend";
                document.getElementById('snackbar4').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
            },
            success: function(msg){
                document.getElementById('snackbar4').className.replace("beforeSend", "");
                document.getElementById('snackbar4').innerHTML = msg;
                show_snackbar('snackbar4');
                if(msg == 'Produto atualizado com sucesso'){
                    fetch_data_product('prod_div_autocomplete', 'prod_input_pagination', 'prod_input_search', 'prod_select_search', 'section_products_table', 0);
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

/* ABRIR MODAL HABILITAR/DELETAR */
function open_prod_modalDelCrtl(product, option){
    document.getElementById('prod_input_modalIdDelCrtl').value = product.id;
    if(option == 1){
        document.getElementById('prod_div_modalDelCrtl').innerHTML = "Você tem certeza que deseja desabilitar o produto "+product.name+" ?";
        document.getElementById('prod_h4_modalDelCrtl').innerHTML = 'Desabilitar Produto';
        document.getElementById('prod_button_modalDelCrtl').innerHTML = '<i class="fas fa-minus-circle text-white"></i> Desabilitar';
    }else{
        document.getElementById('prod_div_modalDelCrtl').innerHTML = "Você tem certeza que deseja habilitar o produto "+product.name+" ?";
        document.getElementById('prod_h4_modalDelCrtl').innerHTML = 'Habilitar Produto';
        document.getElementById('prod_button_modalDelCrtl').innerHTML = '<i class="fas fa-check-circle"></i> Habilitar';
    }
    $('#prod_modalDelCrtl').modal();
}

/* BUTTON SE VAI CRIAR OU DELETAR */
function action_prod_modalDelCrtl(){
    var button_innerHTML = document.getElementById('prod_button_modalDelCrtl').innerHTML;
    if(button_innerHTML == '<i class="fas fa-minus-circle text-white"></i> Desabilitar'){
        enable_product();
    }else{
        able_product();
    }
}

/* DESABILITAR PRODUTO */
function enable_product(){
    id = document.getElementById('prod_input_modalIdDelCrtl').value;
    $.ajax({
        type: 'DELETE',
        url: 'products/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            show_snackbar('snackbar3');
            if(msg == 'Produto desabilitado com sucesso'){
                fetch_data_product('prod_div_autocomplete', 'prod_input_pagination', 'prod_input_search', 'prod_select_search', 'section_products_table', 0);
            }
        }
    });
}

/* HABILITAR PRODUTO */
function able_product(){
    id = document.getElementById('prod_input_modalIdDelCrtl').value;
    $.ajax({
        type: 'GET',
        url: 'products/create/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            show_snackbar('snackbar3');
            if(msg == 'Produto habilitado com sucesso'){
                fetch_data_product('prod_div_autocomplete', 'prod_input_pagination', 'prod_input_search', 'prod_select_search', 'section_products_table', 0);
            }
        }
    });
}

/* PEGANDO A EXTENSÃO DO INPUT FILE */
function getting_file_extension(file_input, extension_input){
    document.getElementById(extension_input).value = file_input.value.split('.')[1];
}

function stock_validation(){
    button = document.getElementById('prod_button_modalProd').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-whiter"></i> Cadastrar'){
        qty_validation('prod_input_modalQtyStock', 'prod_small_modalQtyStock', 'Campo Validado', 'Campo estoque inválido');
    }else{
        field_validation('prod_input_modalQtyStock', 'prod_small_modalQtyStock', 'Campo Validado', 'Campo estoque inválido', 1);
    }
}
