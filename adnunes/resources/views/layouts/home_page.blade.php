<div class="container">
    <div class="card bg-transparent border-0">
        <div class="form-row">
            <div class="col-12 col-lg-6">
                <h1 style="color: #4d61a7">SOBRE NÓS</h1>
                <div class="text-justify text-secondary">
                    A ADNUNES  e uma microempresa de locação de materiais  de construção  civil.  É uma microempresa empresa familiar e com muita  dedicação e honestidade e tentamos  fazer o melhor  para o próximo.Fundada em 2015 pelo Adilson Aparecido Nunes.
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <img class="rounded float-lg-right" src="{{asset('assets/home/13.png')}}" style="width:350px; height: auto">
            </div>
        </div>
    </div>
</div>

<div class="jumbotron jumbotron-fluid w-100 vertical-center text-white" style="background-color: #4d61a7">
    <div class="container">
        <div class="form-row">
            <div class="col-12 col-lg-1 text-center">
                <i class="far fa-hand-peace fa-4x mt-1" style="color:#ffd400"></i>
            </div>
            <div class="col-12 col-lg-9 text-center text-lg-left mt-2 mt-lg-0">
                <h2>Fale Agora Conosco Pelo Whatsapp</h2>
                Estamos prontos para lhe atender.
            </div>
            <div class="col-12 col-lg-2 text-center mt-2 mt-lg-0">
                <button class="btn bg-button" onclick="send_whatsapp()">
                    Clique aqui <i class="fas fa-angle-double-right"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="container mt-lg-4">
    <div class="form-row">
        <div class="col-12 text-center mb-5">
            <h2 style='color:#4d61a7'>Ferramentas Para Aluguel</h2>
            Temos uma variedade de ferramentas para a sua reforma ou construção
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/5.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/4.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5 mt-sm-0">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/3.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5 mt-md-0">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/2.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5 mt-lg-0">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/1.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5 mt-lg-0">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/6.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/7.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/8.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/9.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/10.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/11.png')}}" style="width:150px; height: auto">
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mt-5">
            <div class="card bg-transparent border-0">
                <img class="rounded" src="{{asset('assets/home/12.png')}}" style="width:150px; height: auto">
            </div>
        </div>

    </div>
</div>

<div class="overflow-hidden jumbotron jumbotron-fluid w-100 mt-5 bg-cover"
    style="background-image: linear-gradient(to bottom, rgba(77, 97, 167,0.6) 0%,
        rgba(255,255,255,0.6) 100%), url({{asset('assets/home/13.png')}});
        min-height: 500px; max-height:800px">
    <div class="container text-white">
        <div class="form-row">

            <div class="col-12 text-center mb-5" style='text-shadow: 2px 2px #000'>
                <h1 style="color: #ffd400">Tipos de Segmentos:</h1>
                <h3>Ferramentas para vários segmentos.</h3>
            </div>

            <div class="col-6 col-md-4 text-center">
                <button class="bg-transparent border-0 text-white" style="cursor: context-menu">
                    <i class="fas fa-wrench fa-2x" style="color:#ffd400"></i>
                    <h3 style='text-shadow: 2px 2px #000'>Ferramentas para reparos gerais</h3>
                </button>
            </div>

            <div class="col-6 col-md-4 text-center">
                <button class="bg-transparent border-0 text-white" style="cursor: context-menu">
                    <i class="fas fa-hard-hat fa-2x" style="color:#ffd400"></i>
                    <h3 style='text-shadow: 2px 2px #000'>Ferramentas para construção</h3>
                </button>
            </div>

            <div class="col-6 col-md-4 mt-5 mt-md-0 text-center">
                <button class="bg-transparent border-0 text-white" style="cursor: context-menu">
                    <i class="fas fa-box-open fa-2x" style="color:#ffd400"></i>
                    <h3 style='text-shadow: 2px 2px #000'>Containers de armazenagem</h3>
                </button>
            </div>

            <div class="col-6 col-md-4 mt-5 text-center">
                <button class="bg-transparent border-0 text-white" style="cursor: context-menu">
                    <i class="fas fa-cut fa-2x" style="color:#ffd400"></i>
                    <h3 style='text-shadow: 2px 2px #000'>Serras e corta pisos</h3>
                </button>
            </div>

            <div class="col-6 col-md-4 mt-5 text-center">
                <button class="bg-transparent border-0 text-white" style="cursor: context-menu">
                    <i class="fas fa-charging-station fa-2x" style="color:#ffd400"></i>
                    <h3 style='text-shadow: 2px 2px #000'>Geradores</h3>
                </button>
            </div>

            <div class="col-6 col-md-4 mt-5 text-center">
                <button class="bg-transparent border-0 text-white" style="cursor: context-menu">
                    <i class="fas fa-wind fa-2x" style="color:#ffd400"></i>
                    <h3 style='text-shadow: 2px 2px #000'>Compressores</h3>
                </button>
            </div>
        </div>
    </div>
</div>
