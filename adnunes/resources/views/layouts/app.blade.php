<!DOCTYPE html>
<html lang="en">
<head>
    {{-- METAS --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {{-- FAVICON --}}
    <link rel = "icon" href = "{{ asset('assets/favicon.ico') }}">

    {{-- FONT-AWESOME --}}
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"
    />

    {{-- MATERIAL ICONS --}}
    <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
    />

    {{-- CSS BOOTSTRAP --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    {{-- CSS DO PROJETO --}}
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/snackbars.css') }}" rel="stylesheet">
    <link href="{{ asset('css/textarea.css') }}" rel="stylesheet">

    {{-- TÍTULO --}}
    <title>AD NUNES LOCAÇÃO</title>
</head>

<header>
    <div id="header_navbar">
        @hasSection('navbar')
            @yield('navbar')
        @endif
    </div>
</header>

{{-- SESSÃO PARA CONTEÚDO --}}
<body>
    <main role="main">
        <div class="container-fluid">
            <div class="row">
                @hasSection('content')
                    @yield('content')
                @endif
            </div>
        </div>
    </main>
    <section>
        @include('auth.update_modal')
    </section>
</body>

{{-- FOOTER --}}
<footer class="page-footer font-small mt-5 text-white" style="background-color: #4d61a7">
    <div class="footer-copyright text-center py-3">

        <div class="row">
            <div class="col-12 text-center">
                <h4>Adnunes Locação de Ferramentas para Construção</h4><hr>
            </div>

            <div class="col-12 col-lg-6">
                <iframe style="max-width: 350px; height: auto" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3734.371825130633!2d-46.05398568561777!3d-20.613695665254326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b43dbdc286ef9b%3A0x1f13ab9f94ec134d!2zQUQgTnVuZXMgTG9jYcOnw6Nv!5e0!3m2!1spt-BR!2sbr!4v1587064713354!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>

            <div class="col-12 col-lg-6">
                <div class="col-12 my-3">
                    <img id = "logo_navbar" src = "{{ asset('assets/logo2.png') }}">
                </div>
                <div>
                    <h6>End: Rua Antónia Silva Ramos nº 50 - Residencial Vitória - Capitólio-MG</h6>
                    <!-- Copyright -->
                </div>

                <div>
                    <h6><i class="fab fa-whatsapp"></i> Contato - Adilson (37) 99986-9675</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer font-small col-12 text-center" style="background-color: #2043bd; height: 60px"><hr>
        <a class="text-white" href="https://www.linkedin.com/in/j%C3%BAnio-rosa-94b5731b2/">© {{date('Y')}} Copyright: Júnio César Rosa</a>
    </div>
</footer>

{{-- BOOTSTRAP JAVASCRIPT --}}
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

{{-- HELPERS --}}
<script src="{{ asset('js/helpers.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/clients.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/products.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/rent.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/consult.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/atualizar_perfil.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/textarea.js')}}" type="text/javascript"></script>

{{-- JQUERY MASKS --}}
<script src="{{ asset('js/mask.js')}}" type="text/javascript"></script>

{{-- SESSÃO PARA JAVASCRIPT --}}
@hasSection('javascript')
    @yield('javascript')
@endif
</html>
