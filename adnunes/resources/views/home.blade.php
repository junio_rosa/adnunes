@extends('layouts.app')

<style>
    body{
        overflow-x: hidden;
    }
</style>

@section('navbar')
    @include('navbar.navbar_auth')
@endsection

@section('content')
    @include('layouts.home_page')
@endsection

@section('javascript')
<script type="text/javascript">
$(function(){
    document.getElementById('home').style.borderBottom = "thick solid rgb(246, 199, 27)";
});
</script>
@endsection
