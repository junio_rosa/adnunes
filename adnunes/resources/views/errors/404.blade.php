<!DOCTYPE html>
<html lang="en">
    <head>
        {{-- METAS --}}
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        {{-- FAVICON --}}
        <link rel = "icon" href = "{{ asset('assets/favicon.ico') }}">

        {{-- FONT-AWESOME --}}
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"
        />

        {{-- MATERIAL ICONS --}}
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />

        {{-- CSS BOOTSTRAP --}}
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        {{-- CSS DO PROJETO --}}
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
        <link href="{{ asset('css/snackbars.css') }}" rel="stylesheet">

        {{-- TÍTULO --}}
        <title>AD NUNES LOCAÇÃO</title>
    </head>

    <body class="total_vh">

        <div class="row h-100">
            <div class="col-sm-12 my-auto text-center">
                <img src = "{{ asset('assets/404.png') }}">
                <h2>
                    404 <br>
                </h2>
                <h3>
                    Página não encontrada
                </h3>
                <div class="font-weight-bold">
                    A página que você está procurando não existe ou um outro erro ocurreu.<br>
                    <a href="/">Clique aqui</a> para ser redirecionado para a página principal.
                </div>
            </div>
         </div>
    </body>
</html>
