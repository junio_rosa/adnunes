@extends('layouts.app')

@section('navbar')
    @include('navbar.navbar_guest')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center my-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Redefinir a senha') }}</div>

                <div class="card-body">
                    <form method="POST" id="form_reset_senha" action="{{ route('password.update') }}">
                        @csrf

                        <input type="text" id="token" name="token" value="{{ $token }}">

                        <div class="form-group col-12">
                            <label>E-mail:</label>
                            <input class="form-control" type="text" id="email" name="email" value="{{ $email ?? old('email') }}"
                                onfocusout="validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido')">
                            <small id="small_email" class="text-secondary">
                                Digite um e-mail válido
                            </small>
                        </div>

                        <div class="form-group col-12">
                            <label>Senha:</label>
                            <div class="input-group">
                                <input class="form-control" type="password" id="senha" name="password"
                                    onfocusout="field_validation('senha', 'small_senha', 'Campo Validado', 'Campo senha inválido', 8)">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary rounded-right" onclick="mostrar_password('senha', 'button_eye_senha')" id="button_eye_senha"  type="button">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                            <small id="small_senha" class="text-secondary">
                                Digite uma senha de no mínimo 8 caracteres
                            </small>
                        </div>

                        <div class="form-group col-12">
                            <label>Repita a senha:</label>
                            <div class="input-group">
                                <input class="form-control" type="password" id="repita_senha"
                                onfocusout="validacao_senhas('senha', 'repita_senha', 'small_senha', 'small_repita_senha')">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary rounded-right" onclick="mostrar_password('repita_senha', 'button_eye_repita_senha')" id="button_eye_repita_senha"  type="button">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </div>
                            </div>
                            <small id="small_repita_senha" class="text-secondary">
                                Digite novamente a senha
                            </small>
                        </div>

                        <div class="col-12 my-3 text-center" id="div_alert">
                            <div class="alert alert-primary" role="alert" id="type_alert">
                            </div>
                        </div>

                        <div class="form-group col-12 text-right">
                            <button type="submit" class="btn btn-primary">
                                <i class="fas fa-redo"></i> {{ __('Redefinir senha') }}
                            </button>
                        </div>
                    </form>

                    <div id="snackbar1"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(function(){
            document.getElementById('token').classList.add('d-none');
            validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido');
            document.getElementById('div_alert').classList.add('d-none');
            @if (isset($msg))
                mensagem = '';
                @foreach($msg as $m)
                    mensagem += "{{$m}}"+'<br>';
                @endforeach
                document.getElementById('div_alert').classList.remove('d-none');
                document.getElementById('type_alert').classList.add("{{$type}}");
                document.getElementById('type_alert').innerHTML = mensagem;
            @endif
        });

        $("#form_reset_senha").on('submit', function (e) {
            e.preventDefault();
            if(formulario_reset_senha_valido()){
                document.getElementById('form_reset_senha').submit();
            }
        });

        function formulario_reset_senha_valido(){
            validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido');
            field_validation('senha', 'small_senha', 'Campo Validado', 'Campo senha inválido', 8);
            validacao_senhas('senha', 'repita_senha', 'small_senha', 'small_repita_senha');
            var email = document.getElementById('small_email').innerHTML;
            var senha = document.getElementById('small_senha').innerHTML;
            var repita_senha = document.getElementById('small_repita_senha').innerHTML;
            var controle = true;
            var msg_erro = '';
            if(email == 'Campo e-mail inválido'){
                msg_erro += email+'<br>';
            }
            if(senha != 'Campo Validado'){
                msg_erro += senha+'<br>';
            }
            if(repita_senha != 'As senhas são iguais'){
                msg_erro += repita_senha+'<br>';
            }
            if(msg_erro != ''){
                document.getElementById('snackbar1').innerHTML = 'ERROS:<br><br>'+msg_erro;
                show_snackbar('snackbar1');
                controle = false;
            }
            return controle;
        }
    </script>
@endsection
