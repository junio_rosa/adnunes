@extends('layouts.app')

@section('navbar')
    @include('navbar.navbar_guest')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center my-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Esqueceu a senha:') }}</div>

                <div class="card-body">
                    <form method="POST" id='form_esqueceu_senha' action="{{ route('password.email') }}">
                        <div class="row">
                            @csrf
                            <div class="form-group col-12">
                                <label>E-mail:</label>
                                <input class="form-control" type="text" id="email" name="email"
                                    onfocusout="validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido')">
                                <small id="small_email" class="text-secondary">
                                    Digite um e-mail válido
                                </small>
                            </div>

                            <div class="col-12 my-3 text-center" id="div_alert">
                                <div class="alert alert-primary" role="alert" id="type_alert">
                                </div>
                            </div>

                            <div class="form-group col-12 text-right">
                                <a href="/login" class="btn btn-success">
                                    <i class="fas fa-arrow-circle-left"></i> Voltar
                                </a>

                                <button class="btn btn-primary mr-1" type="submit">
                                    <i class="fas fa-external-link-square-alt"></i> {{ __('Enviar um link para redefinir a senha') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <div id="snackbar1"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function(){
        document.getElementById('div_alert').classList.add('d-none');
        @if (isset($msg))
            mensagem = '';
            @foreach($msg as $m)
                mensagem += "{{$m}}"+'<br>';
            @endforeach
            document.getElementById('div_alert').classList.remove('d-none');
            document.getElementById('type_alert').classList.add("{{$type}}");
            document.getElementById('type_alert').innerHTML = mensagem;
        @endif
    });

    $("#form_esqueceu_senha").on('submit', function (e) {
        e.preventDefault();
        validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido');
        email = document.getElementById('small_email').innerHTML;
        if(email != 'Campo e-mail inválido'){
            document.getElementById('form_esqueceu_senha').submit();
        }else{
            document.getElementById('snackbar1').innerHTML = 'ERROS:<br><br>'+email;
            show_snackbar('snackbar1');
        }
    });
</script>
@endsection
