@extends('layouts.app')

@section('navbar')
    @include('navbar.navbar_auth')
@endsection

@section('content')
<div class="container">
    <div class="card border my-4">
        <div class="card-header">
            <div class="container">
                <h4 class="card-title text-secondary">
                    Painel Administrativo dos Produtos
                </h4>
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="input-group">
                            <input type="text" class="form-control" id="prod_input_search" placeholder="Busque por produtos..." autocomplete="on">
                            <div class="input-group-append">
                                <button class="btn btn-secondary rounded-right" type="button" onclick="fetch_data_product('prod_div_autocomplete', 'prod_input_pagination', 'prod_input_search', 'prod_select_search', 'section_products_table', 'products', 0)">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>

                        <div id="prod_div_autocomplete" class="dropdown"></div>
                    </div>

                    <div class="form-group col-12">
                        <label for = 'prod_select_search'>Selecione o tipo de busca:</label>
                        <select class='form-control w-100' id='prod_select_search'>
                            <option value='name' selected='selected'>Buscar por Nome</option>
                            <option value='name_del'>Buscar por Nome (Produtos desabilitados)</option>
                        </select>
                    </div>

                    <div class="form-group ml-auto">
                        <button class="btn btn-primary text-white"
                            onclick="open_prod_modal(0)">
                            <i class="fas fa-tags"></i> Novo produto
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <section id="section_products_table">
            @include('products.table_products')
        </section>

        <input class="d-none" id="prod_input_pagination" value="1">
    </div>
</div>

<div class = "modal fade" id = "prod_modalDelCrtl" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-md" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted" id='prod_h4_modalDelCrtl'></h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">
                <div class="row">
                    <div class="col-12 mb-5" id="prod_div_modalDelCrtl">
                    </div>
                    <input class="d-none" id="prod_input_modalIdDelCrtl">
                </div>

                <div class="form-group col-12 text-right">
                    <button class="btn btn-primary mr-1" id='prod_button_modalDelCrtl' type="button" onclick="action_prod_modalDelCrtl()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>

                <div id="snackbar3">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>

<div class = "modal fade" id = "prod_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-lg" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted" id="prod_h4_modalTitle"></h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    <input class="d-none" id="prod_input_modalId">

                    <div class="form-group col-12">
                        <label for="prod_input_modalName">
                            Nome:
                        </label>
                        <input class="form-control w-100"
                                type="text"
                                id="prod_input_modalName" autocomplete="on"
                                onfocusout="field_validation('prod_input_modalName', 'prod_small_modalName', 'Campo Validado', 'Campo nome inválido', 1)">
                        <small id="prod_small_modalName" class="text-secondary">
                            Digite o nome do produto
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label for="prod_input_modalUnits">
                            Nº de unidades:
                        </label>
                        <input class="form-control w-100"
                                type="text"
                                id="prod_input_modalUnits" autocomplete="on"
                                onfocusout="qty_validation('prod_input_modalUnits', 'prod_small_modalUnits', 'Campo Validado', 'Campo unidades inválido')">
                        <small id="prod_small_modalUnits" class="text-secondary">
                            Digite o número de unidades do produto
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label>Selecione o tipo de produto:</label>
                        <select class='form-control w-100' id='prod_select_is_rent'>
                            <option value='1' selected='selected'>Produto para aluguel</option>
                            <option value='0'>Produto para venda</option>
                        </select>
                    </div>

                    <div class="form-group col-12">
                        <label for="prod_input_modalQtyStock">
                            Quantidade de produtos no estoque:
                        </label>
                        <input class="form-control w-100"
                                type="text"
                                id="prod_input_modalQtyStock" autocomplete="on"
                                onfocusout="stock_validation()">
                        <small id="prod_small_modalQtyStock" class="text-secondary">
                            Digite o estoque do produto
                        </small>
                    </div>

                    <div class="form-group col-12">
                        <label for="prod_input_modalPriceCost">
                            Valor do produto:
                        </label>
                        <input class="form-control w-100"
                                type="text"
                                id="prod_input_modalPriceCost" autocomplete="on"
                                onfocusout="money_validation('prod_input_modalPriceCost', 'prod_small_modalPriceCost', 'Campo Validado', 'Campo valor do produto inválido')">
                        <small id="prod_small_modalPriceCost" class="text-secondary">
                            Digite o valor do produto
                        </small>
                    </div>

                    <div class="form-group col-12">
                        <label for="prod_input_modalDailyValue">
                            Valor da diária:
                        </label>
                        <input class="form-control w-100"
                                type="text"
                                id="prod_input_modalDailyValue" autocomplete="on"
                                onfocusout="money_validation('prod_input_modalDailyValue', 'prod_small_modalDailyValue', 'Campo Validado', 'Campo valor da diária inválido')">
                        <small id="prod_small_modalDailyValue" class="text-secondary">
                            Digite o valor da diária
                        </small>
                    </div>

                    <div class='form-group col-12'>
                        <input type='file' class="d-none" value='fileupload' id='prod_input_modalThumb' onchange='image_preview.call(this, "prod_img_modalThumb"); getting_file_extension(prod_input_modalThumb,"prod_input_modalFileExtension")'>
                        <input type="text" class="d-none" id="prod_input_modalFileExtension">
                        <button class="btn btn-dark" id="prod_button_modalThumb"><i class="fas fa-camera-retro"></i> Imagem do Produto</button>
                        <br><img class= 'rounded mt-2' src='' id='prod_img_modalThumb' style='max-height:200px; max-width: 200px;'>
                    </div>

                    <div class="form-group col-12 text-right">
                        <button class="btn btn-primary mr-1" id="prod_button_modalProd" type="button" onclick="action_modal_product()">
                        </button>

                        <button class="btn btn-danger" data-dismiss = "modal">
                            <i class="fas fa-times-circle text-white"></i> Fechar
                        </button>
                    </div>
                </div>

                <div id="snackbar4">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    $(function(){
        $('#prod_input_modalUnits').mask('00000000000');
        $('#prod_input_modalQtyStock').mask('00000000000');
        $('#rent_input_addProducts').mask('00000000000');
        $("#prod_input_modalPriceCost").mask("#.##0,00", {reverse: true});
        $("#prod_input_modalDailyValue").mask("#.##0,00", {reverse: true});
        document.getElementById('products').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        document.getElementById('prod_input_pagination').value = page;
        fetch_data_product('prod_div_autocomplete', 'prod_input_pagination', 'prod_input_search', 'prod_select_search', 'section_products_table', 0);
        document.getElementById('products').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });
</script>
@endsection
