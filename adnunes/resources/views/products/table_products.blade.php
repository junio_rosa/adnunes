<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$products->count()}} produtos de {{$products->total()}} ({{$products->firstItem()}} a {{$products->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $products->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Produto</th>
                    <th>Informações</th>
                    <th>Código</th>
                    @isset($rent)
                        @else
                        <th>Ações</th>
                    @endisset
                </tr>
            </thead>

            <tbody>
                @foreach($products as $prod)
                <tr class="text-center"
                    @isset($rent)
                        style="cursor: pointer" onclick="rent_modal_addProducts({{$prod}})"
                    @endisset
                >
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle"><img class= 'rounded' src='{{$prod->thumb}}' style='height:150px; width:auto;'></td>
                    <td class="text-justify">
                        <strong>
                            Nome:
                        </strong>{{$prod->name}}<br>

                        <strong>
                            Unidades:
                        </strong>{{$prod->units}}<br>

                        <strong>
                            Estoque:
                        </strong>{{$prod->qty_stock}}<br>

                        <strong>
                            Valor do produto:
                        </strong>{{number_format($prod->price_cost, 2, ',', '.')}}<br>

                        <strong>
                            Valor da diária:
                        </strong>{{number_format($prod->daily_value, 2, ',', '.')}}<br>
                        <strong>
                            Tipo de produto:
                        </strong>
                            @if ($prod->is_rent == 1)
                                Produto para aluguel
                            @else
                                Produto para venda
                            @endif
                    </td>
                    <td class="align-middle">{{$prod->id}}</td>
                    @isset($rent)
                        @else
                        <td class="align-middle">
                            @isset($able_prod)
                                @if ($able_prod)
                                <button class="btn btn-sm btn-primary"
                                    onclick="open_prod_modal({{$prod}})">
                                    <i class="fas fa-sync"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-danger mt-1"
                                    onclick="open_prod_modalDelCrtl({{$prod}}, 1)">
                                    <i class="fas fa-trash"></i> Desabilitar
                                </button>
                                @else
                                <button class="btn btn-sm btn-primary text-white"
                                    onclick="open_prod_modalDelCrtl({{$prod}}, 2)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                                @endif
                            @endisset
                        </td>
                    @endisset
                    @php
                        $i++;
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$products->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>