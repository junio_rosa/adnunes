<div class = "navbar navbar-expand-lg bg-light">
    <div class = "container">

        <!--LOGO-->
        <a class = "navbar-brand" href="/home">
            <img id = "logo_navbar" src = "{{ asset('assets/logo.png') }}">
        </a>
        <!--BUTTON MOBILE-->
        <button class = "navbar-toggler bg-button" type = "button" data-toggle = "collapse" data-target = "#navbar_dropdown">
            <i class="fas fa-bars text-dark"></i>
        </button>

        <!--MENU DE NAVEGAÇÃO-->
        <div class = "form-inline collapse navbar-collapse" id = "navbar_dropdown">
            <ul class = "navbar-nav ml-auto">
                {{-- LINKS ADMINISTRADOR --}}
                <li>
                    <a class = "nav-link mr-2 cursor-pointer" id="home" href="/home">
                        Home
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 cursor-pointer" id="clients" href="/clients">
                        Clientes
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 cursor-pointer" id="products" href='/products'>
                        Produtos
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 cursor-pointer" id="rent" href="/rent">
                        Aluguel
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 cursor-pointer" id="consults" href="/consult">
                        Consultas
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class = "nav-link dropdown-toggle mr-2 cursor-pointer"
                        id = "admin" role="button" data-toggle="dropdown">
                        {{ Auth::user()->name }}
                    </a>
                    {{-- LOGOUT --}}
                    <div class="dropdown-menu" aria-labelledby="admin">
                        <a class="dropdown-item" onclick="abre_modal_atualiza_usuario({{Auth::user()}})">
                            Atualizar Perfil
                        </a>
                        <a class="dropdown-item"
                            href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            Logout
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </a>
                    </div>

                </li>
            </ul>
        </div>

    </div>
</div>
<hr>
