<div class = "navbar navbar-expand-lg bg-light">
    <div class = "container">

        <!--LOGO-->
        <a class = "navbar-brand" href="/">
            <img id = "logo_navbar" src = "{{ asset('assets/logo.png') }}">
        </a>
        <!--BUTTON MOBILE-->
        <button class = "navbar-toggler bg-button" type = "button" data-toggle = "collapse" data-target = "#navbar_dropdown">
            <i class="fas fa-bars text-dark"></i>
        </button>

        <!--MENU DE NAVEGAÇÃO-->
        <div class = "form-inline collapse navbar-collapse" id = "navbar_dropdown">
            <ul class = "navbar-nav ml-auto">
                {{-- LINKS CONVIDADO --}}
                <li>
                    <a class = "nav-link mr-2 cursor-pointer"
                        id="index" href="/">
                        Home
                    </a>
                </li>
                <li>
                    <a class="nav-link mr-2 cursor-pointer" href="#"
                       onclick="abre_modal_pre_cadastro(0)">
                        Pré Cadastro
                    </a>
                </li>
            </ul>
        </div>

    </div>
</div>
<hr>

<section>
    @include('clients.pre_cadastro')
</section>
