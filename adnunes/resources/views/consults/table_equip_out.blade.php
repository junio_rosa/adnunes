<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$equip_out->count()}} saídas de equipamentos de {{$equip_out->total()}} ({{$equip_out->firstItem()}} a {{$equip_out->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $equip_out->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Cliente</th>
                    <th>Produto</th>
                    <th>Data de Saída</th>
                    <th>Qtde Alugada</th>
                    <th>Pedido Relacionado</th>
                    <th>Status do Pedido</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($equip_out as $e)
                <tr class="text-center
                @if ($e->rent->validation == 'Venceu')
                    bg-pending"
                @else
                    @if ($e->rent->status == 'Baixa')
                        bg-discharge"
                        @else
                        bg-aproved"
                    @endif
                @endif
                >
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle">{{$e->client->name}}</td>
                    <td class="align-middle">{{$e->product->name}}</td>
                    <td class="align-middle">{{date_format(date_create($e->date), "d/m/Y")}}</td>
                    <td class="align-middle">{{$e->qty}}</td>
                    <td class="align-middle">{{$e->rent_id}}</td>
                    <td class="align-middle">{{$e->rent->status}}</td>
                    <td class="align-middle">{{$e->id}}</td>
                    <td class="align-middle">
                        <button class="btn btn-danger mt-1" onclick="return_rent_id({{$e->rent_id}})">
                            <i class="fas fa-file-pdf"></i> PDF do Pedido
                        </button>
                    </td>
                </tr>
                @php
                    $i++;
                @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$equip_out->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>
