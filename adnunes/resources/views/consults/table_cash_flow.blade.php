<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$cash_flow->count()}} movimentações de caixa de {{$cash_flow->total()}} ({{$cash_flow->firstItem()}} a {{$cash_flow->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $cash_flow->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Cliente</th>
                    <th>Pedido Relacionado</th>
                    <th>Status</th>
                    <th>Data</th>
                    <th>Preço Total</th>
                    <th>Valor Pago na Movimentação</th>
                    <th>Valor Total já Acertado</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($cash_flow as $c)
                <tr class="text-center
                    @if ($c->rent->validation == 'Venceu')
                        bg-pending"
                    @else
                        @if ($c->rent->status == 'Baixa')
                            bg-discharge"
                            @else
                            bg-aproved"
                        @endif
                    @endif
                >
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle">{{$c->client_name->client_nome}}</td>
                    <td class="align-middle">{{$c->rent_id}}</td>
                    <td class="align-middle">{{$c->rent->status}}</td>
                    <td class="align-middle">{{date_format(date_create($c->date), "d/m/Y")}}</td>
                    <td class="align-middle">{{number_format($c->total_price, 2, ',', '.')}}</td>
                    <td class="align-middle">{{number_format($c->paid, 2, ',', '.')}}</td>
                    <td class="align-middle">{{number_format($c->total_paid, 2, ',', '.')}}</td>
                    <td class="align-middle">{{$c->id}}</td>
                    <td class="align-middle">
                        <button class="btn btn-danger mt-1" onclick="return_rent_id({{$c->rent_id}})">
                            <i class="fas fa-file-pdf"></i> PDF do Pedido
                        </button>
                    </td>
                </tr>
                @php
                    $i++;
                @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$cash_flow->links("pagination::bootstrap-4")}}
            </tfoot>
            <div class="card-footer">
                <h4>Valor Total Pago: <strong class="text-success">R$ {{number_format($sum_paid, 2, ',', '.')}}</strong></h4>
            </div>
        </table>
    </div>
</div>
