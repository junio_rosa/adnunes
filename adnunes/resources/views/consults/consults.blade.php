@extends('layouts.app')

@section('navbar')
    @include('navbar.navbar_auth')
@endsection

@section('content')
<div class="container">
    <div class="card border my-4">
        <div class="card-header">
            <div class="container">
                <h4 class="card-title text-secondary">
                    Painel de Consultas
                </h4>
                    <section id="section_consult_by_date">
                        <div class="form-row">
                            <div class="form-group col-12 col-lg-6">
                                <label>Data Início:</label>
                                <input type="date" class = 'form-control w-100' id="consult_init_date">
                            </div>
                            <div class="form-group col-12 col-lg-6">
                                <label>Data Final:</label>
                                <input type="date" class = 'form-control w-100' id="consult_ending_date">
                            </div>
                        </div>
                    </section>
                    <section id="section_consult_by_name">
                        <div class="form-row">
                            <div class="form-group col-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="consult_qty_equip_input" placeholder="Busque por produtos..." autocomplete="on">
                                </div>

                                <div id="consult_qty_equip_autocomplete" class="dropdown"></div>
                            </div>
                        </div>
                    </section>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label>Selecione o tipo de busca:</label>
                            <select class='form-control w-100' id='consult_select_search' onchange="change_consult()">
                                <option value='equip_in'>Buscar por Entrada de Equipamento</option>
                                <option value='equip_out'>Buscar por Saida de Equipamento</option>
                                <option value='cash_flow' selected='selected'>Buscar por Movimentação de Caixa</option>
                                <option value='qty_equip'>Buscar por informações de estoque dos produtos</option>
                            </select>
                        </div>

                        <div class="form-group ml-auto">
							<button class="my-2 mr-1 btn btn-danger" onclick="gerar_relatorio_equipamento()">
								<i class="fas fa-file-pdf"></i> Gerar PDF de equipamentos
							</button>
                            <a class="btn btn-secondary text-white mr-1" onclick="action_consult_search()">
                                <i class="fas fa-search"></i> Buscar
                            </a>
                        </div>
                    </div>
            </div>
        </div>

        <section id="section_equip_entry">
            @include('consults.table_equip_in')
        </section>

        <section id="section_equip_out">
            @include('consults.table_equip_out')
        </section>

        <section id="section_cash_flow">
            @include('consults.table_cash_flow')
        </section>

        <section id="section_qty_equip">
            @include('consults.table_qty_equip')
        </section>

        <input class="d-none" id="equip_in_pagination" value="1">
        <input class="d-none" id="equip_out_pagination" value="1">
        <input class="d-none" id="cash_flow_pagination" value="1">
        <input class="d-none" id="qty_equip_pagination" value="1">
        <input class="d-none" id="consult_control_pagination" value="3">
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    $(function(){
        change_consult();
        document.getElementById('consults').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var control_rent = document.getElementById('consult_control_pagination').value;
        if(control_rent == 1){
            document.getElementById('equip_in_pagination').value = page;
            consult_fetch_data('/consult/equip_in', 'equip_in_pagination', 'section_equip_entry');
        }
        if(control_rent == 2){
            document.getElementById('equip_out_pagination').value = page;
            consult_fetch_data('/consult/equip_out', 'equip_out_pagination', 'section_equip_out');
        }
        if(control_rent == 3){
            document.getElementById('cash_flow_pagination').value = page;
            consult_fetch_data('/consult/cash_flow', 'cash_flow_pagination', 'section_cash_flow');
        }
        if(control_rent == 4){
            document.getElementById('qty_equip_pagination').value = page;
            consult_qty_equip();
        }
        document.getElementById('consults').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });
</script>
@endsection
