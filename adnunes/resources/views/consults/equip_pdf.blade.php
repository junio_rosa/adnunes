<html>
    <head>
        <meta charset="UTF-8">
        <title>Pdf do Aluguel</title>
        <link rel="icon" href="{{asset('assets/favicon.ico')}}">
        <link rel=stylesheet href="{{asset('css/app.css')}}">
    </head>
    <body style="-webkit-print-color-adjust: exact; color-adjust: exact !important;">
        <div class='container'>

            <div class='col-12 form-row my-4 border border-dark'>

                <div class='mt-2 form-group col-4'>
                    <img src="{{asset('assets/logo_pdf.png')}}">
                </div>

                <div class='mt-2 form-group col-4 text-center'>
                    <strong>Locação de Máquinas e Utensílios para Construção Civil</strong>
                </div>

                <div class='btn mt-2 form-group col-4' style="background-color: #e9e9e9">
                    <small>37</small> 99986-9675|<small>37</small> 99114-3385<br>
                    <small>Vivo</small>
                </div>

                <div class='col-8 font-weight-bold' style='margin-top: -15px'>
                    ADILSON APARECIDO NUNES 0449946978 - CNPJ: 23.420.085/0001-62
                </div>

                <div class='col-4 text-center' style='margin-top: -15px'>
                    Contato: <strong style='font-family: cursive'>Adilson</strong>
                </div>

                <div class='col-12'>
                    Rua Antonia da Silva Ramos, 50 - B. Residencial Vitoria - CAPITÓLIO - MG
                </div>
            </div>
			
			<table class="table table-ordered table-hover table-bordered">
				@php
					$i = 1;
					$j = 0;
				@endphp
				
				@while ($j < sizeof($array_sum_qty))
				<tr>
				<td class="text-center">
				    <strong>Linha: </strong> {{$i}} <br>
				    <img class= 'rounded' src='{{asset($qty_equip[$j]->thumb)}}' style='height:150px; width:auto;'> <br>
                    <strong>Produto: </strong> {{$qty_equip[$j]->name}} <br>
                    <strong>Código do produto: </strong>{{$qty_equip[$j]->id}} <br>
                    <strong>Qtde no estoque: </strong>{{$qty_equip[$j]->qty_stock}} <br>
                    <strong> Qtde alugada: </strong>{{$array_sum_qty[$j] - $array_sum_return_qty[$j]}} <br>
                    @if($qty_equip[$j]->is_rent == 1)
                        <strong>Produto para aluguel</strong><br>
                    @else
                        <strong>Produto para venda</strong><br>
                    @endif
                </td>
                <td class="text-justify">
                    <strong>Pedidos que possuem o produto:</strong><br>
                    @php
				        $pedidos = explode('§', $array_rent[$j]);
				        $aux = 0;
				        while($aux < sizeof($pedidos)){
				            echo($pedidos[$aux].'<br>');
				            $aux++;
				        }
				    @endphp
				</td>
				</tr>
				@php
				    $i++;
                    $j++;
                @endphp
				@endwhile
			</table>
		
		</div>
	</body>
</html>
