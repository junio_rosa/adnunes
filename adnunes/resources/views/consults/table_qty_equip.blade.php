<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$qty_equip->count()}} produtos de {{$qty_equip->total()}} ({{$qty_equip->firstItem()}} a {{$qty_equip->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $qty_equip->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Produto</th>
                    <th>Nome Produto</th>
                    <th>Qtde no estoque</th>
                    <th>Qtde alugada</th>
                    <th>Pedidos que ainda possuem esse produto para devolver</th>
                    <th>Código Produto</th>
                </tr>
            </thead>

            <tbody>
                @php
                    $j = 0;
                @endphp
                @while ($j < sizeof($array_sum_qty))
                <tr class="text-center">
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle"><img class= 'rounded' src='{{$qty_equip[$j]->thumb}}' style='height:150px; width:auto;'></td>
                    <td class="align-middle">{{$qty_equip[$j]->name}}</td>
                    <td class="align-middle">{{$qty_equip[$j]->qty_stock}}</td>
                    <td class="align-middle">{{$array_sum_qty[$j] - $array_sum_return_qty[$j]}}</td>
                    <td class="align-middle">
                        <div class="text-justify w-100" style="overflow-y:auto; max-height: 150px">
                            {{$array_rent[$j]}}
                        </div>
                    </td>
                    <td class="align-middle">{{$qty_equip[$j]->id}}</td>
                    @php
                        $i++;
                        $j++;
                    @endphp
                </tr>
                @endwhile
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$qty_equip->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>
