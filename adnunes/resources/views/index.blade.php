@extends('layouts.app')

<style>
    body{
        overflow-x: hidden;
    }
</style>

@section('navbar')
    @include('navbar.navbar_guest')
@endsection

@section('content')
    @include('layouts.home_page')
@endsection

@section('javascript')
<script type="text/javascript">

    var control;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    $(function(){
        control = 0;
        document.getElementById('pre_cad_cadastro_simplificado').classList.add('d-none');
        $('#pre_cad_cpf').mask('000.000.000-00');
        $('#pre_cad_cnpj').mask('00.000.000/0000-00');
        var Phone_Mask_Behavior = function (val) {
            return val.replace(/\D/g, '').length == 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        Phone_Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(Phone_Mask_Behavior.apply({}, arguments), options);
            }
        };
        $('#pre_cad_tel_fixo').mask(Phone_Mask_Behavior, Phone_Options);
        $('#pre_cad_tel_comercio').mask(Phone_Mask_Behavior, Phone_Options);
        $('#pre_cad_tel_cel').mask(Phone_Mask_Behavior, Phone_Options);
        $('.editor').jqte();
        document.getElementById('index').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });
</script>
@endsection
