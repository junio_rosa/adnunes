@extends('layouts.app')

@section('navbar')
    @include('navbar.navbar_auth')
@endsection

@section('content')
<div class="container">
    <div class="card border my-4">
        <div class="card-header">
            <div class="container">
                <h4 class="card-title text-secondary">
                    Painel Administrativo de Aluguéis
                </h4>
                <section id="section_rent_searchClient">
                    <div class="form-row">
                        <div class="form-group col-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="rent_cli_input_search" placeholder="Busque por clientes..." autocomplete="off">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary rounded-right" type="button" onclick="fetch_data_client('rent_cli_div_autocomplete', 'rent_cli_input_pagination', 'rent_cli_input_search', 'rent_cli_select_search', 'section_rent_clients_table', 1); reset_rent_cliId()">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="rent_cli_div_autocomplete" class="dropdown"></div>
                        </div>

                        <div class="form-group col-12">
                            <label>Selecione o tipo de busca:</label>
                            <select class='form-control w-100' id='rent_cli_select_search' onchange="cli_select_on_change('rent_cli_input_search','rent_cli_select_search')">
                                <option value='nome' selected='selected'>Buscar por Nome</option>
                                <option value='endereco'>Buscar por Endereço</option>
                                <option value='cpf'>Buscar por CPF</option>
                                <option value='cnpj'>Buscar por CNPJ</option>
                            </select>
                        </div>

                        <div class="form-group ml-auto">
                            <button class="btn btn-primary text-white" onclick="rent_next()">
                                <i class="fas fa-arrow-circle-right"></i> Avançar
                            </button>

                            <button class="btn btn-secondary text-white" onclick="rent_reports()">
                                <i class="far fa-file"></i> Relatórios
                            </button>
                        </div>
                    </div>
                </section>

                <section id="section_rent_searchProduct">
                    <div class="form-row">
                        <div class="form-group col-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="rent_prod_input_search" placeholder="Busque por produtos..." autocomplete="off">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary rounded-right" type="button" onclick="fetch_data_product('rent_prod_div_autocomplete', 'rent_prod_input_pagination', 'rent_prod_input_search', 'rent_prod_select_search', 'section_rent_products_table', 1)">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="rent_prod_div_autocomplete" class="dropdown"></div>
                        </div>

                        <div class="form-group col-12">
                            <label>Selecione o tipo de busca:</label>
                            <select class='form-control w-100' id='rent_prod_select_search'>
                                <option value='name' selected='selected'>Buscar por Nome</option>
                            </select>
                        </div>

                        <div class="form-group ml-auto">
                            <button class="btn btn-primary text-white" onclick="rent_previous()">
                                <i class="fas fa-arrow-circle-left"></i> Voltar
                            </button>

                            <button class="btn btn-success text-white" onclick="rent_preview()">
                                <i class="fas fa-edit"></i> Visualizar Pedido
                            </button>

                            <button class="btn btn-secondary text-white" onclick="rent_reports()">
                                <i class="far fa-file"></i> Relatórios
                            </button>
                        </div>
                    </div>
                </section>

                <section id="section_rent_reports">
                    <div class="form-row">
                        <div class="form-group col-12">
                            <input type="text" class="form-control" id="rent_reports_input_search" placeholder="Busque por dados do clientes relacionados aos aluguéis..." autocomplete="off">
                            <div id="rent_reports_div_autocomplete" class="dropdown"></div>
                        </div>

                        <div class="form-group col-12">
                            <label>Selecione o tipo de busca:</label>
                            <select class='form-control w-100' id='rent_reports_select_search' onchange="cli_select_on_change('rent_reports_input_search','rent_reports_select_search')">
                                <option value='nome' selected='selected'>Buscar por nome</option>
                                <option value='address'>Buscar por endereço de entrega</option>
                                <option value='cpf'>Buscar por CPF</option>
                                <option value='cnpj'>Buscar por CNPJ</option>
                            </select>
                        </div>

                        <div class="form-group col-12 col-lg-4">
                            <label>De:</label>
                            <input type="date" class="form-control" id="rent_reports_init_date">
                        </div>

                        <div class="form-group col-12 col-lg-4">
                            <label>Até:</label>
                            <input type="date" class="form-control" id="rent_reports_ending_date">
                        </div>

                        <div class="form-group col-12 col-lg-4">
                            <label>Selecione o tipo de pedido:</label>
                            <select id="rent_select_rentType" class="form-control">
                                <option value='vazio' selected='selected'></option>
                                <option value='quitar'>Pedidos a Quitar</option>
                                <option value='quitados'>Pedidos Quitados</option>
                                <option value='baixa'>Pedidos em Baixa</option>
                                <option value='vencidos'>Pedidos Vencidos</option>
                            </select>
                        </div>

                        <div class="form-group ml-auto">
                            <button class="btn btn-primary text-white" onclick="rent_previous()">
                                <i class="fas fa-arrow-circle-left"></i> Voltar
                            </button>

                            <button class="btn btn-secondary rounded-right" type="button" onclick="fetch_data_rent(); rent_reports()">
                                <i class="fa fa-search"></i> Buscar
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        @php
            $rent = true;
        @endphp
        <section id="section_rent_clients_table">
            @include('clients.table_clients', compact('rent'))
        </section>

        <section id="section_rent_products_table">
            @include('products.table_products', compact('rent'))
        </section>

        <section id="section_table_rent_reports">
            @include('rent.table_rent')
        </section>

        <input class="d-none" id="rent_control_pagination" value = 1>
        <input class="d-none" id="rent_cli_input_pagination" value="1">
        <input class="d-none" id="rent_prod_input_pagination" value="1">
        <input class="d-none" id="rent_reports_pagination" value="1">
        <input type="text" class="d-none" id="rent_cli_id">
        <input type="text" class="d-none" id="rent_cli_status">
        <div id="snackbar5">Mensagem de erro:</div>
    </div>
</div>

<div class = "modal fade" id = "rent_add_products" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-md" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted">Adicionar produto</h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    <div class="form-group col-12">
                        <div id="rent_msg_addProducts"></div>
                    </div>

                    <div class="form-group col-12 mb-5">
                        <input type="text" id="rent_input_addProducts" class="form-control" placeholder="Digite a quantidade de produtos a serem adicionados na venda">
                    </div>

                    <input type="text" class="d-none" id="rent_idProducts_addProducts">
                    <input type="text" class="d-none" id="rent_qtyStock_addProducts">
                    <input type="text" class="d-none" id="rent_thumb_addProducts">
                    <input type="text" class="d-none" id="rent_name_addProducts">
                    <input type="text" class="d-none" id="rent_typeProduct_addProducts">
                    <input type="text" class="d-none" id="rent_monthly_addProducts">
                    <input type="text" class="d-none" id="rent_daily_addProducts">
                    <input type="text" class="d-none" id="rent_previousQty_addProducts">

                    <div class="form-group col-12 text-right">
                        <button class="btn btn-primary mr-1" type="button" onclick="rent_addProduct()">
                            <i class="fas fa-plus-circle"></i> Adicionar Produto
                        </button>

                        <button class="btn btn-danger" data-dismiss = "modal">
                            <i class="fas fa-times-circle text-white"></i> Fechar
                        </button>
                    </div>
                </div>

                <div id="snackbar7">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>

<div class = "modal fade" id = "rent_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-xl" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted" id="rent_modal_title"></h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    <input class='d-none' id="rent_modal_cliId">

                    <input class="d-none" id="rent_modal_rentId">

                    <div class="form-group col-12">
                        <label>Nome: </label>
                        <input class="form-control w-100"
                                type="text"
                                id="rent_modal_cliName" readonly>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label>Data de devolução: </label>
                        <input class="form-control w-100"
                                type="date" autocomplete="on"
                                id="rent_modal_cliDevolutionDate"
                                onfocusout="field_date_validation('rent_modal_cliDevolutionDate', 'rent_small_cliDevolutionDate', 'Data Válida', 'Data Inválida')">
                        <small id="rent_small_cliDevolutionDate" class="text-secondary">
                            Digite uma data de devolução para o pedido
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label>Status do pedido: </label>
                        <input class="form-control w-100"
                                type="text"
                                id="rent_modal_rentStatus" readonly>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label>Hora: </label>
                        <input class="form-control w-100"
                                type="text" autocomplete="on"
                                id="rent_modal_cliHour"
                                onfocusout="hour_validation('rent_modal_cliHour', 'rent_small_cliHour', 'Campo Validado', 'Campo Hora Inválido')">
                        <small id="rent_small_cliHour" class="text-secondary">
                            Digite o horário que o pedido foi realizado
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label>Data do pedido: </label>
                        <input class="form-control w-100"
                                type="date" autocomplete="on"
                                id="rent_modal_cliRentDate"
                                onfocusout="field_date_validation('rent_modal_cliRentDate', 'rent_small_cliRentDate', 'Data Válida', 'Data Inválida')">
                        <small id="rent_small_cliRentDate" class="text-secondary">
                            Digite a data que o pedido foi realizado
                        </small>
                    </div>

                    <div class="form-group col-12">
                        <label>Endereço de Entrega: </label>
                        <input class="form-control w-100"
                                type="text" autocomplete="on"
                                id="rent_modal_cliAddress"
                                onfocusout="field_validation('rent_modal_cliAddress', 'rent_small_cliAddress', 'Campo Validado', 'Campo endereço inválido', 1)">
                        <small id="rent_small_cliAddress" class="text-secondary">
                            Digite o endereço de entrega
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label>Valor da entrega: </label>
                        <input class="form-control w-100"
                                type="text" autocomplete="on"
                                id="rent_modal_rentDeliveryValue">
                        <small class="text-secondary">
                            Digite um valor de entrega
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label>Desconto: </label>
                        <input class="form-control w-100"
                                type="text" autocomplete="on"
                                id="desconto">
                        <small class="text-secondary">
                            Digite um valor para o desconto
                        </small>
                    </div>

                    <div class="form-group col-12">
                        <label>Valor total Diária: </label>
                        <input class="form-control w-100"
                                type="text"
                                id="rent_modal_cliTotalDaily" readonly>
                    </div>

                    <div class="form-group col-12 col-lg-6" id="form_group_rent_total_price">
                        <label>Valor total: </label> <i class="fas fa-dollar-sign text-success" style="cursor:pointer" onclick="calc_preco_total()"> Calcular Preço Total</i>
                        <input class="form-control w-100"
                                type="text" autocomplete="on"
                                id="rent_modal_cliTotalPrice"
                                onfocusout="money_validation('rent_modal_cliTotalPrice', 'rent_small_cliTotalPrice', 'Campo Validado', 'Campo Valor Total Inválido')">
                        <small id="rent_small_cliTotalPrice" class="text-secondary">
                            Digite o valor total do pedido
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-6" id="form_group_rent_previous_paid">
                        <label>Valor pago pelo cliente:
                            <i class="fas fa-info-circle text-info"
                                id="popover"
                                data-placement="top"
                                data-toggle="popover"
                                style="cursor: pointer"></i>
                        </label>
                        <input class="form-control w-100"
                                type="text"
                                id="rent_modal_cliPreviousPaid" readonly>
                        <small class="text-secondary">
                            Digite o valor já acertado
                        </small>
                    </div>

                    <div class="form-group col-12 col-lg-4" id="form_group_rent_paid_value">
                        <label>Valor já acertado: </label>
                        <input class="form-control w-100"
                                type="text"
                                id="rent_modal_cliPaidValue" readonly>
                    </div>

                    <div class="form-group col-12">
                        <label>Pessoas Autorizadas a Receber o Produto:</label>
                        <textarea class="form-control w-100 editor" id="rent_modal_cliAuthorizedWorkers"></textarea>
                    </div>

                    <div class="table-responsive mb-5">
                        <table class="table table-ordered table-hover" id="rent_table">
                            {{-- THEAD COLOCADA POR JAVASCRIPT --}}
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-12 text-right">
                        <button class="btn btn-primary" type="button" id="rent_modal_button" onclick="action_modal_rent()">
                        </button>

                        <button class="btn btn-danger" data-dismiss = "modal">
                            <i class="fas fa-times-circle text-white"></i> Fechar
                        </button>
                    </div>
                </div>

                <div id="snackbar6">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>

<div class = "modal fade" id = "rent_delete_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-md" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted">Deletar Pedido</h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">
                <div class="row">
                    <div class="col-12 mb-5" id="delete_rentModal_msg">
                    </div>
                    <input class="d-none" id="delete_rentModal_id">
                </div>

                <div class="form-group col-12 text-right">
                    <button class="btn btn-primary mr-1" type="button" id="button_delete_rentModal" onclick="action_button_delete_rent()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>

                <div id="snackbar8">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    $(function(){
        $('#rent_modal_cliHour').mask('00:00');
        $("#rent_modal_cliTotalPrice").mask("#.##0,00", {reverse: true});
        $("#rent_modal_cliPreviousPaid").mask("#.##0,00", {reverse: true});
        $("#rent_modal_rentDeliveryValue").mask("#.##0,00", {reverse: true});
        $("#desconto").mask("#.##0,00", {reverse: true});
        $("#rent_input_addProducts").mask("0000000000");
        $('.editor').jqte();
        document.getElementById('rent').style.borderBottom = "thick solid rgb(246, 199, 27)";
        rent_previous();
        reset_rent_cliId();
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var control_rent = document.getElementById('rent_control_pagination').value;
        if(control_rent == 1){
            document.getElementById('rent_cli_input_pagination').value = page;
            fetch_data_client('rent_cli_div_autocomplete', 'rent_cli_input_pagination', 'rent_cli_input_search', 'rent_cli_select_search', 'section_rent_clients_table', 1);
            document.getElementById('rent_cli_id').value = '';
            reset_rent_cliId();
        }
        if(control_rent == 2){
            document.getElementById('rent_prod_input_pagination').value = page;
            fetch_data_product('rent_prod_div_autocomplete', 'rent_prod_input_pagination', 'rent_prod_input_search', 'rent_prod_select_search', 'section_rent_products_table', 1);
        }
        if(control_rent == 3){
            document.getElementById('rent_reports_pagination').value = page;
            fetch_data_rent();
            rent_reports();
        }
        document.getElementById('rent').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });
</script>
@endsection
