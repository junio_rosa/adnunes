<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$rents_preview->count()}} aluguéis de {{$rents_preview->total()}} ({{$rents_preview->firstItem()}} a {{$rents_preview->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $rents_preview->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Cliente</th>
                    <th>Data de Devolução</th>
                    <th>Endereço de Entrega</th>
                    <th>Valor Total</th>
                    <th>Status</th>
                    <th>Valor já Acertado</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($rents_preview as $r)
                <tr class="text-center
                @if ($r->validation == 'Venceu')
                    bg-pending"
                @else
                    @if ($r->status == 'Baixa')
                        bg-discharge"
                        @else
                        bg-aproved"
                    @endif
                @endif
                >
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle">
						@if($r->client->tipo_cadastro == 'Pessoa Jurídica') {{$r->client->nome_empresa}} @else {{$r->client->client_name}} @endif
					</td>
                    <td class="align-middle">{{date_format(date_create($r->devolution_date), "d/m/Y")}}</td>
                    <td class="align-middle">{{$r->address}}</td>
                    <td class="align-middle">{{number_format(($r->total_price + $r->delivery_value - $r->desconto), 2, ',', '.')}}</td>
                    <td class="align-middle">{{$r->status}}</td>
                    <td class="align-middle">{{number_format($r->previous_paid, 2, ',', '.')}}</td>
                    <td class="align-middle">{{$r->id}}</td>
                    <td class="align-middle">
                        <button class="my-2 mr-1 btn
                            @if ($r->status == 'Baixa')
                                btn-success"
                                onclick="open_rent_modal({{$r}},{{$r->client}}, {{$r->rent_details}})">
                                <i class="fas fa-file-alt"></i> Visualizar
                            @else
                                @if ($r->status == 'A quitar')
                                    btn-primary"
                                @else
                                    btn-primary"
                                @endif
                                onclick="open_rent_modal({{$r}},{{$r->client}}, {{$r->rent_details}})">
                                <i class="fas fa-sync"></i> Editar
                            @endif
                        </button>

                        @if ($r->status == 'Baixa')
                            <button class="my-2 mr-1 btn btn-primary" onclick="gera_recibo({{$r->id}})">
                                <i class="far fa-file-alt"></i> Gerar Recibo
                            </button>
                        @endif

                        <button class="my-2 mr-1 btn btn-danger" onclick="return_rent_id({{$r->id}})">
                            <i class="fas fa-file-pdf"></i> Gerar PDF
                        </button>
                        @if ($r->status == 'A quitar')
                        <button class="my-2 mr-1 btn btn-dark" onclick="open_modal_delete_rent({{$r}}, {{$r->client}})">
                            <i class="fas fa-trash"></i> Excluir
                        </button>

                        <button class="my-2 mr-1 btn btn-success" onclick="open_modal_pay_all({{$r}}, {{$r->client}})">
                            <i class="fas fa-dollar-sign"></i> Pagar Tudo
                        </button>
                        @endif
                    </td>
                </tr>
                @php
                    $i++;
                @endphp
                @endforeach
            </tbody>
        </table>
        <input class='d-none' type="text" id='rent_report_pdf_id'>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$rents_preview->links("pagination::bootstrap-4")}}
            </tfoot>
            <div class="card-footer">
                <h4>Valor Total do Pedidos: <strong class="text-success">R$ {{number_format(($sum_total_price + $sum_delivery_value -$sum_desconto), 2, ',', '.')}}</strong></h4>
                <h4>Valor Total já acertado: <strong class="text-success">R$ {{number_format($sum_previous_paid, 2, ',', '.')}}</strong></h4>
                <h4>Valor que falta ser acertado: <strong class="text-danger">R$ {{number_format(($sum_total_price + $sum_delivery_value - $sum_previous_paid - $sum_desconto), 2, ',', '.')}}</strong></h4>
            </div>
        </table>
    </div>
</div>
