<html>
    <head>
        <meta charset="UTF-8">
        <title>Pdf do Aluguel</title>
        <link rel="icon" href="{{asset('assets/favicon.ico')}}">
        <link rel=stylesheet href="{{asset('css/app.css')}}">
    </head>
    <body style="-webkit-print-color-adjust: exact; color-adjust: exact !important;">
        <div class='container'>

            <div class='col-12 form-row my-4 border border-dark'>

                <div class='mt-2 form-group col-4'>
                    <img src="{{asset('assets/logo_pdf.png')}}">
                </div>

                <div class='mt-2 form-group col-4 text-center'>
                    <strong>Locação de Máquinas e Utensílios para Construção Civil</strong>
                </div>

                <div class='btn mt-2 form-group col-4' style="background-color: #e9e9e9">
                    <small>37</small> 99986-9675|<small>37</small> 99114-3385<br>
                    <small>Vivo</small>
                </div>

                <div class='col-8 font-weight-bold' style='margin-top: -15px'>
                    ADILSON APARECIDO NUNES 0449946978 - CNPJ: 23.420.085/0001-62
                </div>

                <div class='col-4 text-center' style='margin-top: -15px'>
                    Contato: <strong style='font-family: cursive'>Adilson</strong>
                </div>

                <div class='col-12'>
                    Rua Antonia da Silva Ramos, 50 - B. Residencial Vitoria - CAPITÓLIO - MG
                </div>
            </div>

            <div class='form-row'>

                <div class='form-group col-8 text-left font-weight-bold' style='margin-top: -20px; margin-left: -5px; font-family: Helvetica'>
                    CONTRATO DE LOCAÇÃO / TERMO DE ENTREGA DE EQUIPAMENTO
                </div>

                <div class='form-group col-4 text-center font-weight-bold text-danger' style='margin-top: -20px; margin-left: -5px; font-family: Helvetica; background-color: #DCDCDC'>
                    Nº &nbsp; &nbsp; &nbsp; &nbsp; {{$rent_pdf->id}}
                </div>

                <div class='col-12 form-row border border-dark' style='margin-top: -16px;'>
                    <div class='mt-2 form-group col-12 text-right'>
                        Pelo presente instrumento particular de Procuração, como Locador (a) <strong>ADILSON APARECIDO NUNES</strong>
                    </div>

                    <div class='col-8 text-left'>
                        como Locatário: <u>@if($rent_pdf->client->tipo_cadastro == 'Pessoa Jurídica') {{$rent_pdf->client->nome_empresa}} @else {{$rent_pdf->client->client_name}} @endif</u>
                    </div>

                    <div class='col-4 text-left'>
                        Nome pop: ____________________
                    </div>

                    <div class='col-8 text-left'>
                        Estabelecido à: <u>{{$rent_pdf->client->client_address}}</u>
                    </div>

                    <div class='col-4 text-left'>
                        Bairro: <u>{{$rent_pdf->client->client_neighborhood}}</u>
                    </div>

                    <div class='col-12 text-left'>
                        Endereço da obra: <u>{{$rent_pdf->address}}</u>
                    </div>

                    <div class='col-4 text-left'>
                        Fone p/ contato 1: <u>{{$rent_pdf->client->client_phone1}}</u>
                    </div>

                    <div class='col-4 text-left'>
                        Fone p/ contato 2: <u>{{$rent_pdf->client->client_phone2}}</u>
                    </div>

                    <div class='col-4 text-left'>
                        Fone p/ contato 3: <u>{{$rent_pdf->client->client_phone3}}</u>
                    </div>

                    <div class='col-4 text-left'>
                        Cidade: <u>{{$rent_pdf->client->client_city}}</u>
                    </div>

                    <div class='col-4 text-left'>
                        Estado: <u>{{$rent_pdf->client->client_state}}</u>
                    </div>

                    <div class='col-4 text-left'>
                        Email: <u>{{$rent_pdf->client->client_email}}</u>
                    </div>

                    <div class='col-4 text-left'></div>
                    <div class='col-4 text-left'>
                        I.Est./RG: ____________________
                    </div>

                    <div class='col-4 text-left'>
                        Pedido Nº: {{$rent_pdf->id}}
                    </div>

                    <div class='col-12 text-left'>
                        CO - Responsabilidade de: ____________________________________________________________<br>
                        Funcionários autorizados a receber, devolver e fazer pedido nesta obra: <br><u> {{$rent_pdf->authorized_workers}}</u>
                    </div>
                </div>
            </div>

            <div class='form-row mt-1'>
                <div class='form-group col-11 text-left font-weight-bold w-100' style='font-family: Helvetica; font-size: 10px; background-color: #e9e9e9; margin-left: -5px;'>
                    RECEBI DA LOCADORA, EM PERFEITA ORDEM, COM SEU RESPECTIVO VALOR DIÁRIO, OS ITENS LOCADOS ABAIXO RELACIONADOS:
                </div>
                <div class='form-group col-1 text-center font-weight-bold' style='font-family: Helvetica; font-size: 9px; background-color: #e9e9e9'></div>

                <table class='table table-bordered table-sm' style='margin-top: -16px; margin-left: -5px; font-size:11px'>
                    <thead style='background-color: #e9e9e9'>
                        <tr>
                            <th scope='col'>DESCRIÇÃO PATRIMÔNIO</th>
                            <th scope='col'>QUANT.</th>
                            <th scope='col'>VALOR DIÁRIA</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php
                            $price_cost = 0;
                        @endphp
                        @foreach ($rent_pdf->rent_details as $products)
                        <tr>
                            <td>{{$products->name}}</td>
                            <td>{{$products->qty}}</td>
                            @if($products->is_rent == 1)
                                <td>R$: {{number_format($products->daily, 2, ',', '.')}}</td>
                            @else
                                <td>R$: {{number_format(($products->price_cost * $products->qty), 2, ',', '.')}}</td>
                            @endif
                        <tr>
                        @php
                            if($products->is_rent == 1){
                                $price_cost = $price_cost + ($products->price_cost * $products->qty);
                            }
                        @endphp
                        @endforeach
                    </tbody>

                    <tfoot>
                       <tr style='background-color: #e9e9e9' scope='col'>
                            <th style='background-color: #e9e9e9' scope='col' class='font-weight-bold'><h6 class="font-weight-bold">TOTAL GERAL: R$ {{number_format(($rent_pdf->total_price + $rent_pdf->delivery_value), 2, ',', '.')}}</h6></th>
                            <th style='background-color: #e9e9e9' scope='col' class='font-weight-bold'><h6 class="font-weight-bold">VALOR DO DESCONTO: R$ {{number_format($rent_pdf->desconto, 2, ',', '.')}}</h6></th>
                            <th style='background-color: #e9e9e9' scope='col' class='font-weight-bold'><h6 class="font-weight-bold">TOTAL A PAGAR: R$ {{number_format(($rent_pdf->total_price + $rent_pdf->delivery_value - $rent_pdf->desconto), 2, ',', '.')}}</h6></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class='form-row' style='font-size:14px'>

                <div class='form-group col-12' style='margin-top: -15px; margin-left: -5px; font-size: 10px'>
                    Fazemos a entrega do produto a uma taxa de R$: {{number_format($rent_pdf->delivery_value, 2, ',', '.')}}. Referente ao contrato de locação, além da LOCATÁRIA assumem pessoal e solidariamente, integral responsabilidade civil e criminal, por todas as obrigações do presente contrato e ficam depositários dos equipamentos locados, até final do contrato conforme condições apresentadas, que mutuamente outorgam e aceitam.
                </div>

                <div class='form-group col-12 font-weight-bold text-center' style='margin-top: -15px; margin-left: -5px; font-size:18px; background-color: #e9e9e9'>
                    CONTRATO DE LOCAÇÃO
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 1º - A LOCADORA aluga ao LOCATÁRIO os equipamentos aqui relacionados,
                    nas condições e prazos estabelecidos, cujo pagamento a LOCATÁRIA efetuará __________________________________________________________ no escritório da LOCADORA ou onde for por ela indicado, contra quitação do correspondente Comprovante. Em caso de mora, a LOCARÁRIA
                    se obriga ao pagamento de juros, acrescido de correção monetária, multa de 10% e das despesas de cobrança administrativa ou judicial.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 2º - Caberá à LOCATÁRIA retirar o equipamento do depósito da
                    LOCADORA, obrigando-se por outro lado, a devolver o referido equipamento ora locado imediatamente após o término do prazo contratual,
                    correndo as despesas de transporte em ambos os casos por conta da LOCATÁRIA que responderá ainda, por eventuais danos causados ao equipamento.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 3º - Após o término de vigência deste contrato, o mesmo poderá
                    ser prorrogado caso seja do interesse de ambas as partes, ficando entretanto, sujeito às alterações dos preços de locação, conforme lista
                    de preços vigente na data de renovação.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 4º - Considerar-se-à rescindido de pleno direito a
                    presente locação nos seguintes casos: a) se a LOCATÁRIA faltar ao pagamento dos aluguéis no prazo fixado; b) se a LOCATÁRIA
                    findo o prazo contratual, não fizer imediata devolução do equipamento locado ou não comunicar o interesse na prorrogação deste contrato.<br>
                    <i>Parágrafo Único - </i>Ocorrendo qualquer das hipóteses acima mencionadas, poderá a LOCADORA valer-se da faculdade prevista no art. 502, do
                    Código Civil retirando, por seus próprios meios e condições o equipamento locado, onde quer que esteja, independente de qualquer aviso,
                    notificação ou interpelação.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 5º - O equipamento locado, conforme verificado pela LOCATÁRIA, assinado,
                    foi entregue em perfeito estado de conservação e uso, ficando a LOCATÁRIA responsável se o equipamento ao ser devolvido, apresentar
                    eventuais avarias, defeitos ou falta de peças componentes; o valor das peças danificadas que forem recusadas pela LOCADORA no ato
                    da devolução, ou faltantes, serão reembolsadas pela locatária ao preço encontrado na data da devolução do equipamento.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 6º - Quaisquer acidentes ocorridos com o equipamento locado
                    o por ele causado a terceiros, desde sua retirada até seu efetivo recebimento pela LOCADORA, serão de exclusiva responsabilidade da
                    LOCATÁRIA, excluindo a locadora de quaisquer responsabilidade civis ou trabalhista e do pagamento de quaisquer indenizações, seja a que
                    título for. Parágrafo Único: A LOCATÁRIA se resposabiliza por eventuais furtos ou roubos ocorridos
                    com o equipamento locado, com o dever de indenizar a LOCADORA por tais eventuais ocorrências,
                    sem prejuízos de perdas e danos devidamente apurados, de acordo com os valores de mercado do equipamento,
                    objeto do presente.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 7º - A devolução do equipamento será acompanhada de Nota de
                    Devolução, emitido pela locatária, que assume exclusiva responsabilidade por infrações fiscais decorrentes de suas omissões.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 8º - A parte que infringir qualquer disposição do presente
                    contrato, sujeitar-se-á ao pagamento de multa, equivalente a <u>R$: {{number_format($price_cost, 2, ',', '')}}</u>.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; 9º - Elegem as partes o Foro da Comarca de Piumhi
                    com exclusão de qualquer outro, por mais privilegiado que seja, afim de dirimirem quaisquer dúvidas decorrentes do presente
                    contrato, respondendo em qualquer caso a parte vencida, pelas despesas judiciais e honorários advocatícios.
                </div>

                <div class='form-group col-12 text-justify' style='margin-top: -15px; margin-left: -5px; font-size:11px;'>
                    &emsp;&emsp;&emsp;&emsp;&emsp; E por estar assim justas e contratadas, havendo
                    as partes lido e achado conforme as cláusulas impressas, assinam o presente em duas vias de igual teor,
                    na presença de duas testemunhas.
                </div>

                <div class='form-group col-12 font-weight-bold text-center' style='margin-top: -15px; margin-left: -5px; font-size:16px; background-color: #e9e9e9'>
                    EM CASO DE MAL USO DO EQUIPAMENTO O CONSERTO SERÁ POR CONTA DO LOCATÁRIO.
                </div>

                <div class='form-group col-12 text-center' style='margin-left: -5px; font-size: 16px'>
                    @php
                    $actual_day = date_format($rent_pdf->rent_date, "d");
					$actual_year = date_format($rent_pdf->rent_date, "Y");
                    $actual_month = date_format($rent_pdf->rent_date, "m");

                    if ($actual_month == '01') {
                        $actual_month = 'Janeiro';
                    }
                    if ($actual_month == '02') {
                        $actual_month = 'Fevereiro';
                    }
                    if ($actual_month == '03') {
                        $actual_month = 'Março';
                    }
                    if ($actual_month == '04') {
                        $actual_month = 'Abril';
                    }
                    if ($actual_month == '05') {
                        $actual_month = 'Maio';
                    }
                    if ($actual_month == '06') {
                        $actual_month = 'Junho';
                    }
                    if ($actual_month == '07') {
                        $actual_month = 'Julho';
                    }
                    if ($actual_month == '08') {
                        $actual_month = 'Agosto';
                    }
                    if ($actual_month == '09') {
                        $actual_month = 'Setembro';
                    }
                    if ($actual_month == '10') {
                        $actual_month = 'Outubro';
                    }
                    if ($actual_month == '11') {
                        $actual_month = 'Novembro';
                    }
                    if ($actual_month == '12') {
                        $actual_month = 'Dezembro';
                    }
                    echo "Capitólio, <u>$actual_day</u> de <u>$actual_month</u> de <u>$actual_year</u> $rent_pdf->hour hrs<br>";
                    echo "Data prevista de devolução: ".date_format(date_create($rent_pdf->devolution_date), "d/m/Y");
                    @endphp
                </div>

                <div class='form-group col-6 text-center' style='margin-left: -5px;'>
                    _________________________________________________<br>
                    <small>LOCATÁRIO (ASSINATURA AUTORIZADA)</small>
                </div>
                <div class='form-group col-6 text-center' style='margin-left: -5px;'>
                    _________________________________________________<br>
                    <small>LOCADORA (ASSINATURA AUTORIZADA)</small>
                </div>
                <div class='form-group col-6 text-center' style='margin-left: -5px;'>
                    _________________________________________________<br>
                    <small>(NOME)</small>
                </div>
                <div class='form-group col-6 text-center' style='margin-left: -5px;'>
                    _________________________________________________<br>
                    <small>(NOME)</small>
                </div>
                <div class='form-group col-6 text-center' style='margin-left: -5px;'>
                    _________________________________________________<br>
                    <small>TESTEMUNHA</small>
                </div>
                <div class='form-group col-6 text-center' style='margin-left: -5px;'>
                    _________________________________________________<br>
                    <small>TESTEMUNHA</small>
                </div>

            </div>

            <div class='col-12 form-row my-2 border border-dark' style="font-size: 14px;">

                <div class='font-weight-bold my-3'>
                    Eu Adilson recebi a mercadoria na data ________/_______/__________ na hora _____:______
                    referente ao pedido {{$rent_pdf->id}} do cliente {{$rent_pdf->client->client_name}}
                </div>
            </div>

        </div>
    </body>

    <script src="{{ asset('js/helpers.js')}}" type="text/javascript"></script>
</html>
