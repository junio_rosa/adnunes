<html>
    <head>
        <meta charset="UTF-8">
        <title>Recibo</title>
        <link rel="icon" href="{{asset('assets/favicon.ico')}}">
        <link rel=stylesheet href="{{asset('css/app.css')}}">
    </head>

    <body style="-webkit-print-color-adjust: exact; color-adjust: exact !important;">
        <div class='container'>

            <div class='col-12 form-row my-4 border border-dark'>

                <div class='mt-2 form-group col-4'>
                    <img src="{{asset('assets/logo_pdf.png')}}">
                </div>

                <div class='mt-2 form-group col-4 text-center'>
                    <strong>Locação de Máquinas e Utensílios para Construção Civil</strong>
                </div>

                <div class='btn mt-2 form-group col-4' style="background-color: #e9e9e9">
                    <small>37</small> 99986-9675|<small>37</small> 99114-3385<br>
                    <small>Vivo</small>
                </div>

                <div class='col-8 font-weight-bold' style='margin-top: -15px'>
                    ADILSON APARECIDO NUNES 0449946978 - CNPJ: 23.420.085/0001-62
                </div>

                <div class='col-4 text-center' style='margin-top: -15px'>
                    Contato: <strong style='font-family: cursive'>Adilson</strong>
                </div>

                <div class='col-12'>
                    Rua Antonia da Silva Ramos, 50 - B. Residencial Vitoria - CAPITÓLIO - MG
                </div>

                <h1 class="my-2 col-12 text-center">RECIBO</h1>

                <div class="col-12">
                    Recebi(emos) do cliente <u>{{$rent_pdf->client->client_name}}</u>, a quantia de
                    R$ {{number_format(($rent_pdf->total_price + $rent_pdf->delivery_value - $rent_pdf->desconto), 2, ',', '.')}},
                    <u id="money">

                    </u>
                    correspondente ao aluguel do pedido de número {{$rent_pdf->id}} no período de
                    <u>
                        {{date_format(date_create($rent_pdf->rent_date), "d/m/Y")}}
                    </u>
                    até a data de
                    <u>
                        {{date_format(date_create($rent_pdf->devolution_date), "d/m/Y")}}
                    </u>, e para clareza firmo(amos) o presente na cidade de
                    <u>
                        {{$rent_pdf->client->client_city}}
                    </u> no dia de
                    <u>
                        @php
                            echo date('d/m/Y');
                        @endphp
                    </u>.
                </div>

                <div class="my-3 col-12">
                    Assinatura: _________________________________________________________________________________
                </div>

                <div class="my-3 col-12">
                    Nome por extenso: <u>Adilson Aparecido Nunes</u>, ou representante legal indicado pela firma.
                </div>
            </div>

        </div>
    </body>

    <script src="{{ asset('js/helpers.js')}}" type="text/javascript"></script>
    <script>
        document.getElementById('money').innerHTML = ValorPorExtenso({{($rent_pdf->total_price + $rent_pdf->delivery_value - $rent_pdf->desconto)}});
    </script>
</html>
