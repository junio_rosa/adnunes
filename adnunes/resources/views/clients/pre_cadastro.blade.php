<div class = "modal fade" id = "modal_pre_cadastro" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-xl" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted" id="pre_cad_title"></h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">

                <div class="form-row">
                    <div class="col-12 form-group" id="div_pre_cad_tipo_cadastro">
                        <label>Selecione o tipo de cadastro</label>
                        <select class='form-control w-100' id='tipo_cadastro' onchange="pre_cadastro_select()">
                            <option value='Pessoa Física' selected='selected'>Pessoa Física</option>
                            <option value='Pessoa Jurídica'>Pessoa Jurídica</option>
                            <option value='Cadastro Simplificado' id='pre_cad_cadastro_simplificado'>Cadastro Simplificado</option>
                        </select>
                    </div>

                    <div class="col-12 w-100 text-center my-4" id="div_dados_empresa">
                        <h3>
                            <u>Dados da empresa</u>
                        </h3>
                    </div>

                    <input type="text" class="d-none" id="pre_cad_id">

                    {{-- NOME --}}
                    <div class="col-12 col-lg-6 form-group" id="div_nome_empresa">
                        <label>Nome da empresa:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_nome_empresa" autocomplete="on"
                                onfocusout="field_validation('pre_cad_nome_empresa', 'pre_cad_small_nome_empresa', 'Campo Validado', 'Campo nome da empresa inválido', 1)">
                        <small id="pre_cad_small_nome_empresa" class="text-secondary">
                            Digite o nome da empresa
                        </small>
                    </div>

                    {{-- EMAIL --}}
                    <div class="col-12 col-lg-6 form-group" id="div_email_empresa">
                        <label>E-mail:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_email_empresa" autocomplete="on"
                                onfocusout="emailValidation('pre_cad_email_empresa', 'pre_cad_small_email_empresa', 'Campo Validado', 'Campo e-mail empresa inválido')">
                        <small id="pre_cad_small_email_empresa" class="text-secondary">
                            Digite o e-mail da empresa
                        </small>
                    </div>

                    {{-- ENDEREÇO --}}
                    <div class="col-12 col-lg-6 form-group" id="div_endereco_empresa">
                        <label>Endereço da empresa:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_end_empresa" autocomplete="on"
                                onfocusout="field_validation('pre_cad_end_empresa', 'pre_cad_small_end_empresa', 'Campo Validado', 'Campo endereço da empresa inválido', 1)">
                        <small id="pre_cad_small_end_empresa" class="text-secondary">
                            Digite o endereço da empresa
                        </small>
                    </div>

                    {{-- BAIRRO --}}
                    <div class="col-12 col-lg-6 form-group" id="div_bairro_empresa">
                        <label>Bairro da empresa:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_bairro_empresa" autocomplete="on"
                                onfocusout="field_validation('pre_cad_bairro_empresa', 'pre_cad_small_bairro_empresa', 'Campo Validado', 'Campo bairro da empresa inválido', 1)">
                        <small id="pre_cad_small_bairro_empresa" class="text-secondary">
                            Digite o bairro da empresa
                        </small>
                    </div>

                    {{-- CIDADE --}}
                    <div class="col-12 col-lg-4 form-group" id="div_cidade_empresa">
                        <label>Cidade da empresa:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_cidade_empresa" autocomplete="on"
                                onfocusout="field_validation('pre_cad_cidade_empresa', 'pre_cad_small_cidade_empresa', 'Campo Validado', 'Campo cidade inválido', 1)">
                        <small id="pre_cad_small_cidade_empresa" class="text-secondary">
                            Digite a cidade da empresa
                        </small>
                    </div>

                    {{-- ESTADO --}}
                    <div class="form-group col-12 col-lg-4" id="div_estado_empresa">
                        <label>Estado:</label>
                        <select class='form-control w-100' id='pre_cad_estado_empresa' required>
                            <option value='AC'>AC</option>
                            <option value='AL'>AL</option>
                            <option value='AP'>AP</option>
                            <option value='AM'>AM</option>
                            <option value='BA'>BA</option>
                            <option value='CE'>CE</option>
                            <option value='ES'>ES</option>
                            <option value='GO'>GO</option>
                            <option value='MA'>MA</option>
                            <option value='MT'>MT</option>
                            <option value='MS'>MS</option>
                            <option value='MG' selected='selected'>MG</option>
                            <option value='PA'>PA</option>
                            <option value='PB'>PB</option>
                            <option value='PE'>PE</option>
                            <option value='PI'>PI</option>
                            <option value='RJ'>RJ</option>
                            <option value='RN'>RN</option>
                            <option value='RS'>RS</option>
                            <option value='RO'>RO</option>
                            <option value='RR'>RR</option>
                            <option value='SC'>SC</option>
                            <option value='SP'>SP</option>
                            <option value='SE'>SE</option>
                            <option value='TO'>TO</option>
                            <option value='DF'>DF</option>
                        </select>
                    </div>

                    {{-- CNPJ --}}
                    <div class="col-12 col-lg-4 form-group" id="div_cnpj_empresa">
                        <label>CNPJ da empresa:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_cnpj" autocomplete="on"
                                onfocusout="cnpj_validation('pre_cad_cnpj', 'pre_cad_small_cnpj', 'Campo Validado', 'Campo CNPJ inválido')">
                        <small id="pre_cad_small_cnpj" class="text-secondary">
                            Digite o CNPJ da empresa
                        </small>
                    </div>

                    <div class="col-12 w-100 text-center my-4" id="div_dados_representante">
                        <h3>
                            <u>Dados do representante da empresa</u>
                        </h3>
                    </div>

                    <div class="col-12 w-100 text-center my-4" id="div_dados_cliente">
                        <h3>
                            <u>Dados do cliente</u>
                        </h3>
                    </div>

                    {{-- NOME --}}
                    <div class="col-12 col-lg-6 form-group">
                        <label>Nome:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_nome" autocomplete="on"
                                onfocusout="field_validation('pre_cad_nome', 'pre_cad_small_nome', 'Campo Validado', 'Campo nome inválido', 1)">
                        <small id="pre_cad_small_nome" class="text-secondary">
                            Digite o nome
                        </small>
                    </div>

                    {{-- EMAIL --}}
                    <div class="col-12 col-lg-6 form-group">
                        <label>E-mail:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_email" autocomplete="on"
                                onfocusout="emailValidation('pre_cad_email', 'pre_cad_small_email', 'Campo Validado', 'Campo e-mail inválido')">
                        <small id="pre_cad_small_email" class="text-secondary">
                            Digite o e-mail
                        </small>
                    </div>

                    {{-- ENDEREÇO --}}
                    <div class="col-12 col-lg-6 form-group">
                        <label>Endereço:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_endereco" autocomplete="on"
                                onfocusout="field_validation('pre_cad_endereco', 'pre_cad_small_endereco', 'Campo Validado', 'Campo endereço inválido', 1)">
                        <small id="pre_cad_small_endereco" class="text-secondary">
                            Digite o endereço
                        </small>
                    </div>

                    {{-- BAIRRO --}}
                    <div class="col-12 col-lg-6 form-group">
                        <label>Bairro:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_bairro" autocomplete="on"
                                onfocusout="field_validation('pre_cad_bairro', 'pre_cad_small_bairro', 'Campo Validado', 'Campo bairro inválido', 1)">
                        <small id="pre_cad_small_bairro" class="text-secondary">
                            Digite o bairro
                        </small>
                    </div>

                    {{-- CIDADE --}}
                    <div class="col-12 col-lg-4 form-group" id="div_pre_cad_cidade">
                        <label>Cidade:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_cidade" autocomplete="on"
                                onfocusout="field_validation('pre_cad_cidade', 'pre_cad_small_cidade', 'Campo Validado', 'Campo cidade inválido', 1)">
                        <small id="pre_cad_small_cidade" class="text-secondary">
                            Digite a cidade
                        </small>
                    </div>

                    {{-- ESTADO --}}
                    <div class="form-group col-12 col-lg-4" id="div_pre_cad_estado">
                        <label>Estado:</label>
                        <select class='form-control w-100' id='pre_cad_estado' required>
                            <option value='AC'>AC</option>
                            <option value='AL'>AL</option>
                            <option value='AP'>AP</option>
                            <option value='AM'>AM</option>
                            <option value='BA'>BA</option>
                            <option value='CE'>CE</option>
                            <option value='ES'>ES</option>
                            <option value='GO'>GO</option>
                            <option value='MA'>MA</option>
                            <option value='MT'>MT</option>
                            <option value='MS'>MS</option>
                            <option value='MG' selected='selected'>MG</option>
                            <option value='PA'>PA</option>
                            <option value='PB'>PB</option>
                            <option value='PE'>PE</option>
                            <option value='PI'>PI</option>
                            <option value='RJ'>RJ</option>
                            <option value='RN'>RN</option>
                            <option value='RS'>RS</option>
                            <option value='RO'>RO</option>
                            <option value='RR'>RR</option>
                            <option value='SC'>SC</option>
                            <option value='SP'>SP</option>
                            <option value='SE'>SE</option>
                            <option value='TO'>TO</option>
                            <option value='DF'>DF</option>
                        </select>
                    </div>

                    {{-- TELEFONE FIXO --}}
                    <div class="form-group col-12 col-lg-4">
                        <label>Telefone fixo:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_tel_fixo" autocomplete="on"
                                onfocusout="phones_validation()">
                        <small id="pre_cad_small_tel_fixo" class="text-secondary">
                            Digite o telefone fixo
                        </small>
                    </div>

                    {{-- TELEFONE COMERCIAL --}}
                    <div class="form-group col-12 col-lg-4">
                        <label>Telefone comercial:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_tel_comercio" autocomplete="on"
                                onfocusout="phones_validation()">
                        <small id="pre_cad_small_tel_comercio" class="text-secondary">
                            Digite o telefone comercial
                        </small>
                    </div>

                    {{-- CELULAR --}}
                    <div class="form-group col-12 col-lg-4">
                        <label>Celular:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_tel_cel" autocomplete="on"
                                onfocusout="phones_validation()">
                        <small id="pre_cad_small_tel_cel" class="text-secondary">
                            Digite o celular
                        </small>
                    </div>

                    {{-- CPF --}}
                    <div class="col-12 col-lg-4 form-group" id="div_pre_cad_cpf">
                        <label>CPF:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="pre_cad_cpf" autocomplete="on"
                                onfocusout="cpf_validation('pre_cad_cpf', 'pre_cad_small_cpf', 'Campo Validado', 'Campo CPF inválido')">
                        <small id="pre_cad_small_cpf" class="text-secondary">
                            Digite o CPF
                        </small>
                    </div>

                    {{-- FORMAS DE PAGAMENTO --}}
                    <div class="form-group col-12" id="div_pre_cad_forma_pagamento">
                        <label>Formas de pagamento:</label>
                        <select class='form-control w-100' id='pre_cad_formas_pagamento' required>
                            <option value='Emissão de Boleto Bancário' selected='selected'>Emissão de Boleto Bancário</option>
                            <option value='Pagamento diretamente na empresa fornecedora'>Pagamento diretamente na empresa fornecedora</option>
                            <option value='Cheque pós-datado'>Cheque pós-datado</option>
                            <option value='Cartão de Crédito'>Cartão de Crédito</option>
                            <option value='Nota Promissória'>Nota Promissória</option>
                        </select>
                    </div>

                    {{-- OBSERVAÇÕES --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Observações:</label>
                        <textarea class="form-control editor" type="text" id="pre_cad_observacoes"></textarea>
                    </div>

                    <div class="col-12 w-100 text-center my-4" id="div_pre_cad_upload">
                        <h3>
                            Upload de arquivos
                        </h3>
                    </div>

                    {{-- IMG DO CONTRATO --}}
                    <div class='form-group col-12' id="div_imagem_contrato">
                        <label class="font-weight-bold">Documento de identificação: (Preferencialmente a CNH, porém pode ser CPF ou RG)</label> <br>
                        <input type='file' class="d-none" value='fileupload' id='pre_cad_file_input' onchange='image_preview.call(this, "pre_cad_img_thumb"); getting_file_extension(pre_cad_file_input, "pre_cad_img_extension")'>
                        <input type="text" class="d-none" id="pre_cad_img_extension">
                        <button class="btn btn-dark" id="pre_cad_img_button"><i class="fas fa-camera-retro"></i> Selecionar imagem do documento</button>
                        <br><img class= 'rounded mt-2' src='' id='pre_cad_img_thumb' style='max-height:200px; max-width: 200px;'>
                    </div>

                    {{-- CONTRATO SOCIAL --}}
                    <div class='form-group col-12' id="div_contrato_social">
                        <label class="font-weight-bold">Upload do contrato social: </label> <br>
                        <input type='file' class="d-none" value='fileupload' id='pre_cad_file_input_contrato_social' onchange='documento_preview.call(this, pre_cad_file_input_contrato_social, "pre_cad_img_thumb_contrato_social"); getting_file_extension(pre_cad_file_input_contrato_social, "pre_cad_img_extension_contrato_social")'>
                        <input type="text" class="d-none" id="pre_cad_img_extension_contrato_social">
                        <button class="btn btn-dark" id="pre_cad_img_button_contrato_social"><i class="fas fa-camera-retro"></i> Selecionar documento</button>
                        <br><img class= 'rounded mt-2' src='' id='pre_cad_img_thumb_contrato_social' style='max-height:200px; max-width: 200px;'>
                    </div>

                    {{-- JUNTA COMERCIAL --}}
                    <div class='form-group col-12' id="div_junta_comercial">
                        <label class="font-weight-bold">Upload do documento de certidão JUCEMG:</label> <br>
                        <input type='file' class="d-none" value='fileupload' id='pre_cad_file_input_junta_comercial' onchange='documento_preview.call(this, pre_cad_file_input_junta_comercial, "pre_cad_img_thumb_junta_comercial"); getting_file_extension(pre_cad_file_input_junta_comercial, "pre_cad_img_extension_junta_comercial")'>
                        <input type="text" class="d-none" id="pre_cad_img_extension_junta_comercial">
                        <button class="btn btn-dark" id="pre_cad_img_button_junta_comercial"><i class="fas fa-camera-retro"></i> Selecionar documento</button>
                        <br><img class= 'rounded mt-2' src='' id='pre_cad_img_thumb_junta_comercial' style='max-height:200px; max-width: 200px;'>
                    </div>

                    {{-- TERMO DE AUTORIZAÇÃO --}}
                    <div class='form-group col-12' id="div_termo">
                        <label class="font-weight-bold">Documento de identificação do sócio: (Preferencialmente a CNH, porém pode ser CPF ou RG)</label> <br>
                        <input type='file' class="d-none" value='fileupload' id='pre_cad_file_input_termo' onchange='image_preview.call(this, "pre_cad_img_thumb_termo"); getting_file_extension(pre_cad_file_input_termo, "pre_cad_img_extension_termo")'>
                        <input type="text" class="d-none" id="pre_cad_img_extension_termo">
                        <button class="btn btn-dark" id="pre_cad_img_button_termo"><i class="fas fa-camera-retro"></i> Selecionar documento</button>
                        <br><img class= 'rounded mt-2' src='' id='pre_cad_img_thumb_termo' style='max-height:200px; max-width: 200px;'>
                    </div>
                </div>

                <div class="form-group col-12">
                    <input type="checkbox" class="form-check-input" id="pre_cad_checkbox_confirmacao">
                        Eu de acordo com a nova lei geral de proteção de dados LGPD, <a target="_blank" href="https://www.serpro.gov.br/lgpd/menu/a-lgpd/o-que-muda-com-a-lgpd">Lei nº 13.709/2018</a>,
                        concordo em ceder meus dados pessoais para uso exclusivo da empresa ADNUNES -
                        CNPJ: 23.420.085/0001-62, assim como a mesma não fara uso indevido desse dados, agindo de acordo com os novos termos do uso de dados.
                </div>

                <a class="text-my-3" id="pre_cad_ver_mais" href="#" onclick="pre_cad_ver_mais()">
                    Ver mais
                </a>

                <div class="form-group col-12 my-3 d-none" id="div_termo_autorizacao">
                    <h3 class="text-center">TERMO DE AUTORIZAÇÃO CADASTRAL E DE CONDIÇÕES DE VENDA</h3>
                    <div class="my-3">
                        Termo de Consentimento para Tratamento de Dados: Este documento visa registrar a
                        manifestação livre, informada e inequívoca pela qual o Titular, ora denominado de cliente,
                        concorda com o tratamento de seus dados pessoais para finalidade específica aprovação
                        de cadastro e para liberação de crédito para compras a prazo, tudo em conformidade com
                        a Lei nº 13.709 – Lei Geral de Proteção de Dados Pessoais (LGPD). Ao manifestar sua
                        aceitação para com o presente termo, o cliente consente e concorda que a Controladora
                        (ADNUNES, 23.420.085/0001-62 e Rua Antônia da Silva Ramos, 50, Residencial Vitória)
                        doravante denominada de fornecedora, tome decisões referentes ao tratamento de seus
                        dados pessoais, dados referentes as empresas em que atuem os usuários ou dados
                        necessários ao usufruto de serviços, bem como realize o tratamento de tais dados,
                        envolvendo operações como as que se referem a coleta, produção, recepção,
                        classificação, utilização, acesso, reprodução, transmissão, distribuição, processamento,
                        arquivamento, armazenamento, eliminação, avaliação ou controle da informação,
                        modificação, comunicação, transferência, difusão ou extração. Dados Pessoais e
                        empresariais. A fornecedora fica autorizada a tomar decisões referentes ao tratamento e a
                        realizar o tratamento dos seguintes dados pessoais do cliente: Nome completo; Nome
                        empresarial; Data de nascimento; Número e imagem da Carteira de Identidade (RG);
                        Número e imagem do Cadastro de Pessoas Físicas (CPF); Número e imagem da Carteira
                        Nacional de Habilitação (CNH); Número do Cadastro Nacional de Pessoas Jurídicas
                        (CNPJ); Fotografia 3x4; Estado civil; Nível de instrução ou escolaridade; Estado Civil,
                        Filiação; Endereço completo; Números de telefone, WhatsApp e endereços de e-mail;
                        Banco, agência e número de contas bancárias; Bandeira, número, validade e código de
                        cartões de crédito; Nome de usuário e senha específicos para uso dos serviços da
                        fornecedora; Comunicação, verbal e escrita, mantida entre o cliente e fornecedora. Além
                        disso, a fornecedora fica autorizada a tomar decisões referentes ao tratamento e a
                        realizar o tratamento dos dados ora informados pelo Titular, com a intenção de autorizar
                        consultas junto aos órgãos de proteção ao crédito, e assim a possibilidade de obter, após
                        análise, aprovação de crédito para compras a prazo. Finalidades do Tratamento dos
                        Dados: Possibilitar que a fornecedora identifique, consulte, analise e aprove cadastro,
                        bem como entre em contato com o cliente para fins de relacionamento comercial.
                        Possibilitar que a fornecedora realize vendas para terceiros devidamente autorizados e
                        indicados pelo cliente no presente documento, ou que informe ou indique tais pessoas
                        posteriormente via mensagens telefônicas ou por e-mail, como também autorize a
                        fornecedora emitir notas fiscais das compras realizadas, boletos e emita cobranças em
                        face do cliente. Possibilitar que a fornecedora estruture, teste, promova e faça
                        propaganda de produtos e serviços, personalizados ou não ao perfil do cliente. Possibilitar
                        que fornecedora utilize tais dados em Pesquisas de Mercado; Possibilitar que a
                        fornecedora utilize tais dados para suas peças de Comunicação e que utilize tais dados
                        emissão de Notas Fiscais e documentos financeiros correlatos; Possibilitar que a
                        fornecedora utilize tais dados para manter banco de dados de profissionais do mercado
                        para facilitar o contato futuro. Compartilhamento de Dados: a fornecedora fica autorizada
                        a compartilhar os dados pessoais do cliente com outros agentes de tratamento de dados,
                        caso seja necessário para as finalidades listadas neste termo, observados os princípios e
                        as garantias estabelecidas pela Lei nº 13.709. Segurança dos Dados: a fornecedora
                        responsabiliza-se pela manutenção de medidas de segurança, técnicas e administrativas
                        aptas a proteger os dados pessoais de acessos não autorizados e de situações acidentais
                        ou ilícitas de destruição, perda, alteração, comunicação ou qualquer forma de tratamento
                        inadequado ou ilícito. Em conformidade ao art. 48 da Lei nº 13.709, a fornecedora
                        comunicará ao cliente e à Autoridade Nacional de Proteção de Dados (ANPD) a
                        ocorrência de incidente de segurança que possa acarretar risco ou dano relevante aocliente. Término do Tratamento dos Dados: a Controladora poderá manter e tratar os
                        dados pessoais do cliente durante todo o período em que forem pertinentes ao alcance
                        das finalidades listadas neste termo. O cliente poderá solicitar via e-mail ou
                        correspondência ao Controlador, a qualquer momento, após justificado motivo de não
                        necessidade manutenção, que sejam eliminados os dados pessoais do cliente. O cliente
                        fica ciente de que poderá ser inviável ao Controlador continuar o fornecimento de
                        produtos ou serviços ao Titular a partir da eliminação dos dados pessoais, podendo ser
                        restaurado mediante assinatura de novo termo. Direitos do cliente: O cliente tem direito a
                        obter da fornecedora, em relação aos dados por ele tratados, a qualquer momento e
                        mediante requisição: I - confirmação da existência de tratamento; II - acesso aos dados; III
                        - correção de dados incompletos, inexatos ou desatualizados; IV - anonimização, bloqueio
                        ou eliminação de dados desnecessários, excessivos ou tratados em desconformidade
                        com o disposto na Lei nº 13.709; V portabilidade dos dados a outro fornecedor de serviço
                        ou produto, mediante requisição expressa e observados os segredos comercial e
                        industrial, de acordo com a regulamentação do órgão controlador; V portabilidade dos
                        dados a outro fornecedor de serviço ou produto, mediante requisição expressa, de acordo
                        com a regulamentação da autoridade nacional, observados os segredos comercial e
                        industrial; VI eliminação dos dados pessoais tratados com o consentimento do titular,
                        exceto nas hipóteses previstas no art. 16 da Lei nº 13.709; VII - informação das entidades
                        públicas e privadas com as quais o controlador realizou uso compartilhado de dados; VIII -
                        informação sobre a possibilidade de não fornecer consentimento e sobre as
                        consequências da negativa; IX - revogação do consentimento, nos termos do § 5º do art.
                        8º da Lei nº 13.709. Direito de Revogação do Consentimento: Este consentimento poderá
                        ser revogado pelo cliente, a qualquer momento, mediante solicitação via e-mail ou
                        correspondência à fornecedora.
                    </div>
                </div>

                <a class="text-my-3 d-none" id="pre_cad_ver_menos" href="#" onclick="pre_cad_ver_menos()">
                    Ver menos
                </a>

                <div class="form-group col-12 text-right">
                    <button class="btn btn-primary mr-1" id="pre_cad_action_button" type="button" onclick="action_pre_cadastro()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>

                <div id="snackbar2">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>
