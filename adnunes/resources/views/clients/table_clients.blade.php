<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$clients->count()}} clientes de {{$clients->total()}} ({{$clients->firstItem()}} a {{$clients->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $clients->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Nome</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($clients as $cli)
                    <tr @isset($rent)
                            id = "tr_cli{{$cli->id}}"
                        @endisset
                        class="text-center
                        @if ($cli->status == 'Aprovado')
                            bg-aproved"
                        @else
                            bg-pending"
                        @endif
                    >
                    <td class="align-middle">{{$i}}</td>
                @if ($cli->tipo_cadastro != 'Pessoa Jurídica')
                    <td class="align-middle">{{$cli->nome}}</td>
                    <td class="text-justify">
                        <strong>Endereço: </strong>{{$cli->endereco}} <strong>Bairro: </strong>{{$cli->bairro}}<br>
                        <strong>Cidade: </strong>{{$cli->cidade}} <strong>Estado: </strong>{{$cli->estado}}
                        <strong>Telefones:</strong>
                        @if($cli->tel_fixo != '')
                            {{$cli->tel_fixo}}
                        @endif
                        @if ($cli->tel_comercio != '')
                            {{$cli->tel_comercio}}
                        @endif
                        @if ($cli->tel_cel)
                            {{$cli->tel_cel}}
                        @endif<br>
                        <strong>Email: </strong>{{$cli->email}} <strong>Status: </strong>{{$cli->status}}<br>
                        @if($cli->cpf != null)
                            <strong>CPF: </strong> {{$cli->cpf}}
                        @endif
                        @if($cli->cnpj != null)
                            <strong>CNPJ: </strong> {{$cli->cnpj}}
                        @endif
                    </td>
                @else
                    <td class="align-middle">{{$cli->nome_empresa}}</td>
                    <td class="text-justify">
                        <strong>Nome do representante: </strong>{{$cli->nome}}
                        <strong>Endereço: </strong>{{$cli->endereco_empresa}} <strong>Bairro: </strong>{{$cli->bairro_empresa}}<br>
                        <strong>Cidade: </strong>{{$cli->cidade_empresa}} <strong>Estado: </strong>{{$cli->estado_empresa}}
                        <strong>Telefones:</strong>
                        @if($cli->tel_fixo != '')
                            {{$cli->tel_fixo}}
                        @endif
                        @if ($cli->tel_comercio != '')
                            {{$cli->tel_comercio}}
                        @endif
                        @if ($cli->tel_cel)
                            {{$cli->tel_cel}}
                        @endif<br>
                        <strong>Email: </strong>{{$cli->email_empresa}} <strong>Status: </strong>{{$cli->status}}<br>
                        @if($cli->cpf != null)
                            <strong>CPF: </strong> {{$cli->cpf}}
                        @endif
                        @if($cli->cnpj != null)
                            <strong>CNPJ: </strong> {{$cli->cnpj}}
                        @endif
                    </td>
                @endif
                    <td class="align-middle">{{$cli->id}}</td>
                    <td class="align-middle">
                        @isset($rent)
                            <button class="btn btn-sm btn-info text-white" onclick="select_client({{$cli}})">
                                <i class="fas fa-user-check"></i> Selecionar Cliente
                            </button>
                        @else
                            @isset($able_cli)
                                @if ($able_cli)
                                @if (!$cli->aprovado)
                                    <button class="my-2 mr-2 btn btn-sm btn-secondary"
                                        onclick="aprovar_cliente({{$cli->id}})">
                                        <i class="fas fa-user-check"></i> Aprovar Cliente
                                    </button>
                                @endif
                                <button class="my-2 mr-2 btn btn-sm btn-primary"
                                    onclick="visualizar_documentos({{$cli}})">
                                    <i class="far fa-folder-open"></i> Visualizar Arquivos
                                </button>
                                <button class="my-2 mr-2 btn btn-sm btn-success"
                                    onclick="abre_modal_pre_cadastro({{$cli}})">
                                    <i class="fas fa-user-edit"></i> Editar
                                </button>
                                <button class="my-2 mr-2 btn btn-sm btn-dark mt-1"
                                    onclick="open_cli_modalDelCrtl({{$cli}}, 1)">
                                    <i class="fas fa-user-times"></i> Desabilitar
                                </button>
                                @else
                                <button class="my-2 mr-2 btn btn-sm btn-info text-white"
                                    onclick="open_cli_modalDelCrtl({{$cli}}, 2)">
                                    <i class="fas fa-user-check"></i> Habilitar
                                </button>
                                @endif
                            @endisset
                        @endisset
                    </td>
                    @php
                        $i++;
                    @endphp
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$clients->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>