@extends('layouts.app')

@section('navbar')
    @include('navbar.navbar_auth')
@endsection

@section('content')
<div class="container">
    <div class="card border my-4">
        <div class="card-header">
            <div class="container">
                <h4 class="card-title text-secondary">
                    Painel Administrativo dos Clientes
                </h4>
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="input-group">
                            <input type="text" class="form-control" id="cli_input_search" placeholder="Busque por clientes..." autocomplete="on">
                            <div class="input-group-append">
                                <button class="btn btn-secondary rounded-right" type="button" onclick="fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0)">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>

                        <div id="cli_div_autocomplete" class="dropdown"></div>
                    </div>

                    <div class="form-group col-12">
                        <label for = 'cli_select_search'>Selecione o tipo de busca:</label>
                        <select class='form-control w-100' id='cli_select_search' onchange="cli_select_on_change('cli_input_search','cli_select_search')">
                            <option value='nome' selected='selected'>Buscar por Nome</option>
                            <option value='endereco'>Buscar por Endereço</option>
                            <option value='cpf'>Buscar por CPF</option>
                            <option value='cnpj'>Buscar por CNPJ</option>
                            <option value='nome_del'>Buscar por Nome (Clientes desabilitados)</option>
                            <option value='endereco_del'>Buscar por Endereço (Clientes desabilitados)</option>
                            <option value='cpf_del'>Buscar por CPF (Clientes desabilitados)</option>
                            <option value='cnpj_del'>Buscar por CNPJ (Clientes desabilitados)</option>
                            <option value='aprovado'>Buscar por clientes desautorizados</option>
                        </select>
                    </div>

                    <div class="form-group ml-auto d-none">
                        <button class="btn btn-primary text-white"
                            onclick="abre_modal_pre_cadastro(0)">
                            <i class="fas fa-user-plus"></i> Novo cliente
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <section id="section_clients_table">
            @include('clients.table_clients')
        </section>

        <section>
            @include('clients.pre_cadastro')
        </section>

        <section>
            @include('clients.modal_documentos')
        </section>

        <input class="d-none" id="cli_input_pagination" value="1">
        <div id="snackbar11">Mensagem de erro:</div>
    </div>
</div>

<div class = "modal fade" id = "cli_modalDelCrtl" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-md" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted" id='cli_h4_modalDelCrtl'></h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">
                <div class="row">
                    <div class="col-12 mb-5" id="cli_div_modalDelCrtl">
                    </div>
                    <input class="d-none" id="cli_input_modalIdDelCrtl">
                </div>

                <div class="form-group col-12 text-right">
                    <button class="btn btn-primary mr-1" id='cli_button_modalDelCrtl' type="button" onclick="action_cli_modalDelCrtl()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>

                <div id="snackbar10">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    var control;

    $(function(){
        control = 1;
        document.getElementById('pre_cad_cadastro_simplificado').classList.remove('d-none');
        $('#pre_cad_cpf').mask('000.000.000-00');
        $('#pre_cad_cnpj').mask('00.000.000/0000-00');
        var Phone_Mask_Behavior = function (val) {
            return val.replace(/\D/g, '').length == 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        Phone_Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(Phone_Mask_Behavior.apply({}, arguments), options);
            }
        };
        $('#pre_cad_tel_fixo').mask(Phone_Mask_Behavior, Phone_Options);
        $('#pre_cad_tel_comercio').mask(Phone_Mask_Behavior, Phone_Options);
        $('#pre_cad_tel_cel').mask(Phone_Mask_Behavior, Phone_Options);
        $('.editor').jqte();
        cli_select_on_change('cli_input_search','cli_select_search');
        document.getElementById('clients').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        document.getElementById('cli_input_pagination').value = page;
        fetch_data_client('cli_div_autocomplete', 'cli_input_pagination', 'cli_input_search', 'cli_select_search', 'section_clients_table', 0);
        document.getElementById('clients').style.borderBottom = "thick solid rgb(246, 199, 27)";
    });
</script>
@endsection
