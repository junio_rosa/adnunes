<div class = "modal fade" id = "modal_visualizar_documentos" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-md" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        <h4 class="text-muted">Visualizar documentos do cliente</h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x text-white"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">

                <div class="form-row" id="arquivo_identificacao_cliente">
                    <div class="form-group d-none">
                        <div id="nome_img_contrato">Arquivo de Identificação do cliente</div>
                    </div>
                    <div class="form-group col-12">
                        <label class='font-weight-bold'>Arquivo de Identificação do cliente:</label>
                        <button class="ml-5 btn btn-primary"
                            onclick="modal_abrir_documento('nome_img_contrato')"
                            ><i class="far fa-image"></i> Visualizar Imagem</button>
                    </div>
                </div>

                <div class="form-row" id="arquivo_termo_contrato_social">
                    <div class="form-group d-none">
                        <div id="nome_termo_contrato_social">Termo de contrato social</div>
                    </div>
                    <div class="form-group col-12">
                        <label class='font-weight-bold'>Termo de contrato social:</label>
                        <button class="ml-5 btn btn-primary"
                            onclick="modal_abrir_documento('nome_termo_contrato_social')"
                            ><i class="fas fa-file-download"></i> Visualizar Documento</button>
                    </div>
                </div>

                <div class="form-row" id="arquivo_termo_junta_comercial">
                    <div class="form-group d-none">
                        <div id="nome_termo_junta_comercial">Certidão JUCEMG</div>
                    </div>
                    <div class="form-group col-12">
                        <label class='font-weight-bold'>Certidão JUCEMG:</label>
                        <button class="ml-5 btn btn-primary"
                            onclick="modal_abrir_documento('nome_termo_junta_comercial')"
                            ><i class="fas fa-file-download"></i> Visualizar Documento</button>
                    </div>
                </div>

                <div class="form-row" id="arquivo_termo_autorizacao">
                    <div class="form-group d-none">
                        <div id="nome_termo_autorizacao">Termo de autorização</div>
                    </div>
                    <div class="form-group col-12">
                        <label class='font-weight-bold'>Termo de autorização:</label>
                        <button class="ml-5 btn btn-primary"
                            onclick="modal_abrir_documento('nome_termo_autorizacao')"
                            ><i class="fas fa-file-download"></i> Visualizar Documento</button>
                    </div>
                </div>
                <hr>

                <div class="form-group col-12 text-right">
                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
