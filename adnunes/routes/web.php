<?php

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\ConsultController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\RentController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('auth');

/* API ATUALIZAR DADOS DE PERFIL */
Route::post('/atualizar_dados_perfil', [HomeController::class, 'atualizar_dados_perfil']);
Route::get('/atualizar_header', [HomeController::class, 'atualizar_header']);

/* API CLIENTES */
Route::get('/clients', [ClientsController::class, 'index'])->middleware('auth');
Route::post('/clients/cadastro', [ClientsController::class, 'store']);
Route::post('/clients/autocomplete', [ClientsController::class, 'autocomplete']);
Route::delete('/clients/{id}', [ClientsController::class, 'destroy']);
Route::get('/clients/create/{id}', [ClientsController::class, 'create']);
Route::get('/clients/aprovar_cliente/{id}', [ClientsController::class, 'aprovar_cliente']);
Route::post('/clients/update', [ClientsController::class, 'update']);
Route::get('/clients/fetch_data', [ClientsController::class, 'show']);
Route::get('/termo_autorizacao', [ClientsController::class, 'termo']);
Route::post('/enviar_termo', [ClientsController::class, 'email_termo']);
/* API PRODUTOS */
Route::get('/products', [ProductsController::class, 'index'])->middleware('auth');
Route::post('/products', [ProductsController::class, 'store']);
Route::post('/products/autocomplete', [ProductsController::class, 'autocomplete']);
Route::get('/products/fetch_data', [ProductsController::class, 'show']);
Route::post('/products/update', [ProductsController::class, 'update']);
Route::delete('/products/{id}', [ProductsController::class, 'destroy']);
Route::get('/products/create/{id}', [ProductsController::class, 'create']);
/* API ALUGUEL */
Route::get('/consult', [ConsultController::class, 'index'])->middleware('auth');
Route::get('/rent', [RentController::class, 'index'])->middleware('auth');
Route::post('/rent', [RentController::class, 'store']);
Route::post('/rent/autocomplete', [RentController::class, 'autocomplete']);
Route::get('/rent/fetch_data', [RentController::class, 'show']);
Route::delete('/rent/{id}', [RentController::class, 'destroy']);
Route::get('/rent/payAll/{id}', [RentController::class, 'payAll']);
Route::put('/rent/{id}', [RentController::class, 'update']);
/* API CONSULTAS */
Route::get('/consult/equip_in', [ConsultController::class, 'equip_in']);
Route::get('/consult/equip_out', [ConsultController::class, 'equip_out']);
Route::get('/consult/cash_flow', [ConsultController::class, 'cash_flow']);
Route::get('/consult/qty_equip', [ConsultController::class, 'qty_equip']);
