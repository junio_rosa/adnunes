<?php

use App\Http\Controllers\ConsultController;
use App\Http\Controllers\RentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:web')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/rent/pdf_rent', [RentController::class, 'rent_pdf']);
Route::post('/rent/recibo', [RentController::class, 'recibo']);
Route::post('/consult/equip_pdf', [ConsultController::class, 'equip_pdf']);
